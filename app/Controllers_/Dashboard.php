<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Dashboard extends BaseController
{
    public function index()
    {
		// $client = \Config\Services::curlrequest();
		// $response = $client->request('GET', 'https://ws.akademik.itb.ac.id/kelas_pengajar?nip_dosen=eq.115110002', [
			// 'headers' => [
				// 'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZnRtZF9sbyIsImV4cCI6MTY3MzcxNTYwMH0.476Vh-XsVqU40tgHrz3hlwGosN54ahrRSKbTBVS2Tw8',
				// 'Accept-Profile' => 'v_ftmd',
				// 'Accept'     => 'application/json',
			// ],				
		// ]);
		// $body = $response->getBody();
		// if (strpos($response->header('content-type'), 'application/json') !== false) {
			// $body = json_decode($body);
		// }		
        // print_r('<pre>');
		// print_r($body);
		echo view('dashboard');
		
    }
	
	public function layout()
	{
		echo view('dashboard');
	}
}

<?php

namespace App\Controllers\Admin;

use CodeIgniter\RESTful\ResourceController;

class Services extends ResourceController
{
    protected $modelName = 'App\Models\ServiceModel';
    protected $format    = 'json';
    public function index()
    {
        return $this->respond($this->model->findAll());
    }

    public function show($id = null)
    {

    }

    public function new()
    {

    }

    public function create()
    {
  		$this->model->save(array(
			'title' => 'Troubleshoot',
			'description' => 'Layanan Troubleshoot',
			'parent_catid' => '2'
		));        
  		$this->model->save(array(
			'title' => 'Installasi Software',
			'description' => 'Layanan Installasi Software',
			'parent_catid' => '2'
		)); 		
		echo "<pre>";
		print_r($this->model->errors());   
    }

    public function edit($id = null)
    {
    
    }

    public function update($id = null)
    {
     
    }

    public function delete($id = null)
    {
     
    }
}

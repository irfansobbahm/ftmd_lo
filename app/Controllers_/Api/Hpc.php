<?php

namespace App\Controllers\Api;

use App\Controllers\BaseController;

class Hpc extends BaseController
{
	public function usersNode($nodeId = 0)
	{
		$hpcNodeUsersModel = model('App\Models\App\hpcNodeUsersModel');
		$username = (isset($_POST['username']) && !empty($_POST['username'])) ? $_POST['username'] : '';
		$data = $hpcNodeUsersModel->getUsersByNodeID($nodeId, $username);
		
		exit(json_encode($data));
	}
	
	public function osNodes($os_id)
	{
		$hpcNodesModel = model('App\Models\App\hpcNodesModel');
		
		$data = $hpcNodesModel->getByOSCat($os_id);
		
		exit(json_encode($data));
	}	
	
	public function getBookingUserNode()
	{
		extract($_POST);

		$hpcBookingModel = model('App\Models\App\hpcBookingModel');
		$data = $hpcBookingModel->getTicketUserNode($ticketId);
		
		exit(json_encode($data));
	}
	
	public function getBookedDatesAhead()
	{
		$hpcBookingModel = model('App\Models\App\hpcBookingModel');
		
		exit(json_encode($hpcBookingModel->getBookedPackage()));
		
		
		extract($_GET);
		$hpcBookingDatesModel = model('App\Models\App\hpcBookingDatesModel');
		$data = $hpcBookingDatesModel->getAllDatesBooked($package_id);
		$data_array = [];
		foreach($data as $row){
			$data_array[] = $row['date'];
		}
		exit(json_encode($data_array));
	}
	
	public function getBookedPackagePerDate($date = '')
	{
		$hpcBookingDatesModel = model('App\Models\App\hpcBookingDatesModel');
		$result = $hpcBookingDatesModel->getBookedDateDetail($date);
		
		exit(json_encode($result));
	}
	
	public function getBookedCalendar()
	{
		
		// $hpcBookingDatesModel = model('App\Models\App\hpcBookingDatesModel');
		// $result = $hpcBookingDatesModel->getBookedCalendar();
		
		$hpcBookingModel = model('App\Models\App\hpcBookingModel');
		$result = $hpcBookingModel->getBookedCalendar();
		
		exit(json_encode($result));
	}
	
	public function updateNode()
	{
		extract($_POST);
		$session = session();
		$result = [
			'status' => 1,
			'msg' => 'Node ip '.$data[0].' successfully updated.'
		];
		$hpcNodesModel = model('App\Models\App\hpcNodesModel');
		
		
		$hpcNodesModel->transBegin();
		$hpcNodesModel->updateNode(['ip', $data[0]], [
			'cores' => $data[1],
			'ram' => $data[2],
			'storage' => $data[3]
		]);
		if ($hpcNodesModel->transStatus() === false) {
			$hpcNodesModel->transRollback();
			$result = [
				'status' => 0,
				'msg' => 'Node ip '.$data[0].' fail to be update.'
			];			
			$session->setFlashdata('error', $result['msg']);
		} else {
			$session->setFlashdata('success', $result['msg']);
			$hpcNodesModel->transCommit();
		}		
		
		exit(json_encode($result));		
	}
}

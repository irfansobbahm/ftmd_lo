<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Hpc extends BaseController
{
    public function bookedSchedule()
    {
        return view('hpc_calendar');
    }

	// PAGE hpc-account.
	public function account()
	{
		$model = model('App\Models\App\hpcBookingModel');

		$session= session();
		
		return view('hpc_account',[
			'model' => $model,
			'data' => $model->getAllUserBookingHPC($session->get('username'))
		]);		
	}	
	
	// PAGE hpc-nodes.
	public function hpcNodes()
	{
		return view('hpc_nodes',[
			'linux' => model('App\Models\App\hpcNodesModel')->getNodesOS(2),
			'windows' => model('App\Models\App\hpcNodesModel')->getNodesOS(1)
		]);		
	}	
	public function hpcAddNodes()
	{
		return view('hpc_nodes',[
			'linux' => model('App\Models\App\hpcNodesModel')->getNodesOS(2),
			'windows' => model('App\Models\App\hpcNodesModel')->getNodesOS(1)
		]);		
	}	
	
	// PAGE hpc-software.
	public function hpcSoftware()
	{
		$model = model('App\Models\App\hpcSoftwareModel');
		
		return view('hpc_software',[
			'data' => [
				'windows' => $model->getSoftware(1),
				'linux' => $model->getSoftware(2)
			]
		]);		
	}	
	
	// PAGE new-software
	public function hpcNewSoftware($id = 0)
	{
		$model = model('App\Models\App\hpcSoftwareModel');
		$osList = model('App\Models\App\hpcOSModel')->findAll();

		return view('manage/hpc_form_software',[
			'osList' => $osList,
			'id' => $id,
			'data' => $model->getRow($id)	
		]);			
	}
	
	// PAGE save-software
	public function hpcSaveSoftware()
	{
		extract($_POST);
			$result = [
				'status' => 1,
				'msg' => 'Software succesfully saved.'
			];			
		$session = session();
		$model = model('App\Models\App\hpcSoftwareModel');
		$model->transBegin();
		$model->saveData($app_software_hpc);
		if ($model->transStatus() === false) {
			$model->transRollback();
			$result = [
				'status' => 0,
				'msg' => 'Fail to save Software.'
			];			
			$session->setFlashdata('error', $result['msg']);
		} else {
			$session->setFlashdata('success', $result['msg']);
			$model->transCommit();
		}

		return redirect()->to(base_role_url('hpc-software')); 
		
	}	
	
	// PAGE save-software
	public function hpcDeleteSoftware()
	{
		extract($_POST);
		$result = [
			'status' => 1,
			'msg' => 'Software name "'.$name.'" succesfully deleted.'
		];
		$session = session();
		$model = model('App\Models\App\hpcSoftwareModel');
		$model->transBegin();
		$model->deleteData($id);
		if ($model->transStatus() === false) {
			$model->transRollback();
			$result = [
				'status' => 0,
				'msg' => 'Fail to delete software.'
			];			
			$session->setFlashdata('error', $result['msg']);
		} else {
			$session->setFlashdata('success', $result['msg']);
			$model->transCommit();
		}		
	}	
	
	// PAGE hpc-package.
	public function hpcPackage()
	{
		$model = model('App\Models\App\hpcPackageModel');
		$data = $model->getPackages();
	
		return view('hpc_package',[
			'data' => $data
		]);		
	}	
	
	// PAGE new-package
	public function hpcNewPackage($id = 0)
	{
		$model = model('App\Models\App\hpcPackageModel');
		$osList = model('App\Models\App\hpcOSModel')->findAll();
		$data = $model->getRow($id);

		
		return view('manage/hpc_form_package',[
			'id' => $id,
			'osList' => $osList,
			'data' => $data
		]);			
	}
	
	// PAGE save-package
	public function hpcSavePackage()
	{
		extract($_POST);
			$result = [
				'status' => 1,
				'msg' => 'Package succesfully saved.'
			];			
		$session = session();
		$model = model('App\Models\App\hpcPackageModel');
		$model->transBegin();
		$model->saveData($app_package_hpc);
		if ($model->transStatus() === false) {
			$model->transRollback();
			$result = [
				'status' => 0,
				'msg' => 'Fail to save package.'
			];			
			$session->setFlashdata('error', $result['msg']);
		} else {
			$session->setFlashdata('success', $result['msg']);
			$model->transCommit();
		}

		return redirect()->to(base_role_url('hpc-package')); 
		
	}	
	
	// PAGE save-package
	public function hpcDeletePackage()
	{
		extract($_POST);
		$result = [
			'status' => 1,
			'msg' => 'Package name "'.$name.'" succesfully deleted.'
		];
		$session = session();
		$model = model('App\Models\App\hpcPackageModel');
		$model->transBegin();
		$model->deleteData($id);
		if ($model->transStatus() === false) {
			$model->transRollback();
			$result = [
				'status' => 0,
				'msg' => 'Fail to delete package.'
			];			
			$session->setFlashdata('error', $result['msg']);
		} else {
			$session->setFlashdata('success', $result['msg']);
			$model->transCommit();
		}		
	}
}

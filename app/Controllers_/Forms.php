<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Forms extends BaseController
{
    public function index()
    {
        echo view('forms');
    }
	
	public function create()
	{
		$FieldTypesModel = model('App\Models\App\FieldTypesModel');

		$session = session();
		
		echo view('create_form',[
			'types' => $FieldTypesModel->getTypes(),
			'session' => $session
		]);
	}
	
	public function save()
	{

		$UserForms = model('App\Models\App\UserForms');	
		$UserFormFields = model('App\Models\App\UserFormFields');

		echo "<pre>";
		print_r($_POST);
	exit;
		$UserForms->save($_POST['user_forms']);
		
		foreach($_POST['user_form_fields']['type'] as $i => $type)
		{
			$UserFormFields->save([
				'form_id' => $UserForms->insertID,
				'descr' => $_POST['user_form_fields']['descr'][$i],
				'type' => $type,
				'sub' => ''
			]);
		}
		
		return redirect()->to(base_role_url('forms/my-form'));
	}
	
	public function view($form_id='')
	{
		$UserForms = model('App\Models\App\UserForms');	
		$UserFormFields = model('App\Models\App\UserFormFields');
		
		$userForm = $UserForms->getForm($form_id);

		$userFormFields = $UserFormFields->getFields($form_id);

		echo view('view_form',[
			'form' => $userForm,
			'fields' => $userFormFields
		]);
	}
	
	public function myForms()
	{
		$UserForms = model('App\Models\App\UserForms');
		$UserFormFields = model('App\Models\App\UserFormFields');

		$session = session();
		
		$userForms = $UserForms->getAll($session->get('id'));

		echo view('my_forms',[
			'userForms' => $userForms,
			'session' => $session
		]);		
	}	
}

<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Pages extends BaseController
{
    protected $modelName = 'App\Models\MenuModel';
	private $model;
	public function __construct()
	{
		$this->model = model($this->modelName);
	}

    public function index()
    {
		$data = $this->model->getMenus('',1);

		return view('manage/page_guide',[
			'data' => $this->model->menus
		]);		
    }

	public function getPageGuide($page_id = 0)
	{
		$guideModel = model('App\Models\PageGuideModel');
		
		$guideModel->where(['page_id'=>$page_id]);
		$content = $guideModel->first();
		
		exit(json_encode($content));
	}
	
	public function savePageGuide()
	{
		extract($_POST);
		
		$guideModel = model('App\Models\PageGuideModel');
		$guideModel->save($page_guide);
		
		$this->model->save([
			'id' =>$page_guide['page_id'],
			'has_guide' => '1'
		]);
		
		session()->setFlashdata('success', 'Page Guide Saved.');
	}	
}

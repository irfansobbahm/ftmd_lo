<?php

namespace App\Controllers;

class Tickets extends BaseController
{
    protected $modelName = 'App\Models\TicketModel';
    protected $format    = 'json';
	public $menuModel;
	public function __construct()
	{
		$this->menuModel = model('MenuModel');
	}
	
	private function writeTicketLog(array $data)
	{
		$ticketLogsModel = model('App\Models\App\TicketLogsModel');
		$ticketLogsModel->save($data);		
	}
	
    public function index()
    {
        return redirect()->back();
    }

    public function show($id = null)
    {
        
    }

    public function new()
    {

    }

	public function open()
	{
		$filter = isset($_GET) ? $_GET : '';
		return view('ticket_views/ticket_open',[
			'state' => 'action',
			'data' => model('TicketModel')->agentTickets('', session()->get('role'), 'open', $filter)
		]);
	}
	
	public function inProgress()
	{
		$filter = isset($_GET) ? $_GET : '';
		return view('ticket_views/ticket_inprogress',[
		'state' => 'action',
			'data' => model('TicketModel')->agentTickets(session()->get('username'),session()->get('role'), 'inprogress', $filter)
		]);
	}
	
	public function pending()
	{
		$filter = isset($_GET) ? $_GET : '';
		return view('ticket_views/ticket_pending',[
			'state' => 'action',
			'data' => model('TicketModel')->agentTickets(session()->get('username'), session()->get('role'), 'pending', $filter)
		]);
	}
	
	public function closed()
	{
		$filter = isset($_GET) ? $_GET : '';
		return view('ticket_views/ticket_closed',[
			'state' => 'action',
			'data' => model('TicketModel')->agentTickets(session()->get('username'), session()->get('role'), 'closed', $filter)
		]);
	}
	
	public function myTickets()
	{
		$filter = isset($_GET) ? $_GET : '';
		return view('ticket_views/my_tickets',[
			'data' => model('TicketModel')->userTickets($filter, session()->get('username'))
		]);
	}
	
	public function allTicket()
	{
		$filter = isset($_GET) ? $_GET : '';
		return view('ticket_views/ticket_all',[
			'state' => 'action',
			'data' => model('TicketModel')->allTicket($filter)
		]);
	}
	
	public function ticketDetail($identifier = '')
	{
		$username = session()->get('username');
		$ticketDetail = model('TicketModel')->ticketDetail($identifier, $username);
		$ticketDetail['logs'] = model('TicketLogsModel')->ticketLogs($identifier);
		// echo "<pre>";
		// print_r($ticketDetail);
		// exit;
		$vars = [
			'ticket_identifier'=> $identifier,
			'ticket' => $ticketDetail,
			'agents' => model('UserModel')->getAgents()
		];
		
		$this->serviceParams($vars, $identifier);
		
		return view('ticket_detail', $vars);
	}
	
	private function serviceParams(&$vars, $ticket_identifier)
	{
		extract($vars);
		$params = [];
		switch($ticket['service_slug']){
			case 'hpc-booking': // vars for HPC Booking form.
				$commentsModel = model('App\Models\App\CommentsModel');
				$hpcNodesModel = model('App\Models\App\hpcNodesModel');
				$params = [
					'comments' => $commentsModel->getTicketComments($ticket_identifier, ((session()->get('role') != 'user') ? 1:0)),
					'defaultNodes' => $this->defaultNodes($vars),
				];
			break;
		}
		
		$vars = array_merge($vars, $params);
	}
	
	private function defaultNodes(&$vars)
	{
		$hpcNodesModel = model('App\Models\App\hpcNodesModel');
		$hpcNodeUsersModel = model('App\Models\App\hpcNodeUsersModel');
		$nodes = $hpcNodesModel->getByOSCat($vars['ticket']['service']['os_id']);
		
		// SELECT DEFAULT VALUE NODE USER.
		if($vars['ticket']['service']['node_user_id'] == 0)
		{
			if($vars['ticket']['service']['os_id'] == 1)
			{
				$default = $hpcNodeUsersModel->getUsersByNodeID($nodes[0]['id'])[0];
				$vars['ticket']['service']['os_user'] = $vars['ticket']['service']['booking_status'] == 'new' ? $default['user']:'';
				$vars['ticket']['service']['os_password'] = $vars['ticket']['service']['booking_status'] == 'new' ? $default['password']:'';
				
			}			
			if($vars['ticket']['service']['os_id'] == 2){
				$vars['ticket']['service']['os_user'] = $vars['ticket']['username'];
			}
			$vars['ticket']['service']['node_ip'] = $nodes[0]['ip'];
		}
		$newNodes = [];
		foreach($nodes as $node)
		{
			$newNodes[$node['ip']] = $node;
		}
		// echo "<pre>";
		// print_r($vars);
		// exit;
		return $newNodes;
	}
	
	public function lockTicket($identifier = '')
	{
		$session = session();
		$username = $session->get('username');
		$ticketLock = model('TicketModel')->ticketLock($identifier, $username);
		if(!$ticketLock){
			return redirect()->back()->with('error', "Can't <b>lock</b> the ticket, it may another agent already lock the ticket."); 
		}
		// WRITE TICKET LOG
		$this->writeTicketLog([
			'ticket_identifier' => $identifier,
			'action' => 'lock_ticket',
			'actor' => $session->get('username'),
			'actor_role' => $session->get('role')
		]);
		return redirect()->to(base_url($session->get('role').'/ticket/'.$identifier.'/view-detail')); 
	}
	public function unlockTicket($identifier = '')
	{
		$session = session();
		$username = $session->get('username');
		$ticketUnlock = model('TicketModel')->ticketUnlock($identifier, $username);
		if(!$ticketUnlock){
			return redirect()->back()->with('error', "Can't <b>unlock</b> the ticket, contact system administrator."); 
		}
		// WRITE TICKET LOG
		$this->writeTicketLog([
			'ticket_identifier' => $identifier,
			'action' => 'unlock_ticket',
			'actor' => $session->get('username'),
			'actor_role' => $session->get('role')
		]);
		return redirect()->to(base_url($session->get('role').'/ticket/'.$identifier.'/view-detail')); 
	}	
	
	public function saveProgressTicket($identifier = '')
	{

		// echo "<pre>";
		// print_r($_POST);
		// exit;

		extract($_POST);
		$session = session();	
		$this->db = \Config\Database::connect(); 
		$this->db->transStart();

		// BOOKING HPC CLOSE TICKET DEFAULT SERVICE STATUS.
		if($action == 'close_ticket'){
			
			try{
				
				if(isset($service_model) && !empty($service_model))
				{
					$serviceModel = model($service_model);
					if($serviceModel->table == 'app_booking_hpc' && @${$serviceModel->table}['booking_status'] == 'new'){
						$serviceModel->changeStatus($identifier,'rejected');
					}
				}
			}catch(\Exception $e) {
				exit($e->getMessage()); 
			}			
		}

		model('TicketModel')->saveTicketAction($identifier,$data); 

		// WRITE TICKET LOG.
		$this->writeTicketLog([
			'ticket_identifier' => $identifier,
			'action' => $action,
			'actor' => $session->get('username'),
			'actor_role' => $session->get('role')
		]);			
		
		if($action == 're_open_ticket'){
			try{
				if(isset($service_model) && !empty($service_model))
				{
					$serviceModel = model($service_model);
					
					// on re-open ticket for service HPC Booking.
					if($serviceModel->table == 'app_booking_hpc'){
						$serviceModel->changeStatus($identifier,'new');
					}
				}
			}catch(\Exception $e) {
				exit($e->getMessage()); 
			}
		}
		

		
		if($action == 'service_action_cancel' || $action == 'service_action_reject'){
			$serviceModel = model($service_model);
			// CHANGE SERVICE BOOKING STATUS CANCEL / REJECT.
			if(isset(${$serviceModel->table}) && !empty(${$serviceModel->table}))
			{
				//print_r(${$serviceModel->table});exit;
				$serviceModel->set('booking_status', ${$serviceModel->table}['booking_status']);
				if($action == 'service_action_reject'){
					$serviceModel->set('node_user_id', 0);
				}
				$serviceModel->where('ticket_identifier', $identifier);
				$serviceModel->update();
				//exit($serviceModel->getLastQuery());
				$this->db->transComplete();
				if($this->db->transStatus()){
					return redirect()->back()->with('success', "Booking Has Been ".(($action == 'service_action_cancel')?'Canceled.':'Rejected.'));
				}else{
					return redirect()->back()->with('error', "Can not <b>save</b> ticket information, contact system administrator.");
				}					
			}			
		}
		
		if($action == 'change_progress_ticket')
		{
			if(isset($service_model) && !empty($service_model))
			{
				$serviceModel = model($service_model);
							// echo "<pre>";
							// print_r($_POST);
							// exit;	
							
							
							
				// SAVE USER NODE IF NOT EXISTS(HPC BOOKING SERVICE).
				if(isset($app_booking_hpc) && !empty($app_booking_hpc))
				{
					${$serviceModel->table}['node_id'] = $_POST['app_hpc_node_users']['node_id'];
					if(isset($app_hpc_node_users) && !empty($app_hpc_node_users))
					{
						$app_hpc_node_users['status'] = 'inactive';
												
						if(!$serviceModel->checkNodeUserExists($app_hpc_node_users['node_id'], $app_hpc_node_users['user'])){
							${$serviceModel->table}['node_user_id'] = $serviceModel->saveNewNodeUser($app_hpc_node_users);
						}else{
							//${$serviceModel->table}['node_user_id']
							$serviceModel->updateNodeUser($app_hpc_node_users['id'],[
								'password' => $app_hpc_node_users['password']
							]);
						}
					}
				}
				
				// SAVE SERVICE DATA.
				if(isset(${$serviceModel->table}) && !empty(${$serviceModel->table})){
					$serviceModel->transStart();

					foreach(${$serviceModel->table} as $col => $val){
						$serviceModel->set($col, $val);
					}
					$serviceModel->set('node_pass', $app_hpc_node_users['password']);
					$serviceModel->where('ticket_identifier', $identifier);
					$serviceSave = $serviceModel->update();
					$serviceModel->transComplete();
					if($serviceModel->transStatus())
					{
						if(${$serviceModel->table}['booking_status'] == 'approved')
						{
							// WRITE TICKET LOG.
							$this->writeTicketLog([
								'ticket_identifier' => $identifier,
								'action' => 'service_action_approve',
								'actor' => $session->get('username'),
								'actor_role' => $session->get('role')
							]);							
						}
					}					
				}
			}			
		}

		$this->db->transComplete();
		if($this->db->transStatus()){
			return redirect()->back()->with('success', "Ticket Successfully Saved.");
		}else{
			return redirect()->back()->with('error', "Can not <b>save</b> ticket, contact system administrator.");
		}	
	}
	
	public function sendTicketComment($identifier = '')
	{
		extract($_POST);
		if(isset($data) && !empty($data) && $action == 'send_ticket_comment'){
			$data['user'] = session()->get('username');		
			$commentModel = model('CommentsModel');
			$commentModel->transStart();
			$commentModel->save($data);
			$commentModel->transComplete();
			if($commentModel->transStatus())
			{
				return redirect()->back(); //->with('success', "Comments Successfully Saved.");	
			}
		}
		return redirect()->back()->with('error', "Can not send comment, contact your system administrator.");
	}
	
	public function hpcBooking($service_id = '876ee1cc-7d82-4d8e-a9de-70a7df148c84')
	{
		$serviceModel = model('ServiceModel');
		return view('form_ticket',[
			'service' => $serviceModel->find($service_id),
			'model' => [
				'bookingPurpose' => model('App\Models\App\hpcBookingPurposeModel')->findAll(),
				'package' => model('App\Models\App\hpcPackageModel')->findAll(),
				'os'=> model('App\Models\App\hpcOSModel')->getOSandSoftware(),
				'software'=> model('App\Models\App\hpcSoftwareModel')->findAll()
			]
		]);
	}
	
	public function newTicket()
	{
		return view('form_ticket',[
			'service' => model('ServiceModel')->getServiceByCurrentURLSlug(current_url(true))
		]);
	}
	
    public function create()
    {
		$ticketModel = model($this->modelName);
		$hpcBookingModel = model('App\Models\App\hpcBookingModel');
		$hpcBookingDatesModel = model('App\Models\App\hpcBookingDatesModel');
		$this->db = \Config\Database::connect(); 
		$this->db->transBegin();
		try {
			// save data.
			$_POST['data']['owner'] = session()->get('username');
			$ticketModel->save($_POST['data']);
			$ticket = $ticketModel->find($ticketModel->insertID);
			if((isset($_POST['service_table']) && !empty($_POST['service_table'])))
			{
				//// BOOKING DATE FORM MODE RANGE
				$booking_date = explode(' to ',$_POST['booking_date']);
				
				$_POST[$_POST['service_table']]['ticket_identifier'] = $ticket['identifier'];
							
				//$_POST[$_POST['service_table']]['booking_dates'] = $_POST['booking_date'];
				//// BOOKING DATE FORM MODE RANGE
				$_POST[$_POST['service_table']]['booking_start_date'] = trim($booking_date[0]);
				$_POST[$_POST['service_table']]['booking_end_date'] = (isset($booking_date[1]) && !empty($booking_date[1])) ? trim($booking_date[1]):trim($booking_date[0]);
				
				
				$hpcBookingModel->save($_POST[$_POST['service_table']]);
				
				$_date = explode(' to ', $_POST['booking_date']);

				$enddate = count($_date) > 1 ? new \DateTime($_date[1]) : new \DateTime($_date[0]);
				$period = new \DatePeriod(
					 new \DateTime($_date[0].' 23:59:59'),
					 new \DateInterval('P1D'),
					 $enddate->modify('+1 day')
				);
				foreach ($period as $key => $value) {
					$bookedDate = [
						'booking_hpc_id' => $hpcBookingModel->insertID,
						'ticket_identifier' => $ticket['identifier'],
						'date' => $value->format('Y-m-d')
					];
					$hpcBookingDatesModel->save($bookedDate);					
				}	

				// mode MULTIPLE DATE
				// $booking_dates = explode(',',$_POST['booking_date']);
				// foreach($booking_dates as $date)
				// {
					// $bookedDate = [
						// 'booking_hpc_id' => $hpcBookingModel->insertID,
						// 'ticket_identifier' => $ticket['identifier'],
						// 'date' => $date
					// ];
					// $hpcBookingDatesModel->save($bookedDate);
				// }				

				
				$hpcBooking = $hpcBookingModel->find($hpcBookingModel->insertID);
				
				$message = [
					'doc' => [
						'mime_in' => 'Document must be PDF file.'
					],
				];				
				$validationRule = [
					'doc' => [
						'label' => 'PDF File',
						'rules' => 'mime_in[doc,application/pdf]',
					],
				];
				
				if($this->validate($validationRule, $message)){
					$doc = $this->request->getFile('doc');
					$doc->move(ROOTPATH . 'public/docs/',$hpcBooking['doc_id'].'.pdf');
				}else{
					$errors = $this->validator->getErrors();
					
					return redirect()->back()->withInput()->with('error', $errors); 
				}
			}
			
			// WRITE TICKET LOG AND COMMIT TRANSACTION
			$session = session();
			$this->writeTicketLog([
				'ticket_identifier' => $ticket['identifier'],
				'action' => 'post_new_ticket',
				'actor' => $session->get('username'),
				'actor_role' => $session->get('role')
			]);
			$this->db->transCommit();	

			return redirect()->route('my-tickets')->with('success', 'New Ticket Successfully Created with Ticket #'.$ticket['identifier']);			
			
		} catch (\Exception $e) {
			
			$this->db->transRollback();

			return redirect()->back()->withInput()->with('error', $e->getMessage()); 
		}
    }

    public function edit($id = null)
    {
        
    }

    public function update($id = null)
    {
        
    }

    public function delete($id = null)
    {
        
    }
}

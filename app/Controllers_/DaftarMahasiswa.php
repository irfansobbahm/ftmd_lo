<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class DaftarMahasiswa extends BaseController
{
    public function index($kd_kuliah='', $tahun ='', $semester = '')
    {
		$this->rekapNilai($kd_kuliah, $tahun, $semester);
	}	
    public function rekapNilai($kd_kuliah='', $tahun='', $semester='')
    {
		$kd_kuliah = empty($kd_kuliah) ? 'MT3103' : $kd_kuliah;
		$tahun = empty($tahun) ? date('Y') : $tahun;
		$semester = empty($semester) ? date('n') : $semester;
		
		$nip_nim = session('nip_nim');
		
		$client = \Config\Services::curlrequest();
		$response = $client->request('GET', 'https://ws.akademik.itb.ac.id/kelas_pengajar?nip_dosen=eq.'.$nip_nim, [
			'headers' => [
				'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZnRtZF9zaWV2YSIsImV4cCI6MTcwNTI1MTYwMH0.LJbHj0QK1Y5IFR7AIn4cVIL3zgCrA8TpyrxTOjDg4IQ',
				'Accept-Profile' => 'v_ftmd',
			],
		]);	
		
		if (strpos($response->header('content-type'), 'application/json') !== false)
		{
			$ws_kelas_pengajar = json_decode($response->getBody());
			$matakuliah = function($ws_kelas_pengajar)
			{
				$matakuliah = [];
				foreach($ws_kelas_pengajar as $pengajar)
				{
					$matakuliah[$pengajar->kd_kuliah] = $pengajar->nama_mk->id;
				}
				return $matakuliah;
			};
			$tahunkuliah = function($ws_kelas_pengajar)
			{
				$tahunkuliah = [];
				foreach($ws_kelas_pengajar as $pengajar)
				{
					$tahunkuliah[$pengajar->tahun] = $pengajar->tahun;
				}
				return $tahunkuliah;
			};
			$mahasiswa = function($kd_kuliah='',$tahun='',$semester='',$nim='',$nama='',$kelas=''){
				
				
				//$semester = empty($semester) ? date('n') : $semester;
				
				$client = \Config\Services::curlrequest();

				$response = $client->request('GET', 'https://ws.akademik.itb.ac.id/dpk', [
					'query' =>[
						'kd_kuliah' => 'eq.'.$kd_kuliah,
						'tahun' => 'eq.'.$tahun,
						'nim' => 'like.*'.$nim.'*',
						'nama' => 'like.*'.$nama.'*'
					],
					'headers' => [
						'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZnRtZF9zaWV2YSIsImV4cCI6MTcwNTI1MTYwMH0.LJbHj0QK1Y5IFR7AIn4cVIL3zgCrA8TpyrxTOjDg4IQ',
						'Accept-Profile' => 'v_ftmd',
					],
				]);	
				if (strpos($response->header('content-type'), 'application/json') !== false)
				{
					return json_decode($response->getBody());
				}else{
					return [];
				}
			};
			
			$uri = new \CodeIgniter\HTTP\URI(current_url());
			$request = \Config\Services::request();
			
			echo view('lo/daftar_mahasiswa',[
				'uriSegments' => $uri->getSegments(),
				'ws' => [
					'kelas_pengajar' => $ws_kelas_pengajar,
					'matakuliah' => $matakuliah($ws_kelas_pengajar),
					'tahunkuliah' => $tahunkuliah($ws_kelas_pengajar),
					'mahasiswa' => $mahasiswa($kd_kuliah, $tahun, $semester, $request->getVar('nim'),$request->getVar('nama'),$kelas='')
				],
				
			]);
		}
    }

    public function nilai($kd_kuliah='', $tahun='', $semester='')
    {
		$uri = new \CodeIgniter\HTTP\URI(uri_string());
		
		$kategoriNilai = $uri->getSegment(3);
		
		$kategoriNilaiString = strtoupper($kategoriNilai);

		if(empty($tahun))
		{
			return redirect()->to(base_role_url('kategori-nilai/'.$kategoriNilai.'/'.'MT3103/'.date('Y').'/ganjil'));
			//exit;
		}

		$kd_kuliah = empty($kd_kuliah) ? 'MT3103' : $kd_kuliah;
		$tahun = empty($tahun) ? date('Y') : $tahun;
		$semester = empty($semester) ? date('n') : $semester;
		
		$nip_nim = session('nip_nim');
		

		$client = \Config\Services::curlrequest();
		$response = $client->request('GET', 'https://ws.akademik.itb.ac.id/kelas_pengajar?nip_dosen=eq.'.$nip_nim, [
			'headers' => [
				'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZnRtZF9zaWV2YSIsImV4cCI6MTcwNTI1MTYwMH0.LJbHj0QK1Y5IFR7AIn4cVIL3zgCrA8TpyrxTOjDg4IQ',
				'Accept-Profile' => 'v_ftmd',
			],
		]);	
		
		if (strpos($response->header('content-type'), 'application/json') !== false)
		{
			$ws_kelas_pengajar = json_decode($response->getBody());
			$matakuliah = function($ws_kelas_pengajar)
			{
				$matakuliah = [];
				foreach($ws_kelas_pengajar as $pengajar)
				{
					$matakuliah[$pengajar->kd_kuliah] = $pengajar->nama_mk->id;
				}
				return $matakuliah;
			};
			$tahunkuliah = function($ws_kelas_pengajar)
			{
				$tahunkuliah = [];
				foreach($ws_kelas_pengajar as $pengajar)
				{
					$tahunkuliah[$pengajar->tahun] = $pengajar->tahun;
				}
				return $tahunkuliah;
			};
			$mahasiswa = function($kd_kuliah='',$tahun='',$semester='',$nim='',$nama='',$kelas=''){
				
				
				//$semester = empty($semester) ? date('n') : $semester;
				
				$client = \Config\Services::curlrequest();

				$response = $client->request('GET', 'https://ws.akademik.itb.ac.id/dpk', [
					'query' =>[
						'kd_kuliah' => 'eq.'.$kd_kuliah,
						'tahun' => 'eq.'.$tahun,
						'nim' => 'like.*'.$nim.'*',
						'nama' => 'like.*'.$nama.'*'
					],
					'headers' => [
						'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZnRtZF9zaWV2YSIsImV4cCI6MTcwNTI1MTYwMH0.LJbHj0QK1Y5IFR7AIn4cVIL3zgCrA8TpyrxTOjDg4IQ',
						'Accept-Profile' => 'v_ftmd',
					],
				]);	
				
				if (strpos($response->header('content-type'), 'application/json') !== false)
				{
					return json_decode($response->getBody());
				}else{
					return [];
				}
			};
			
			$uri = new \CodeIgniter\HTTP\URI(current_url());
			$request = \Config\Services::request();
			
			echo view('lo/kategori_nilai',[
				'catNilai' => $kategoriNilaiString,
				'catNilaiSlug' =>$kategoriNilai,
				'uriSegments' => $uri->getSegments(),
				'ws' => [
					'kelas_pengajar' => $ws_kelas_pengajar,
					'matakuliah' => $matakuliah($ws_kelas_pengajar),
					'tahunkuliah' => $tahunkuliah($ws_kelas_pengajar),
					'mahasiswa' => $mahasiswa($kd_kuliah, $tahun, $semester, $request->getVar('nim'),$request->getVar('nama'),$kelas='')
				],
				
			]);
		}

    }
	
    // public function uas($kd_kuliah='', $tahun='', $semester='')
    // {

		// $uri = new \CodeIgniter\HTTP\URI(uri_string());

		// $kategoriNilaiString = strtoupper($uri->getSegment(3));

		// if(empty($tahun))
		// {
			// return redirect()->to(base_role_url('kategori-nilai/'.$uri->getSegment(3).'/'.'MT3103/'.date('Y').'/ganjil'));
			// //exit;
		// }

		// $kd_kuliah = empty($kd_kuliah) ? 'MT3103' : $kd_kuliah;
		// $tahun = empty($tahun) ? date('Y') : $tahun;
		// $semester = empty($semester) ? date('n') : $semester;
		
		// $nip_nim = session('nip_nim');
		

		// $client = \Config\Services::curlrequest();
		// $response = $client->request('GET', 'https://ws.akademik.itb.ac.id/kelas_pengajar?nip_dosen=eq.'.$nip_nim, [
			// 'headers' => [
				// 'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZnRtZF9zaWV2YSIsImV4cCI6MTcwNTI1MTYwMH0.LJbHj0QK1Y5IFR7AIn4cVIL3zgCrA8TpyrxTOjDg4IQ',
				// 'Accept-Profile' => 'v_ftmd',
			// ],
		// ]);	
		
		// if (strpos($response->header('content-type'), 'application/json') !== false)
		// {
			// $ws_kelas_pengajar = json_decode($response->getBody());
			// $matakuliah = function($ws_kelas_pengajar)
			// {
				// $matakuliah = [];
				// foreach($ws_kelas_pengajar as $pengajar)
				// {
					// $matakuliah[$pengajar->kd_kuliah] = $pengajar->nama_mk->id;
				// }
				// return $matakuliah;
			// };
			// $tahunkuliah = function($ws_kelas_pengajar)
			// {
				// $tahunkuliah = [];
				// foreach($ws_kelas_pengajar as $pengajar)
				// {
					// $tahunkuliah[$pengajar->tahun] = $pengajar->tahun;
				// }
				// return $tahunkuliah;
			// };
			// $mahasiswa = function($kd_kuliah='',$tahun='',$semester='',$nim='',$nama='',$kelas=''){
				
				
				// //$semester = empty($semester) ? date('n') : $semester;
				
				// $client = \Config\Services::curlrequest();

				// $response = $client->request('GET', 'https://ws.akademik.itb.ac.id/dpk', [
					// 'query' =>[
						// 'kd_kuliah' => 'eq.'.$kd_kuliah,
						// 'tahun' => 'eq.'.$tahun,
						// 'nim' => 'like.*'.$nim.'*',
						// 'nama' => 'like.*'.$nama.'*'
					// ],
					// 'headers' => [
						// 'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZnRtZF9zaWV2YSIsImV4cCI6MTcwNTI1MTYwMH0.LJbHj0QK1Y5IFR7AIn4cVIL3zgCrA8TpyrxTOjDg4IQ',
						// 'Accept-Profile' => 'v_ftmd',
					// ],
				// ]);	
				
				// if (strpos($response->header('content-type'), 'application/json') !== false)
				// {
					// return json_decode($response->getBody());
				// }else{
					// return [];
				// }
			// };
			
			// $uri = new \CodeIgniter\HTTP\URI(current_url());
			// $request = \Config\Services::request();
			
			// echo view('lo/kategori_nilai',[
				// 'catNilai' => $kategoriNilaiString,
				// 'uriSegments' => $uri->getSegments(),
				// 'ws' => [
					// 'kelas_pengajar' => $ws_kelas_pengajar,
					// 'matakuliah' => $matakuliah($ws_kelas_pengajar),
					// 'tahunkuliah' => $tahunkuliah($ws_kelas_pengajar),
					// 'mahasiswa' => $mahasiswa($kd_kuliah, $tahun, $semester, $request->getVar('nim'),$request->getVar('nama'),$kelas='')
				// ],
				
			// ]);
		// }

    // }

    public function uts($kd_kuliah='', $tahun='', $semester='')
    {

		$model = model('App\Models\App\DaftarMahasiswaModel');
		$data = $model->getMahasiswaDosen('ony_arifianto');
		// echo "<pre>";
		// print_r($data);
		// exit;
        echo view('lo/kategori_nilai',[
			'cat' => 'uts',
			'daftar_mahasiswa' => $data
		]);
    }

    public function praktikum()
    {
		$model = model('App\Models\App\DaftarMahasiswaModel');
		$data = $model->getMahasiswaDosen('ony_arifianto');
		// echo "<pre>";
		// print_r($data);
		// exit;
        echo view('lo/kategori_nilai',[
			'cat' => 'praktikum',
			'daftar_mahasiswa' => $data
		]);
    }
    public function kehadiran()
    {
		$model = model('App\Models\App\DaftarMahasiswaModel');
		$data = $model->getMahasiswaDosen('ony_arifianto');
		// echo "<pre>";
		// print_r($data);
		// exit;
        echo view('lo/kategori_nilai',[
			'cat' => 'kehadiran',
			'daftar_mahasiswa' => $data
		]);
    }
    public function quiz()
    {
		$model = model('App\Models\App\DaftarMahasiswaModel');
		$data = $model->getMahasiswaDosen('ony_arifianto');
		// echo "<pre>";
		// print_r($data);
		// exit;
        echo view('lo/kategori_nilai',[
			'cat' => 'quiz',
			'daftar_mahasiswa' => $data
		]);
    }
    public function pr()
    {
		$model = model('App\Models\App\DaftarMahasiswaModel');
		$data = $model->getMahasiswaDosen('ony_arifianto');
		// echo "<pre>";
		// print_r($data);
		// exit;
        echo view('lo/kategori_nilai',[
			'cat' => 'pr',
			'daftar_mahasiswa' => $data
		]);
    }
	
}

<?php

if(!function_exists('slugify'))
{
	function slugify($text, string $divider = '-')
	{
	  // replace non letter or digits by divider
	  $text = preg_replace('~[^\pL\d]+~u', $divider, $text);

	  // transliterate
	  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

	  // remove unwanted characters
	  $text = preg_replace('~[^-\w]+~', '', $text);

	  // trim
	  $text = trim($text, $divider);

	  // remove duplicate divider
	  $text = preg_replace('~-+~', $divider, $text);

	  // lowercase
	  $text = strtolower($text);

	  if (empty($text)) {
		return 'n-a';
	  }

	  return $text;
	}	
}

if(!function_exists('uri_save_ticket_data'))
{
	function uri_save_ticket_data(){
		return base_url(session()->get('role').'/save-ticket-data');
	}
}

if(!function_exists('date_diff_words'))
{
	function date_diff_words($datestart, $dateend)
	{
		// Declare and define two dates
		$date1 = strtotime($datestart);
		$date2 = strtotime($dateend);
		$containDiffs = [];
		
		// Formulate the Difference between two dates
		$diff = abs($date2 - $date1);

		// To get the year divide the resultant date into
		// total seconds in a year (365*60*60*24)
		$years = floor($diff / (365*60*60*24));
		$containDiffs['years'] = $years;
		
		// To get the month, subtract it with years and
		// divide the resultant date into
		// total seconds in a month (30*60*60*24)
		$months = floor(($diff - $years * 365*60*60*24)
									 / (30*60*60*24));
		$containDiffs['months'] = $months;

		// To get the day, subtract it with years and
		// months and divide the resultant date into
		// total seconds in a days (60*60*24)
		$days = floor(($diff - $years * 365*60*60*24 -
				   $months*30*60*60*24)/ (60*60*24));
		$containDiffs['days'] = $days;

		// To get the hour, subtract it with years,
		// months & seconds and divide the resultant
		// date into total seconds in a hours (60*60)
		$hours = floor(($diff - $years * 365*60*60*24
			 - $months*30*60*60*24 - $days*60*60*24)
										 / (60*60));
		$containDiffs['hours'] = $hours;

		// To get the minutes, subtract it with years,
		// months, seconds and hours and divide the
		// resultant date into total seconds i.e. 60
		$minutes = floor(($diff - $years * 365*60*60*24
			   - $months*30*60*60*24 - $days*60*60*24
								- $hours*60*60)/ 60);
		$containDiffs['minutes'] = $minutes;

		// To get the minutes, subtract it with years,
		// months, seconds, hours and minutes
		$seconds = floor(($diff - $years * 365*60*60*24
			   - $months*30*60*60*24 - $days*60*60*24
					  - $hours*60*60 - $minutes*60));
		
		$containDiffs['seconds'] = $seconds;
		$revDiffs = array_reverse($containDiffs);
		
		//print_r($revDiffs);
		
		$prevKey = 'a';
		$prevVal = 'a second';
		foreach($revDiffs as $key => $dtdiff){
			if($dtdiff == 0){
				return $prevVal." $prevKey ago";
			}
			$prevKey = $key;
			$prevVal = $dtdiff;
			//$prevVal = ($prevVal == 1) ? 'a':$prevVal;
		}
		
		return $prevVal." $prevKey ago";
	}
}

if(!function_exists('countAllUsers'))
{
	function countAllUsers()
	{
		$ticketModel = model('UserModel');

		return $ticketModel->count_allUsers();
	}
}

if(!function_exists('countAllNewUserToday'))
{
	function countAllNewUserToday()
	{
		$ticketModel = model('UserModel');

		return $ticketModel->count_allNewUserToday();
	}
}

if(!function_exists('countAllUserSinceLastWeek'))
{
	function countAllUserSinceLastWeek()
	{
		$ticketModel = model('UserModel');

		return $ticketModel->count_allUserSinceLastWeek();
	}
}

if(!function_exists('count_ticket'))
{
	function count_ticket(object $currMenu)
	{
		$menuModel = model('MenuModel');
		$ticketModel = model('TicketModel');
		$menuHasBadge = $menuModel->getMenuByBadgeHelperName(__FUNCTION__);
		$countsTicket = $ticketModel->countAllTicket(session()->get('username'), session()->get('role'));
		$slug_badges = [];
		foreach($menuHasBadge as $badge){
			$slug_badges[$badge->slug] = '';
		}
		// echo "<pre>";
		// print_r($slug_badges);
		// print_r(array_merge($slug_badges, $countsTicket));
		// print_r($menuHasBadge);
		// exit;
		return $countsTicket[$currMenu->slug];
	}
}

if(!function_exists('countAllOpenTicket'))
{
	function countAllOpenTicket()
	{
		$ticketModel = model('TicketModel');
		
		return $ticketModel->count_agentTickets('', session()->get('role'), 'open');
	}
}

if(!function_exists('countAllTicketsToday'))
{
	function countAllTicketsToday()
	{
		$ticketModel = model('TicketModel');

		return $ticketModel->count_allTicketToday();
	}
}

if(!function_exists('countAllTicketsSinceYesterday'))
{
	function countAllTicketsSinceYesterday()
	{
		$ticketModel = model('TicketModel');

		return $ticketModel->count_allTicketSinceYesterday();
	}
}

if(!function_exists('countAllTicketSinceLastWeek'))
{
	function countAllTicketSinceLastWeek()
	{
		$ticketModel = model('TicketModel');

		return $ticketModel->count_allTicketSinceLastWeek();
	}
}

if(!function_exists('total_open_ticket'))
{
	function total_open_ticket(object $currMenu, $ticketModel = null)
	{
		$ticketModel = (is_null($ticketModel)) ? model('TicketModel') : $ticketModel;
		
		return $ticketModel->count_agentTickets('', session()->get('role'), 'open');
	}
}
if(!function_exists('total_inprogress_ticket'))
{
	function total_inprogress_ticket(object $currMenu, $ticketModel = null)
	{
		$ticketModel = (is_null($ticketModel)) ? model('TicketModel') : $ticketModel;
		
		return $ticketModel->count_agentTickets(session()->get('username'),session()->get('role'), 'inprogress');
	}
}

if(!function_exists('total_pending_ticket'))
{
	function total_pending_ticket(object $currMenu, $ticketModel = null)
	{
		$ticketModel = (is_null($ticketModel)) ? model('TicketModel') : $ticketModel;
		
		return $ticketModel->count_agentTickets(session()->get('username'),session()->get('role'), 'pending');
	}
}

if(!function_exists('total_closed_ticket'))
{
	function total_closed_ticket(object $currMenu, $ticketModel = null)
	{
		$ticketModel = (is_null($ticketModel)) ? model('TicketModel') : $ticketModel;
		
		return $ticketModel->count_agentTickets(session()->get('username'), session()->get('role'), 'closed');
	}
}

if(!function_exists('total_my_ticket'))
{
	function total_my_ticket(object $currMenu, $ticketModel = null)
	{
		$ticketModel = (is_null($ticketModel)) ? model('TicketModel') : $ticketModel;
		
		return $ticketModel->count_userTickets(session()->get('username'));
	}
}

if(!function_exists('total_all_ticket'))
{
	function total_all_ticket(object $currMenu, $ticketModel = null)
	{
		$ticketModel = (is_null($ticketModel)) ? model('TicketModel') : $ticketModel;

		return $ticketModel->count_allTicket();
	}
}

if(!function_exists('hasTicketOwner'))
{
	function hasTicketOwner($ticketIdentifier)
	{
		
		$ticketModel = model('TicketModel');
		

		return $ticketModel->hasTicketOwner($ticketIdentifier, session()->get('username'));
	}
}

if(!function_exists('hasAgentInCharge'))
{
	function hasAgentInCharge($ticketIdentifier)
	{
		
		$ticketModel = model('TicketModel');
		

		return $ticketModel->hasAgentInCharge($ticketIdentifier, session()->get('username'));
	}
}

if(!function_exists('hasAllowedEditTicket'))
{
	function hasAllowedEditTicket($ticketIdentifier)
	{
		
		$ticketModel = model('TicketModel');
		$agentIC = $ticketModel->hasAgentInCharge($ticketIdentifier, session()->get('username'));
		$currentRole = session()->get('role');
		
		$ticketModel->where('identifier', $ticketIdentifier);
		$ticket = $ticketModel->first();
		$allowed = false;
		
		switch($ticket['status']){
			case 'open':
				$allowed = ($agentIC || $currentRole == 'manager');
			break;
			case 'inprogress':
				$allowed = ($agentIC || $currentRole == 'manager');
			break;		
			case 'closed':
				$allowed = (in_array($currentRole,['agent','manager']));
			break;
		}

		return $allowed;
	}
}

if(!function_exists('allowedTicketAction'))
{
	function allowedTicketAction($ticketStatus, $action, $ticketOwner, $agentPIC)
	{
		$currentRole = session()->get('role');
		$currentUserName = session()->get('username');
		$allowed = false;
		switch($ticketStatus){
			case 'open':
				$actionAllowed = ['lock_ticket'];
			break;
			case 'inprogress':
				$actionAllowed = ['unlock_ticket','change_progress_ticket','change_agent_incharge','close_ticket'];
			break;
			case 'closed':
				$actionAllowed = ['re_open_ticket'];
			break;
		}
		if(in_array($action,$actionAllowed)){
			if($allowed){
				if($ticketOwner == $currentUserRole){
					
				}
			}
		}
	}
}

if(!function_exists('curPostRequest'))
{
    function curPostRequest($url, $data)
    {
        /* eCurl */
        $curl = curl_init($url);
   
        /* Set JSON data to POST */
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            
        /* Define content type */
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            
        /* Return json */
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            
        /* make request */
        $result = curl_exec($curl);
             
        /* close curl */
        curl_close($curl);
		
		return $result;
    }
}

if(!function_exists('base_role_url'))
{
    function base_role_url($url = '')
    {
		return base_url(session()->get('role').((!empty($url))?'/'.$url:''));
    }
}

if(!function_exists('include_view_service_form'))
{
    function include_view_service_form($objThis, $viewName)
    {
		return $objThis->include($_ENV['app.view.services.form'].$viewName);
    }
}

if(!function_exists('include_view_service'))
{
    function include_view_service($objThis, $viewName)
    {
		return $objThis->include($_ENV['app.view.services'].$viewName);
    }
}

// TICKET VALIDATION
if(!function_exists('has_ticket_access'))
{
    function has_ticket_access($ticket)
    {
		if(!(is_owner($ticket) || is_agent_pic($ticket) || (is_agent() || is_manager())))
		{
			throw new \CodeIgniter\Router\Exceptions\RedirectException("403");
		}
    }
}
if(!function_exists('is_owner'))
{
    function is_owner($ticket)
    {
		return $ticket['username'] == session()->get('username');
    }
}

if(!function_exists('is_agent_pic'))
{
    function is_agent_pic($ticket)
    {
		return $ticket['agent'] == session()->get('username');
    }
}

if(!function_exists('is_user'))
{
    function is_user()
    {
		return session()->get('role') == 'user';
    }
}

if(!function_exists('is_agent'))
{
    function is_agent()
    {
		return session()->get('role') == 'agent';
    }
}

if(!function_exists('is_manager'))
{
    function is_manager()
    {
		return session()->get('role') == 'manager';
    }
}

if(!function_exists('has_action'))
{
    function has_action($ticket, $ticket_status = '')
    {
		switch($ticket_status){
			case 'open':
				return $ticket['status'] == $ticket_status && is_agent();
			break;
			case 'inprogress':
				return $ticket['status'] == $ticket_status && (is_agent_pic($ticket) || is_manager());
			break;	
			case 'closed':
				return $ticket['status'] == $ticket_status && (is_agent($ticket) || is_manager());
			break;
			default:
				return false;
			break;
		}
    }
}

if(!function_exists('is_role'))
{
    function is_role(array $roles)
    {
		return in_array(session()->get('role'),$roles);
    }
}

if(!function_exists('role'))
{
    function role($role = '')
    {
		return session()->get('role') == $role;
    }
}

if(!function_exists('parse_booking_dates'))
{
    function parse_booking_dates($dates)
    {
		return explode(',', $dates);
    }
}

if(!function_exists('date_sort'))
{
	function date_sort($a, $b) {
		return strtotime($a) - strtotime($b);
	}
}

if(!function_exists('check_continuous_dates'))
{
	function check_continuous_dates($date, array &$x = []) {
		$a = [];
		$prev = null;
		foreach ($date as $k => $v) {
			$current = new DateTime(current($date));
			$next = new DateTime(next($date));
			
			$diff = $current->diff($next);
			if ($diff->days == 1) {
				if($current->format('Y-m-d') == prev($date)){
					
				}else{
					$a[$current->format('Y-m-d')] = ['start'=>$current->format('Y-m-d'),'end'=>$next->format('Y-m-d')];
				}
			}else if($diff->days > 1){
				
			}else{

			}
			
		}
		$x[] = $a;

		return true;
	}
}

if(!function_exists('_get_')){
	function _get_($name = ''){
		if(isset($_GET[$name])){
			return $_GET[$name];
		}
		return '';
	}
}


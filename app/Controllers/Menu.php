<?php

namespace App\Controllers\Admin;

use CodeIgniter\RESTful\ResourceController;

class Menu extends ResourceController
{
    protected $modelName = 'App\Models\MenuModel';
    protected $format    = 'json';

    public function index()
    {
        return $this->respond($this->model->findAll());
    }

    public function show($id = null)
    {
        
    }

    public function new()
    {

    }

    public function create()
    {
       $this->model->save(array(
		'parent_id' => 0,
		'label' => 'New Menu',
		'role' => 'user',
    'index' => 0
	   ));      

		echo "<pre>";
		print_r($this->model->errors());	   
    }
	
    public function edit($id = null)
    {
        
    }

    public function update($id = null)
    {
		$data = [
			'role' => 'admin',
		];

		$this->model->update($id, $data);  

		echo "<pre>";
		print_r($this->model->errors());			
    }

    public function delete($id = null)
    {
        
    }
	
	public function logout()
	{
		session()->destroy();
		return redirect()->to(base_url('home'));
	}
}

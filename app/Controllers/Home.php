<?php

namespace App\Controllers;

use App\Models\UserModel;

class Home extends BaseController
{
	var $request;
	var $userModel;
	var $session;
	
	public function __construct()
	{
		helper(['form', 'url']);
		
		$this->session = session();
		$this->request = service('request');
		$this->userModel = model('App\Models\UserModel');

	}
	
    public function index()
    {
		return view('landing');
    }
	
	public function login()
	{
		
		if($loggedin = $this->session->has('isLoggedIn')){
			
			if($loggedin){
				return redirect()->to(base_url($this->session->get('role').'/dashboard'));
			}
		}

		if( getenv('CI_ENVIRONMENT') == 'production' ){
			// Enable debugging
			\phpCAS::setLogger();
			//phpCAS::setDebug('/tmp/phpcas.log');
			// Enable verbose error messages. Disable in production!
			\phpCAS::setVerbose(true);

			// Initialize phpCAS
			// 'http://login-dev4.itb.ac.id/cas';
			\phpCAS::client(CAS_VERSION_2_0, 'login.itb.ac.id', 443, '/cas');
			
			//
			\phpCAS::setNoCasServerValidation();	
			
			// TAMBAH INI JIKA REDIRECT LOOP TERJADI.
			\phpCAS::setNoClearTicketsFromUrl();			
			
			\phpCAS::forceAuthentication();
			$username = strstr(\phpCAS::getUser(),'@',true);
			$user = $this->userModel->userExists($username);
			if(!$user->getNumRows()){
				$data = [
					'name' => \phpCAS::getAttribute('cn'),
					'role' => 'user',
					'username' => \phpCAS::getUser(),
					'email' => \phpCAS::getAttribute('mail')
				];

				$this->userModel->save($data);
			}

			

			$user = $this->userModel->userExists($username);
			$user = $user->getRowArray();
			

			$sessionData = [
				'id' => $user['id'],
				'username' => $user['username'],
				'nip_nim' => $user['nip_nim'],
				'role' => $user['role'],
				'name' => $user['name'],
				'email' => $user['email'],
				'isLoggedIn' => TRUE
			];
			// if($data['role'] == 'asdos'){
				// $sessionData['confirm_asdos'] = 0;
			// }
			$this->session->set($sessionData);
			
			return redirect()->to(base_url($this->session->get('role').'/dashboard'));			
			
		}else{

			$errors = $this->session->getFlashdata('errors');

			return view('welcome_message',[
				'errors' => ($errors) ? $errors : '',
			]);				
		}		
	}
	private function nim_nip()
	{
		$CASAttr = \phpCAS::getAttributes();
		if(strtolower($CASAttr['itbStatus']) == 'mahasiswa'){
			return \phpCAS::getAttribute('itbNIM');
		}
		if(strtolower($CASAttr['itbStatus']) == 'dosen'){
			return \phpCAS::getAttribute('itbNIP');
		}	
		return '';
	}
	public function auth()
	{
		$username = $this->request->getPost('username');
		$password = $this->request->getPost('password');
		$data = $this->userModel->where('username', $username)->first();
		// print_r($data);die;
		if($data)
		{
			// $authenticatePassword = password_verify($password, $data['password_hash']);
			// print_r($authenticatePassword);die;
			// if($authenticatePassword)
			// {
                $sessionData = [
                    'id' => $data['id'],
                    'username' => $data['username'],
					'nip_nim' => $data['nip_nim'],
					'role' => $data['role'],
					'name' => $data['name'],
					'email' => $data['email'],
                    'isLoggedIn' => TRUE
                ];
				
				if($data['role'] == 'asdos'){
					$sessionData['confirm_asdos'] = 0;
				}
				
                $this->session->set($sessionData);
				
				switch($this->session->get('role')){
					case 'mahasiswa':
						$url = $this->session->get('role').'/nilai';
						break;
						default:
						$url = $this->session->get('role').'/dashboard';
						break;
					}
					
					return redirect()->to(base_url($url));				
				}
			// }
			// print_r("masuk");die;
			return redirect()->to(base_url('home'))->with('errors', '<span style="color:red;">Invalid credentials.</span>');
		}
}

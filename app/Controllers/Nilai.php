<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Nilai extends BaseController
{
    public function pengajuan_perubahan()
    {
        echo view('lo/list_pengajuan_perubahan_nilai',[
			'data' => []
		]);
    }
	
	// public function kategori_nilai($cat = '',$kd_kuliah='', $tahun='', $semester='', $nip = '', $nim = '')
	// {
		
		// //$this->{$cat}($kd_kuliah, $tahun, $semester, $nip = '', $nim = '');
	// }
	
    public function save_new_kategori()
	{
		$MenuModel = model('MenuModel');
		$KategoriNilaiModel = model('KategoriNilai');
		
		$_POST['map_route_id'] = 65; 
		$_POST['display'] = 1;
		$_POST['slug'] = slugify($_POST['label']).'/(:any)';
		$_POST['parent_id'] = '72';
		
		$MenuModel->save($_POST);
		
		$session = session();
		 
		$_POST['kategori_nilai']['nama'] = $_POST['label'];
		$_POST['kategori_nilai']['dosen_uuid'] = $session->get('id');
		$_POST['kategori_nilai']['dosen_pembuat'] = $session->get('id');
		
		$KategoriNilaiModel->save($_POST['kategori_nilai']);
		
		return redirect()->back();
	}	
}

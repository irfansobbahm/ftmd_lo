<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class LearningOutcome extends BaseController
{
    public function index($kd_kuliah='', $tahun ='', $semester = '')
    {
		
		$this->rekapNilai($kd_kuliah, $tahun, $semester);
	}	

    public function mahasiswa($kd_kuliah='', $tahun='', $semester='')
    {
		$uri = new \CodeIgniter\HTTP\URI(uri_string());
		
		$kategoriNilai = $uri->getSegment(3);
		
		$kategoriNilaiString = strtoupper($kategoriNilai);

		if(empty($tahun))
		{
			return redirect()->to(base_role_url('learning-outcome/'.$kategoriNilai.'/'.'MT3103/'.date('Y').'/ganjil'));
			//exit;
		}

		$kd_kuliah = empty($kd_kuliah) ? 'MT3103' : $kd_kuliah;
		$tahun = empty($tahun) ? date('Y') : $tahun;
		$semester = empty($semester) ? date('n') : $semester;
		
		$nip_nim = session('nip_nim');
		

		$client = \Config\Services::curlrequest();
		$response = $client->request('GET', 'https://ws.akademik.itb.ac.id/kelas_pengajar?nip_dosen=eq.'.$nip_nim, [
			'headers' => [
				'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZnRtZF9zaWV2YSIsImV4cCI6MTcwNTI1MTYwMH0.LJbHj0QK1Y5IFR7AIn4cVIL3zgCrA8TpyrxTOjDg4IQ',
				'Accept-Profile' => 'v_ftmd',
			],
		]);	
		
		if (strpos($response->header('content-type'), 'application/json') !== false)
		{
			$ws_kelas_pengajar = json_decode($response->getBody());
			$matakuliah = function($ws_kelas_pengajar)
			{
				$matakuliah = [];
				foreach($ws_kelas_pengajar as $pengajar)
				{
					$matakuliah[$pengajar->kd_kuliah] = $pengajar->nama_mk->id;
				}
				return $matakuliah;
			};
			$tahunkuliah = function($ws_kelas_pengajar)
			{
				$tahunkuliah = [];
				foreach($ws_kelas_pengajar as $pengajar)
				{
					$tahunkuliah[$pengajar->tahun] = $pengajar->tahun;
				}
				return $tahunkuliah;
			};
			$mahasiswa = function($kd_kuliah='',$tahun='',$semester='',$nim='',$nama='',$kelas=''){
				
				
				//$semester = empty($semester) ? date('n') : $semester;
				
				$client = \Config\Services::curlrequest();

				$response = $client->request('GET', 'https://ws.akademik.itb.ac.id/dpk', [
					'query' =>[
						'kd_kuliah' => 'eq.'.$kd_kuliah,
						'tahun' => 'eq.'.$tahun,
						'nim' => 'like.*'.$nim.'*',
						'nama' => 'like.*'.$nama.'*'
					],
					'headers' => [
						'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZnRtZF9zaWV2YSIsImV4cCI6MTcwNTI1MTYwMH0.LJbHj0QK1Y5IFR7AIn4cVIL3zgCrA8TpyrxTOjDg4IQ',
						'Accept-Profile' => 'v_ftmd',
					],
				]);	
				
				if (strpos($response->header('content-type'), 'application/json') !== false)
				{
					return json_decode($response->getBody());
				}else{
					return [];
				}
			};
			
			$uri = new \CodeIgniter\HTTP\URI(current_url());
			$request = \Config\Services::request();
			
			echo view('lo/mahasiswa',[
				'catNilai' => $kategoriNilaiString,
				'catNilaiSlug' =>$kategoriNilai,
				'uriSegments' => $uri->getSegments(),
				'ws' => [
					'kelas_pengajar' => $ws_kelas_pengajar,
					'matakuliah' => $matakuliah($ws_kelas_pengajar),
					'tahunkuliah' => $tahunkuliah($ws_kelas_pengajar),
					'mahasiswa' => $mahasiswa($kd_kuliah, $tahun, $semester, $request->getVar('nim'),$request->getVar('nama'),$kelas='')
				],
				
			]);
		}
    }
	
    public function mahasiswa_skala($kd_kuliah='', $tahun='', $semester='')
    {
		$uri = new \CodeIgniter\HTTP\URI(uri_string());
		
		$kategoriNilai = $uri->getSegment(3);
		
		$kategoriNilaiString = strtoupper($kategoriNilai);

		if(empty($tahun))
		{
			return redirect()->to(base_role_url('learning-outcome/'.$kategoriNilai.'/'.'MT3103/'.date('Y').'/ganjil'));
			//exit;
		}

		$kd_kuliah = empty($kd_kuliah) ? 'MT3103' : $kd_kuliah;
		$tahun = empty($tahun) ? date('Y') : $tahun;
		$semester = empty($semester) ? date('n') : $semester;
		
		$nip_nim = session('nip_nim');
		

		$client = \Config\Services::curlrequest();
		$response = $client->request('GET', 'https://ws.akademik.itb.ac.id/kelas_pengajar?nip_dosen=eq.'.$nip_nim, [
			'headers' => [
				'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZnRtZF9zaWV2YSIsImV4cCI6MTcwNTI1MTYwMH0.LJbHj0QK1Y5IFR7AIn4cVIL3zgCrA8TpyrxTOjDg4IQ',
				'Accept-Profile' => 'v_ftmd',
			],
		]);	
		
		if (strpos($response->header('content-type'), 'application/json') !== false)
		{
			$ws_kelas_pengajar = json_decode($response->getBody());
			$matakuliah = function($ws_kelas_pengajar)
			{
				$matakuliah = [];
				foreach($ws_kelas_pengajar as $pengajar)
				{
					$matakuliah[$pengajar->kd_kuliah] = $pengajar->nama_mk->id;
				}
				return $matakuliah;
			};
			$tahunkuliah = function($ws_kelas_pengajar)
			{
				$tahunkuliah = [];
				foreach($ws_kelas_pengajar as $pengajar)
				{
					$tahunkuliah[$pengajar->tahun] = $pengajar->tahun;
				}
				return $tahunkuliah;
			};
			$mahasiswa = function($kd_kuliah='',$tahun='',$semester='',$nim='',$nama='',$kelas=''){
				
				
				//$semester = empty($semester) ? date('n') : $semester;
				
				$client = \Config\Services::curlrequest();

				$response = $client->request('GET', 'https://ws.akademik.itb.ac.id/dpk', [
					'query' =>[
						'kd_kuliah' => 'eq.'.$kd_kuliah,
						'tahun' => 'eq.'.$tahun,
						'nim' => 'like.*'.$nim.'*',
						'nama' => 'like.*'.$nama.'*'
					],
					'headers' => [
						'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZnRtZF9zaWV2YSIsImV4cCI6MTcwNTI1MTYwMH0.LJbHj0QK1Y5IFR7AIn4cVIL3zgCrA8TpyrxTOjDg4IQ',
						'Accept-Profile' => 'v_ftmd',
					],
				]);	
				
				if (strpos($response->header('content-type'), 'application/json') !== false)
				{
					return json_decode($response->getBody());
				}else{
					return [];
				}
			};
			
			$uri = new \CodeIgniter\HTTP\URI(current_url());
			$request = \Config\Services::request();
			
			echo view('lo/mahasiswa_skala',[
				'catNilai' => $kategoriNilaiString,
				'catNilaiSlug' =>$kategoriNilai,
				'uriSegments' => $uri->getSegments(),
				'ws' => [
					'kelas_pengajar' => $ws_kelas_pengajar,
					'matakuliah' => $matakuliah($ws_kelas_pengajar),
					'tahunkuliah' => $tahunkuliah($ws_kelas_pengajar),
					'mahasiswa' => $mahasiswa($kd_kuliah, $tahun, $semester, $request->getVar('nim'),$request->getVar('nama'),$kelas='')
				],
				
			]);
		}
    }
	
    public function formula($kd_kuliah='', $tahun='', $semester='')
    {
		$uri = new \CodeIgniter\HTTP\URI(uri_string());
		
		$kategoriNilai = $uri->getSegment(3);
		
		$kategoriNilaiString = strtoupper($kategoriNilai);

		if(empty($tahun))
		{
			return redirect()->to(base_role_url('learning-outcome/'.$kategoriNilai.'/'.'MT3103/'.date('Y').'/ganjil'));
			//exit;
		}

		$kd_kuliah = empty($kd_kuliah) ? 'MT3103' : $kd_kuliah;
		$tahun = empty($tahun) ? date('Y') : $tahun;
		$semester = empty($semester) ? date('n') : $semester;
		
		$nip_nim = session('nip_nim');
		

		$client = \Config\Services::curlrequest();
		$response = $client->request('GET', 'https://ws.akademik.itb.ac.id/kelas_pengajar?nip_dosen=eq.'.$nip_nim, [
			'headers' => [
				'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZnRtZF9zaWV2YSIsImV4cCI6MTcwNTI1MTYwMH0.LJbHj0QK1Y5IFR7AIn4cVIL3zgCrA8TpyrxTOjDg4IQ',
				'Accept-Profile' => 'v_ftmd',
			],
		]);	
		
		if (strpos($response->header('content-type'), 'application/json') !== false)
		{
			$ws_kelas_pengajar = json_decode($response->getBody());
			$matakuliah = function($ws_kelas_pengajar)
			{
				$matakuliah = [];
				foreach($ws_kelas_pengajar as $pengajar)
				{
					$matakuliah[$pengajar->kd_kuliah] = $pengajar->nama_mk->id;
				}
				return $matakuliah;
			};
			$tahunkuliah = function($ws_kelas_pengajar)
			{
				$tahunkuliah = [];
				foreach($ws_kelas_pengajar as $pengajar)
				{
					$tahunkuliah[$pengajar->tahun] = $pengajar->tahun;
				}
				return $tahunkuliah;
			};
			$mahasiswa = function($kd_kuliah='',$tahun='',$semester='',$nim='',$nama='',$kelas=''){
				
				
				//$semester = empty($semester) ? date('n') : $semester;
				
				$client = \Config\Services::curlrequest();

				$response = $client->request('GET', 'https://ws.akademik.itb.ac.id/dpk', [
					'query' =>[
						'kd_kuliah' => 'eq.'.$kd_kuliah,
						'tahun' => 'eq.'.$tahun,
						'nim' => 'like.*'.$nim.'*',
						'nama' => 'like.*'.$nama.'*'
					],
					'headers' => [
						'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZnRtZF9zaWV2YSIsImV4cCI6MTcwNTI1MTYwMH0.LJbHj0QK1Y5IFR7AIn4cVIL3zgCrA8TpyrxTOjDg4IQ',
						'Accept-Profile' => 'v_ftmd',
					],
				]);	
				
				if (strpos($response->header('content-type'), 'application/json') !== false)
				{
					return json_decode($response->getBody());
				}else{
					return [];
				}
			};
			
			$uri = new \CodeIgniter\HTTP\URI(current_url());
			$request = \Config\Services::request();
			
			echo view('lo/formula',[
				'catNilai' => $kategoriNilaiString,
				'catNilaiSlug' =>$kategoriNilai,
				'uriSegments' => $uri->getSegments(),
				'ws' => [
					'kelas_pengajar' => $ws_kelas_pengajar,
					'matakuliah' => $matakuliah($ws_kelas_pengajar),
					'tahunkuliah' => $tahunkuliah($ws_kelas_pengajar),
					'mahasiswa' => $mahasiswa($kd_kuliah, $tahun, $semester, $request->getVar('nim'),$request->getVar('nama'),$kelas='')
				],
				
			]);
		}
    }

    public function bobot($kd_kuliah='', $tahun='', $semester='')
    {
		$uri = new \CodeIgniter\HTTP\URI(uri_string());
		
		$kategoriNilai = $uri->getSegment(3);
		
		$kategoriNilaiString = strtoupper($kategoriNilai);

		if(empty($tahun))
		{
			return redirect()->to(base_role_url('learning-outcome/'.$kategoriNilai.'/'.'MT3103/'.date('Y').'/ganjil'));
			//exit;
		}
		
		$kd_kuliah = empty($kd_kuliah) ? 'MT3103' : $kd_kuliah;
		$tahun = empty($tahun) ? date('Y') : $tahun;
		$semester = empty($semester) ? date('n') : $semester;
		
		$nip_nim = session('nip_nim');
		

		$client = \Config\Services::curlrequest();
		$response = $client->request('GET', 'https://ws.akademik.itb.ac.id/kelas_pengajar?nip_dosen=eq.'.$nip_nim, [
			'headers' => [
				'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZnRtZF9zaWV2YSIsImV4cCI6MTcwNTI1MTYwMH0.LJbHj0QK1Y5IFR7AIn4cVIL3zgCrA8TpyrxTOjDg4IQ',
				'Accept-Profile' => 'v_ftmd',
			],
		]);	
		
		if (strpos($response->header('content-type'), 'application/json') !== false)
		{
			$ws_kelas_pengajar = json_decode($response->getBody());
			$matakuliah = function($ws_kelas_pengajar)
			{
				$matakuliah = [];
				foreach($ws_kelas_pengajar as $pengajar)
				{
					$matakuliah[$pengajar->kd_kuliah] = $pengajar->nama_mk->id;
				}
				return $matakuliah;
			};
			$tahunkuliah = function($ws_kelas_pengajar)
			{
				$tahunkuliah = [];
				foreach($ws_kelas_pengajar as $pengajar)
				{
					$tahunkuliah[$pengajar->tahun] = $pengajar->tahun;
				}
				return $tahunkuliah;
			};
			$mahasiswa = function($kd_kuliah='',$tahun='',$semester='',$nim='',$nama='',$kelas=''){
				
				
				//$semester = empty($semester) ? date('n') : $semester;
				
				$client = \Config\Services::curlrequest();

				$response = $client->request('GET', 'https://ws.akademik.itb.ac.id/dpk', [
					'query' =>[
						'kd_kuliah' => 'eq.'.$kd_kuliah,
						'tahun' => 'eq.'.$tahun,
						'nim' => 'like.*'.$nim.'*',
						'nama' => 'like.*'.$nama.'*'
					],
					'headers' => [
						'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZnRtZF9zaWV2YSIsImV4cCI6MTcwNTI1MTYwMH0.LJbHj0QK1Y5IFR7AIn4cVIL3zgCrA8TpyrxTOjDg4IQ',
						'Accept-Profile' => 'v_ftmd',
					],
				]);	
				
				if (strpos($response->header('content-type'), 'application/json') !== false)
				{
					return json_decode($response->getBody());
				}else{
					return [];
				}
			};
			
			$uri = new \CodeIgniter\HTTP\URI(current_url());
			$request = \Config\Services::request();
			
			echo view('lo/bobot',[
				'catNilai' => $kategoriNilaiString,
				'catNilaiSlug' =>$kategoriNilai,
				'uriSegments' => $uri->getSegments(),
				'ws' => [
					'kelas_pengajar' => $ws_kelas_pengajar,
					'matakuliah' => $matakuliah($ws_kelas_pengajar),
					'tahunkuliah' => $tahunkuliah($ws_kelas_pengajar),
					'mahasiswa' => $mahasiswa($kd_kuliah, $tahun, $semester, $request->getVar('nim'),$request->getVar('nama'),$kelas='')
				],
				
			]);
		}
    }	
	
	// NEW MENU
	public function komponenAsesmen($kd_kuliah='', $tahun='', $semester='')
	{
		
		$uri = new \CodeIgniter\HTTP\URI(uri_string());
		
		$kategoriNilai = $uri->getSegment(3);
		
		$kategoriNilaiString = strtoupper($kategoriNilai);
		
		if(empty($tahun))
		{
			return redirect()->to(base_role_url('learning-outcome/'.$kategoriNilai.'/'.'MT3103/'.date('Y').'/ganjil'));
			//exit;
		}		
		
		$kd_kuliah = empty($kd_kuliah) ? 'MT3103' : $kd_kuliah;
		$tahun = empty($tahun) ? date('Y') : $tahun;
		$semester = empty($semester) ? date('n') : $semester;
		
		$nip_nim = session('nip_nim');


		$client = \Config\Services::curlrequest();
		$response = $client->request('GET', 'https://ws.akademik.itb.ac.id/kelas_pengajar?nip_dosen=eq.'.$nip_nim, [
			'headers' => [
				'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZnRtZF9zaWV2YSIsImV4cCI6MTcwNTI1MTYwMH0.LJbHj0QK1Y5IFR7AIn4cVIL3zgCrA8TpyrxTOjDg4IQ',
				'Accept-Profile' => 'v_ftmd',
			],
		]);	
		
		if (strpos($response->header('content-type'), 'application/json') !== false)
		{
			$ws_kelas_pengajar = json_decode($response->getBody());
			$matakuliah = function($ws_kelas_pengajar)
			{
				$matakuliah = [];
				foreach($ws_kelas_pengajar as $pengajar)
				{
					$matakuliah[$pengajar->kd_kuliah] = $pengajar->nama_mk->id;
				}
				return $matakuliah;
			};
			$tahunkuliah = function($ws_kelas_pengajar)
			{
				$tahunkuliah = [];
				foreach($ws_kelas_pengajar as $pengajar)
				{
					$tahunkuliah[$pengajar->tahun] = $pengajar->tahun;
				}
				return $tahunkuliah;
			};
			$mahasiswa = function($kd_kuliah='',$tahun='',$semester='',$nim='',$nama='',$kelas=''){
				$client = \Config\Services::curlrequest();

				$response = $client->request('GET', 'https://ws.akademik.itb.ac.id/dpk', [
					'query' =>[
						'kd_kuliah' => 'eq.'.$kd_kuliah,
						'tahun' => 'eq.'.$tahun,
						'nim' => 'like.*'.$nim.'*',
						'nama' => 'like.*'.$nama.'*'
					],
					'headers' => [
						'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZnRtZF9zaWV2YSIsImV4cCI6MTcwNTI1MTYwMH0.LJbHj0QK1Y5IFR7AIn4cVIL3zgCrA8TpyrxTOjDg4IQ',
						'Accept-Profile' => 'v_ftmd',
					],
				]);	
				
				if (strpos($response->header('content-type'), 'application/json') !== false)
				{
					return json_decode($response->getBody());
				}else{
					return [];
				}
			};
			
			$uri = new \CodeIgniter\HTTP\URI(current_url());
			$request = \Config\Services::request();
			$nilaiModel = model('NilaiModel');
			$session = session();
			
			$where = [
				'dosen_uuid' => $session->get('id'),
				'tahun_akademik' => $tahun,
				'semester' => $semester,
				'kode_kuliah' => $kd_kuliah
			];
			$kategori_nilai = $nilaiModel->getAll($where);
			
			$KomponenAsesmenModel = model('App\Models\KomponenAsesmenModel');
			$KomponenAsesmenRangkumanModel = model('App\Models\KomponenAsesmenRangkumanModel');
			
			$komponen_asesmen = array();
			$data_komponen_asesmen = $KomponenAsesmenModel->getRow([
				'kode_kuliah' => $kd_kuliah,
				'dosen_uuid' => session()->get('id'),
				'tahun_akademik' => $tahun,
				'semester' => $semester
			]);
			$komponen_asesmen['komponen_asesmen'] = $data_komponen_asesmen;

			if($data_komponen_asesmen){
				$data_komponen_rangkuman = $KomponenAsesmenRangkumanModel->getAllKomponenAsesmenRangkuman([
					'id_komponen_asesmen' => $data_komponen_asesmen->id
				]);
				$komponen_asesmen['komponen_rangkuman'] = $data_komponen_rangkuman;
			}			
						
			echo view('lo/komponen_asesmen',[
				'catNilai' => $kategoriNilaiString,
				'catNilaiSlug' =>$kategoriNilai,
				'uriSegments' => $uri->getSegments(),
				'ws' => [
					'kelas_pengajar' => $ws_kelas_pengajar,
					'matakuliah' => $matakuliah($ws_kelas_pengajar),
					'tahunkuliah' => $tahunkuliah($ws_kelas_pengajar),
					'mahasiswa' => $mahasiswa($kd_kuliah, $tahun, $semester, $request->getVar('nim'),$request->getVar('nama'),$kelas='')
				],
				'kategori_nilai' => $kategori_nilai,
				'komponen_asesmen' => $komponen_asesmen
			]);
		}		
	}
	
	public function api_komponen_asesmen()
	{
		extract($_POST);
		$KomponenAsesmenModel = model('App\Models\KomponenAsesmenModel');
		$KomponenAsesmenRangkumanModel = model('App\Models\KomponenAsesmenRangkumanModel');
		
		$komponen_asesmen = array();
		$data_komponen_asesmen = $KomponenAsesmenModel->getRow([
			'kode_kuliah' => $kd_kuliah,
			'dosen_uuid' => session()->get('id'),
			'tahun_akademik' => $tahun,
			'semester' => $semester
		]);
		$komponen_asesmen['komponen_asesmen'] = $data_komponen_asesmen;

		if($data_komponen_asesmen){
			$data_komponen_rangkuman = $KomponenAsesmenRangkumanModel->getAllKomponenAsesmenRangkuman([
				'id_komponen_asesmen' => $data_komponen_asesmen->id
			]);
			$komponen_asesmen['komponen_rangkuman'] = $data_komponen_rangkuman;
		}
		
		return json_encode($komponen_asesmen);
					
	}
	
	public function api_kelompok_nilai()
	{
		extract($_POST);
		$KomponenAsesmenModel = model('App\Models\KomponenAsesmenModel');
		$KomponenAsesmenRangkumanModel = model('App\Models\KomponenAsesmenRangkumanModel');
		$NilaiModel = model('App\Models\App\NilaiModel');
		$NilaiMhsModel = model('App\Models\App\NilaiMhsModel');
		$PembobotanLoModel = model('App\Models\PembobotanLoModel');
		$PembobotanLoNilaiMhsModel = model('App\Models\PembobotanLoNilaiMhsModel');		
		
		$data = array();
		$data_komponen_asesmen = $KomponenAsesmenModel->getRow([
			'kode_kuliah' => $kd_kuliah,
			'dosen_uuid' => session()->get('id'),
			'tahun_akademik' => $tahun,
			'semester' => $semester
		]);
		$data['komponen_asesmen'] = $data_komponen_asesmen;
		
		if($data_komponen_asesmen){
			$data_komponen_rangkuman = $KomponenAsesmenRangkumanModel->getAllKomponenAsesmenRangkuman([
				'id_komponen_asesmen' => $data_komponen_asesmen->id
			]);
			$data['komponen_rangkuman'] = $data_komponen_rangkuman;
			foreach($data_komponen_rangkuman[$lo] as $katnil => $krang)
			{
				$data_kelompok_nilai = $NilaiModel->getRow([
					'kategori_nilai' => strtoupper($katnil),
					'kode_kuliah' => $kd_kuliah,
					'dosen_uuid' => session()->get('id'),
					'tahun_akademik' => $tahun,
					'semester' => $semester
				]);
				if($data_kelompok_nilai){
					$data_kelompok_nilai_mhs = $NilaiMhsModel->getAllKelompokNilaiMhs([
						'id_kelompok_nilai' => $data_kelompok_nilai->id
					]);
					$data_kelompok_nilai->kelompok_nilai_mhs = $data_kelompok_nilai_mhs;
				}				
				$data['kelompok_nilai'][$data_kelompok_nilai->kategori_nilai] = $data_kelompok_nilai;
			}
		}	
		$data_pembobotan_lo = $PembobotanLoModel->getRow([
			'lo' => $lo,
			'kode_kuliah' => $kd_kuliah,
			'dosen_uuid' => session()->get('id'),
			'tahun_akademik' => $tahun,
			'semester' => $semester
		]);	
		if(!is_null($data_pembobotan_lo)){
			$nilaimhs = $PembobotanLoNilaiMhsModel->getAll([
				'id_pembobotan_lo' => $data_pembobotan_lo->id,
			]);
			$ndata = [];
			foreach($nilaimhs as $nilai){
				$ndata[$nilai->nim] = $nilai;
			}
			$data_pembobotan_lo->nilaimhs = $ndata;
		}
		$data['pembobotan_lo'] = $data_pembobotan_lo;
		
		return json_encode($data);
	}
	
	public function api_pembobotan_lo()
	{
		extract($_POST);
		$PembobotanLoModel = model('App\Models\PembobotanLoModel');
		$PembobotanLoNilaiMhsModel = model('App\Models\PembobotanLoNilaiMhsModel');			
		
		$data = array();
		$data_pembobotan_lo = $PembobotanLoModel->getRow([
			'lo' => $lo,
			'kode_kuliah' => $kd_kuliah,
			'dosen_uuid' => session()->get('id'),
			'tahun_akademik' => $tahun,
			'semester' => $semester
		]);
		
		$data_pembobotan_lo->nilaimhs = $PembobotanLoNilaiMhsModel->getAll([
			'id_pembobotan_lo' => $data_pembobotan_lo->id,
		]);
		
		echo json_encode($data_pembobotan_lo);
	}
	
	public function api_hasil_asesmen()
	{
		extract($_POST);
		$KomponenAsesmenModel = model('App\Models\KomponenAsesmenModel');
		$KomponenAsesmenRangkumanModel = model('App\Models\KomponenAsesmenRangkumanModel');
		$NilaiModel = model('App\Models\App\NilaiModel');
		$NilaiMhsModel = model('App\Models\App\NilaiMhsModel');
		$PembobotanLoModel = model('App\Models\PembobotanLoModel');
		$PembobotanLoNilaiMhsModel = model('App\Models\PembobotanLoNilaiMhsModel');		
		
		$data = array();
		$data_komponen_asesmen = $KomponenAsesmenModel->getRow([
			'kode_kuliah' => $kd_kuliah,
			'dosen_uuid' => session()->get('id'),
			'tahun_akademik' => $tahun,
			'semester' => $semester
		]);
		$data['komponen_asesmen'] = $data_komponen_asesmen;
		
		if($data_komponen_asesmen){
			$data_komponen_rangkuman = $KomponenAsesmenRangkumanModel->getAllKomponenAsesmenRangkuman([
				'id_komponen_asesmen' => $data_komponen_asesmen->id
			]);
			$data['komponen_rangkuman'] = $data_komponen_rangkuman;
			foreach($data_komponen_rangkuman[$lo] as $katnil => $krang)
			{
				$data_kelompok_nilai = $NilaiModel->getRow([
					'kategori_nilai' => strtoupper($katnil),
					'kode_kuliah' => $kd_kuliah,
					'dosen_uuid' => session()->get('id'),
					'tahun_akademik' => $tahun,
					'semester' => $semester
				]);
				if($data_kelompok_nilai){
					$data_kelompok_nilai_mhs = $NilaiMhsModel->getAllKelompokNilaiMhs([
						'id_kelompok_nilai' => $data_kelompok_nilai->id
					]);
					$data_kelompok_nilai->kelompok_nilai_mhs = $data_kelompok_nilai_mhs;
				}				
				$data['kelompok_nilai'][$data_kelompok_nilai->kategori_nilai] = $data_kelompok_nilai;
			}
		}	
		$data_pembobotan_lo = $PembobotanLoModel->getRow([
			'lo' => $lo,
			'kode_kuliah' => $kd_kuliah,
			'dosen_uuid' => session()->get('id'),
			'tahun_akademik' => $tahun,
			'semester' => $semester
		]);	
		$data['pembobotan_lo'] = $data_pembobotan_lo;
		
		return json_encode($data);
	}	
	public function saveKomponenAsesmen()
	{
		// echo "<pre>";
		// print_r($_POST);
		// exit;
		 $KomponenAsesmenModel = model('App\Models\KomponenAsesmenModel');
		 $KomponenAsesmenRangkumanModel = model('App\Models\KomponenAsesmenRangkumanModel');
		 $ID = $KomponenAsesmenModel->save_komponen($_POST['komponen_asesmen']);
		 $komponen_asesmen_rangkuman = $_POST['komponen_asesmen_rangkuman'];
		 $ids = [];
		 foreach($komponen_asesmen_rangkuman as $LO => $katnil)
		 {
			 
			 foreach($katnil as $kat => $sat)
			 {
				 $exc = array_flip($sat);
				 foreach($exc as $idkom)
				 {
					 $getid = explode('-',$idkom);
					 if(count($getid) > 1){
						$ids[$LO][] = $getid[1];
					 }
				 }
			 }
		 }
		
		 foreach($komponen_asesmen_rangkuman as $LO => $katnil)
		 {
			 foreach($katnil as $kat => $sat)
			 {
				 foreach($sat as $k => $val){
					 $id_komponen_asesmen = !empty($ID) ? $ID : $_POST['komponen_asesmen']['id'];
					 if(array_key_exists($LO, $ids)){
						//@$KomponenAsesmenRangkumanModel->where('id_komponen_asesmen',$id_komponen_asesmen)->where('LO', $LO)->whereNotIn('id', $ids[$LO])->delete();					 
					 }
				 }
			 }
		 }		
		 $KomponenAsesmenRangkumanModel->where('id_komponen_asesmen',$id_komponen_asesmen)->delete();
		 foreach($komponen_asesmen_rangkuman as $LO => $katnil)
		 {		 
			 foreach($katnil as $kat => $sat)
			 {
				 foreach($sat as $k => $val){
					 $xsat = explode(' ',$val);
					 $idx = explode('-',$k);
					 $id = count($idx) > 1 ? $idx[1]:'';
					 if(!empty($val)){
						 $KomponenAsesmenRangkumanModel->save([
							//'id' => $id,
							'LO' => $LO,
							'kategori_nilai' => strtoupper($kat),
							'urutan_satuan' => $val,
							'id_komponen_asesmen' => $id_komponen_asesmen
						 ]);
					 }
					 if(empty($val)){
						//@$KomponenAsesmenRangkumanModel->where('id',$id)->delete();
					 }
				 }
			 }
		 }
		 
		 echo json_encode(["rowid" => $ID]);
	}
	
	public function saveloTerases()
	{
		$_POST['lo_terases']['hasil_ases'] = json_encode($_POST['lo_terases']['hasil_ases']);
		extract($_POST);
		$loTerasesModel = model('loTerasesModel');

		$ID = $loTerasesModel->_save($lo_terases);
		$ID = !empty($ID) ? $ID : $lo_terases['id'];
		
		echo json_encode(["rowid" => $ID]);
	}
	
	public function pembobotanLO($kd_kuliah='', $tahun='', $semester='')
	{
		
		$uri = new \CodeIgniter\HTTP\URI(uri_string());
		
		$kategoriNilai = $uri->getSegment(3);
		
		$kategoriNilaiString = strtoupper($kategoriNilai);

		if(empty($tahun))
		{
			return redirect()->to(base_role_url('learning-outcome/pembobotan-lo/MT3103/'.date('Y').'/ganjil'));
			exit;
		}

		$kd_kuliah = empty($kd_kuliah) ? 'MT3103' : $kd_kuliah;
		$tahun = empty($tahun) ? date('Y') : $tahun;
		$semester = empty($semester) ? date('n') : $semester;
		
		$nip_nim = session('nip_nim');
		

		$client = \Config\Services::curlrequest();
		$response = $client->request('GET', 'https://ws.akademik.itb.ac.id/kelas_pengajar?nip_dosen=eq.'.$nip_nim, [
			'headers' => [
				'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZnRtZF9zaWV2YSIsImV4cCI6MTcwNTI1MTYwMH0.LJbHj0QK1Y5IFR7AIn4cVIL3zgCrA8TpyrxTOjDg4IQ',
				'Accept-Profile' => 'v_ftmd',
			],
		]);	
		
		if (strpos($response->header('content-type'), 'application/json') !== false)
		{
			$ws_kelas_pengajar = json_decode($response->getBody());
			$matakuliah = function($ws_kelas_pengajar)
			{
				$matakuliah = [];
				foreach($ws_kelas_pengajar as $pengajar)
				{
					$matakuliah[$pengajar->kd_kuliah] = $pengajar->nama_mk->id;
				}
				return $matakuliah;
			};
			$tahunkuliah = function($ws_kelas_pengajar)
			{
				$tahunkuliah = [];
				foreach($ws_kelas_pengajar as $pengajar)
				{
					$tahunkuliah[$pengajar->tahun] = $pengajar->tahun;
				}
				return $tahunkuliah;
			};
			$mahasiswa = function($kd_kuliah='',$tahun='',$semester='',$nim='',$nama='',$kelas=''){
				
				
				//$semester = empty($semester) ? date('n') : $semester;
				
				$client = \Config\Services::curlrequest();

				$response = $client->request('GET', 'https://ws.akademik.itb.ac.id/dpk', [
					'query' =>[
						'kd_kuliah' => 'eq.'.$kd_kuliah,
						'tahun' => 'eq.'.$tahun,
						'nim' => 'like.*'.$nim.'*',
						'nama' => 'like.*'.$nama.'*'
					],
					'headers' => [
						'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZnRtZF9zaWV2YSIsImV4cCI6MTcwNTI1MTYwMH0.LJbHj0QK1Y5IFR7AIn4cVIL3zgCrA8TpyrxTOjDg4IQ',
						'Accept-Profile' => 'v_ftmd',
					],
				]);	
				
				if (strpos($response->header('content-type'), 'application/json') !== false)
				{
					return json_decode($response->getBody());
				}else{
					return [];
				}
			};
			
			$uri = new \CodeIgniter\HTTP\URI(current_url());
			$request = \Config\Services::request();

			$PembobotanLoModel = model('App\Models\PembobotanLoModel');
			$PembobotanLoNilaiMhsModel = model('App\Models\PembobotanLoNilaiMhsModel');
			
			$data = array();
			$data_pembobotan_lo = $PembobotanLoModel->getRow([
				'kode_kuliah' => $kd_kuliah,
				'dosen_uuid' => session()->get('id'),
				'tahun_akademik' => $tahun,
				'semester' => $semester
			]);
			
			echo view('lo/pembobotan_lo',[
				'catNilai' => $kategoriNilaiString,
				'catNilaiSlug' =>$kategoriNilai,
				'uriSegments' => $uri->getSegments(),
				'pembobotan_lo' => $data_pembobotan_lo,
				'ws' => [
					'kelas_pengajar' => $ws_kelas_pengajar,
					'matakuliah' => $matakuliah($ws_kelas_pengajar),
					'tahunkuliah' => $tahunkuliah($ws_kelas_pengajar),
					'mahasiswa' => $mahasiswa($kd_kuliah, $tahun, $semester, $request->getVar('nim'),$request->getVar('nama'),$kelas='')
				],
				
			]);
		}		
	}
	
	public function savePembobotanLo()
	{
		extract($_POST);
		// echo "<pre>";
		// print_r($pembobotan_lo);
		// exit;
		$PembobotanLoModel = model('App\Models\PembobotanLoModel');
		$PembobotanLoNilaiMhsModel = model('App\Models\PembobotanLoNilaiMhsModel');
		$ID = $PembobotanLoModel->save_plo($pembobotan_lo);		
		$id_pembobotan_lo = !empty($ID) ? $ID : $_POST['pembobotan_lo']['id'];
		//$PembobotanLoNilaiMhsModel->where('id_pembobotan_lo', $ID)->delete();
		foreach($pembobotan_lo['nilaimhs'] as $nim => $nilailo)
		{
			$PembobotanLoNilaiMhsModel->save([
				'id' => $nilailo['id'],
				'nim' => $nim,
				'nilai_lo' => $nilailo['value'],
				'id_pembobotan_lo' => $id_pembobotan_lo
			]);
		}
		
		
		 echo json_encode(["rowid" => $id_pembobotan_lo]);
	}
	
	public function hasilAsesmen($kd_kuliah='', $tahun='', $semester=''){
		$uri = new \CodeIgniter\HTTP\URI(uri_string());
		
		$kategoriNilai = $uri->getSegment(3);
		
		$kategoriNilaiString = strtoupper($kategoriNilai);

		if(empty($tahun))
		{
			return redirect()->to(base_role_url('learning-outcome/hasil-asesmen/MT3103/'.date('Y').'/ganjil'));
			exit;
		}

		$kd_kuliah = empty($kd_kuliah) ? 'MT3103' : $kd_kuliah;
		$tahun = empty($tahun) ? date('Y') : $tahun;
		$semester = empty($semester) ? date('n') : $semester;
		
		$nip_nim = session('nip_nim');
		

		$client = \Config\Services::curlrequest();
		$response = $client->request('GET', 'https://ws.akademik.itb.ac.id/kelas_pengajar?nip_dosen=eq.'.$nip_nim, [
			'headers' => [
				'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZnRtZF9zaWV2YSIsImV4cCI6MTcwNTI1MTYwMH0.LJbHj0QK1Y5IFR7AIn4cVIL3zgCrA8TpyrxTOjDg4IQ',
				'Accept-Profile' => 'v_ftmd',
			],
		]);	
		
		if (strpos($response->header('content-type'), 'application/json') !== false)
		{
			$ws_kelas_pengajar = json_decode($response->getBody());

			$matakuliah = function($ws_kelas_pengajar)
			{
				$matakuliah = [];
				foreach($ws_kelas_pengajar as $pengajar)
				{
					$matakuliah[$pengajar->kd_kuliah] = $pengajar->nama_mk->id;
				}
				return $matakuliah;
			};
			$tahunkuliah = function($ws_kelas_pengajar)
			{
				$tahunkuliah = [];
				foreach($ws_kelas_pengajar as $pengajar)
				{
					$tahunkuliah[$pengajar->tahun] = $pengajar->tahun;
				}
				return $tahunkuliah;
			};
			$mahasiswa = function($kd_kuliah='',$tahun='',$semester='',$nim='',$nama='',$kelas=''){
				
				
				//$semester = empty($semester) ? date('n') : $semester;
				
				$client = \Config\Services::curlrequest();

				$response = $client->request('GET', 'https://ws.akademik.itb.ac.id/dpk', [
					'query' =>[
						'kd_kuliah' => 'eq.'.$kd_kuliah,
						'tahun' => 'eq.'.$tahun,
						'nim' => 'like.*'.$nim.'*',
						'nama' => 'like.*'.$nama.'*'
					],
					'headers' => [
						'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZnRtZF9zaWV2YSIsImV4cCI6MTcwNTI1MTYwMH0.LJbHj0QK1Y5IFR7AIn4cVIL3zgCrA8TpyrxTOjDg4IQ',
						'Accept-Profile' => 'v_ftmd',
					],
				]);	
				
				if (strpos($response->header('content-type'), 'application/json') !== false)
				{
					return json_decode($response->getBody());
				}else{
					return [];
				}
			};
			
			$uri = new \CodeIgniter\HTTP\URI(current_url());
			$request = \Config\Services::request();
			$model = model('App\Models\App\loMatrixModel');
			$loAktual = $model->getLOAktual($kd_kuliah);			




			$KomponenAsesmenRangkumanModel = model('App\Models\KomponenAsesmenRangkumanModel');
			$KomponenAsesmenRangkumanModel->hasilAsesmen([
				'kode_kuliah' => $kd_kuliah,
				'dosen_uuid' => session()->get('id'),
				'tahun_akademik' => $tahun,
				'semester' => $semester
			]);		

			$NilaiModel = model('NilaiModel');
			$NilaiMhsModel = model('NilaiMhsModel');
			$list_mhs = $mahasiswa($kd_kuliah, $tahun, $semester, $request->getVar('nim'),$request->getVar('nama'),$kelas='');
			foreach($list_mhs as $mhs)
			{
				$nilai = $NilaiModel->getMhsNilaiLO([
					'nim' => $mhs->nim,
					'kode_kuliah' => $kd_kuliah,
					'dosen_uuid' => session()->get('id'),
					'tahun_akademik' => $tahun,
					'semester' => $semester
				]);
				$mhs->nilai = $nilai['nilai'];
				$mhs->nilaiLo = $nilai['nilaiLo'];
				
			}
			$loTerasesModel = model('loTerasesModel');

			echo view('lo/hasil_asesmen',[
				'catNilai' => $kategoriNilaiString,
				'catNilaiSlug' =>$kategoriNilai,
				'uriSegments' => $uri->getSegments(),
				'loAktual' => $loAktual->getRow(),
				'ws' => [
					'kelas_pengajar' => $ws_kelas_pengajar,
					'matakuliah' => $matakuliah($ws_kelas_pengajar),
					'tahunkuliah' => $tahunkuliah($ws_kelas_pengajar),
					'mahasiswa' => $list_mhs
				],
				'lo_terases' => $loTerasesModel->getRow([
					'kode_kuliah' => $kd_kuliah,
					'dosen_uuid' => session()->get('id'),
					'tahun_akademik' => $tahun,
					'semester' => $semester					
				])
			]);
		}		
	}
	public function loAsesmen(){
		
	}	
}

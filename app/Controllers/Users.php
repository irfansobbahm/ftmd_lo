<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Users extends BaseController
{
    protected $modelName = 'App\Models\UserModel';
    protected $format    = 'json';
	private $model;
	public function __construct()
	{
		$this->model = model($this->modelName);
	}

    public function index()
    {
		$this->model->select('id,username,role,name,email');
		$data = $this->model->findAll();
		
		$roleModel = model('RoleModel');
		$roleModel->select('identifier, name');
		$roles = $roleModel->findAll();
		
		return view('manage/users',[
			'roles' => $roles,
			'data' => $data
		]);		
        //return $this->respond();
    }

    public function show($id = null)
    {
        
    }

    public function new()
    {

    }

    public function create()
    {
       $this->model->save(array(
		'username' => 'user',
		'password' => 'p',
		'email' => 'user@myplace.com'
	   ));      

		echo "<pre>";
		print_r($this->model->errors());	   
    }
	
    public function edit($id = null)
    {
        
    }

    public function update($id = null)
    {
		$data = [
			'role' => 'admin',
		];

		$this->model->update($id, $data);  

		echo "<pre>";
		print_r($this->model->errors());			
    }

    public function delete($id = null)
    {
        
    }
	
	public function logout()
	{
		if( getenv('CI_ENVIRONMENT') == 'production' ){
			// Enable debugging
			\phpCAS::setLogger();
			//phpCAS::setDebug('/tmp/phpcas.log');
			// Enable verbose error messages. Disable in production!
			\phpCAS::setVerbose(true);

			// Initialize phpCAS
			// 'http://login-dev4.itb.ac.id/cas';
			\phpCAS::client(CAS_VERSION_2_0, 'login.itb.ac.id', 443, '/cas');
			
			//
			\phpCAS::setNoCasServerValidation();	
			
			// TAMBAH INI JIKA REDIRECT LOOP TERJADI.
			\phpCAS::setNoClearTicketsFromUrl();			
			
			\phpCAS::logoutWithRedirectService(base_url());
			session()->destroy();
		}else{
			session()->destroy();
		}
		return redirect()->to(base_url('home'));
	}
}

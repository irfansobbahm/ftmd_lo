<?php

namespace App\Controllers\Api;

use App\Controllers\BaseController;

class Users extends BaseController
{
    protected $modelName = 'App\Models\UserModel';
    protected $format    = 'json';
	public function updateUserRole()
	{
		$username = (isset($_POST['username']))?$_POST['username']:'';
		$data = (isset($_POST['data']))?$_POST['data']:'';
		$change = (isset($_POST['change']))?$_POST['change']:'';

		$model = model($this->modelName);
		$update = $model->changeUserRole($username, $data);

		if($update)
		{
			session()->setFlashdata('success', 'Successfully change user role "'.$change['user'].'" from "'.$change['from'].'" to "'.$change['to'].'"');
		}
		
		echo json_encode([
			'status' => $update
		]);
	}
}

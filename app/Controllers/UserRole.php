<?php

namespace App\Controllers\Admin;

use CodeIgniter\RESTful\ResourceController;

class UserRole extends ResourceController
{
    protected $modelName = 'App\Models\RoleModel';
    protected $format    = 'json';
    public function index()
    {
        return $this->respond($this->model->findAll());
    }

    public function show($id = null)
    {
      
    }

    public function new()
    {
       
    }

    public function create()
    {
 		$this->model->save(array(
			'identifier' => 'management',
			'name' => 'User Manager'
		));        
		
		echo "<pre>";
		print_r($this->model->errors());
    }

    public function edit($id = null)
    {
        
    }

    public function update($id = null)
    {
		$data = [
			'identifier' => 'admin',
			'name' => 'Administrator'
		];
		$this->model->update($id, $data); 
		echo "<pre>";
		print_r($this->model->errors());			
    }

    public function delete($id = null)
    {
        
    }
}

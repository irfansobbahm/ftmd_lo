<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Dashboard extends BaseController
{
    public function index()
    {
		$nip_nim = session('nip_nim');
		
		$client = \Config\Services::curlrequest();
		$response = $client->request('GET', 'https://ws.akademik.itb.ac.id/kelas_pengajar?nip_dosen=eq.'.$nip_nim, [
			'headers' => [
				'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZnRtZF9zaWV2YSIsImV4cCI6MTcwNTI1MTYwMH0.LJbHj0QK1Y5IFR7AIn4cVIL3zgCrA8TpyrxTOjDg4IQ',
				'Accept-Profile' => 'v_ftmd',
			],
		]);	
		
		if (strpos($response->header('content-type'), 'application/json') !== false)
		{
			$ws_kelas_pengajar = json_decode($response->getBody());
			$matakuliah = function($ws_kelas_pengajar)
			{
				$matakuliah = [];
				foreach($ws_kelas_pengajar as $pengajar)
				{
					$matakuliah[$pengajar->kd_kuliah] = $pengajar->nama_mk->id;
				}
				return $matakuliah;
			};			
			// echo "<pre>";
			// print_r($matakuliah($ws_kelas_pengajar));
			// exit;
		}
		
		echo view('dashboard',[
			'ws' => [
				'matakuliah' => $matakuliah($ws_kelas_pengajar)
			],
		]);
		
    }
	
	public function layout()
	{
		echo view('dashboard');
	}
}

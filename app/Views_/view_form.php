<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>

<div class="row">
<div class="card col-lg-8 m-auto pt-2 pb-4">
	<div class="card-header">
		<h4 class="mb-3 font-weight-bolder"><?= $form->title ?></h4>
		<h5 class="mb-3 text-sm"><?= $form->description ?></h5>	
		<hr class="horizontal dark">
	</div>
	<form method="POST">
	<div class="card-body pt-0">
		
		<?php foreach($fields as $n => $field){ ?>
		<div class="form-group mb-1 pb-3">
			<label style="font-size:12pt;"><?= ($n+1).'. '.$field->descr ?></label>
			
		<?php switch($field->type){ 
				case 'textarea':
					$html = '<textarea rows="5" required class="form-control"></textarea>';
				break;
				case 'date':
					$html  = 	'<input type="'.$field->type.'" class="form-control mt-0 mb-2" placeholder="Input jawaban : dd/mm/yyyy">';	
				break;	
				case 'email':
					$html = 	'<input type="'.$field->type.'" class="form-control mt-0  mb-2" placeholder="Input jawaban : saya@domain.com">';
					
				break;
				case 'phone_num':
					$html  = 	'<div class="row">';
					$html .=		'<div class="col-2">';
					$html .= 	'<select readonly class="form-control mt-0  mb-2">';
					$html .=		'<option selected>+62</option>';
					$html .= 	'</select>';
					$html .=		'</div>';
					$html .=		'<div class="col-sm"><input type="number" class="form-control col-md-6 mt-0"></div>';
					$html .= 	'</div>';
					
				break;		
				case 'radio':
					$sub = explode(';', $field->sub);
					$html = '';
					foreach($sub as $text){
						$html .= '<div class="form-check mb-3">';
						$html .= '<input class="form-check-input" type="radio" name="flexRadioDefault" id="customRadio1">';
						$html .= '<label class="custom-control-label" for="customRadio1">'.$text.'</label>	';	
						$html .= '</div>';	
					}

				break;
				case 'checkbox':
					$sub = explode(';', $field->sub);
					$html = '';
					foreach($sub as $text){				
						$html .= '<div class="form-check mb-3">';
						$html .= '  <input class="form-check-input" type="checkbox" value="" id="fcustomCheck1">';
						$html .= '  <label class="custom-control-label" for="customCheck1">'.$text.'</label>';			
						$html .= '</div>';				
					}
				break;				
				default:
					$html = '<input required type="'.$field->type.'" class="form-control mt-0  mb-2" placeholder="Input Jawaban">';
				break;
			}
			
			echo $html;
			?>		
			
			
		</div>
		<?php } ?>
		
		
	</div>
	<div class="card-footer pt-0">
		<button type="submit" class="btn btn-primary">Submit</button>
	</div>
	</form>
</div>
</div>


<?= $this->endSection() ?>
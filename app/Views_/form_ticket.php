<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>

<div class="row">
	<div class="card">
		<div class="card-body">
		<h4 class="text-uppercase text-md">Create New Ticket - <?= $this->breadcrumb->crumb_last ?></h4>		
		  <form enctype="multipart/form-data" class="needs-validation" method="POST" action="<?= uri_save_ticket_data() ?>">
			  
			  <input type="hidden" name="data[service_id]" value="<?= (isset($service['id']) && !empty($service['id'])) ? $service['id']:'' ?>">
			  
			  <div class="row">
				<div class="col-md-12">
				  <div class="form-group d-none">
					<label for="example-text-input" class="form-control-label">Ticket Subject</label>					
					<input required readonly class="form-control" type="text" name="data[subject]" value="<?= $this->breadcrumb->crumb_last ?>" onfocus="focused(this)" onfocusout="defocused(this)">
				  </div>
				  
				  <div class="form-group">
					<label for="example-text-input" class="form-control-label">Describe your need</label>
					<textarea class="form-control" name="data[description]" rows="10"><?= $this->breadcrumb->crumb_last ?></textarea>
				  </div>
				  
				</div>
				<p></p>				
			  </div>
			  
		  <!-- FORM SERVICES -->  
		  <?php if((isset($service['id']) && !empty($service['id'])) && (isset($service['service_table']) && !empty($service['service_table']))){ ?>
			<input type="hidden" name="service_table" value="<?= (isset($service['service_table']) && !empty($service['service_table'])) ? $service['service_table']:'' ?>">
			<?= include_view_service_form($this, $service['slug']) ?>
		  <?php } ?>
		  
		  <hr class="horizontal dark">
		  <button type="submit" class="btn bg-gradient-primary">Send Ticket</button>
		  </form>
		  
		</div>		
		
	</div>
</div>


<?= $this->endSection() ?>
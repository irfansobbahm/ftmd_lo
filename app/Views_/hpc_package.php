<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>

	<div class="card mb-4 w-100">
		<div class="card-header pb-0">
			<a class="btn btn-primary btn-sm mb-0 me-2" href="<?= base_role_url('hpc-package/new-package') ?>"><i class="fa fa-plus"></i> New Package</a>
		</div>
		<div class="card-body px-0 pb-2">
		<div class="table-responsive p-0 mt-2">
			<table class="table align-items-center mb-0">
			  <thead>
				  <th class="text-uppercase text-secondary text-sm font-weight-bolder ">Package Name</th>
				  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder">Operating System</th>
				  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder">Cores</th>
				  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder">RAM(GB)</th>
				  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder ps-2">Storage(GB)</th>
				  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder ps-2">Action</th>				  
				</tr>
			  </thead>
			  <tbody>
			  <?php foreach($data as $row){ ?>
				<tr>									
				  <td class="">
					  <div class="d-flex flex-column justify-content-center">
						<h6 class="mx-3 mb-0 text-sm"><?= $row['title'] ?></h6>
					  </div>						
				  </td>
				  <td class="align-middle text-sm">
					  <div class="d-flex flex-column justify-content-center text-center">
						<h6 class="mb-0 text-sm text-primary"><?= $row['os'] ?></h6>
					  </div>	
				  </td>					  
				  <td class="align-middle text-sm">
					  <div class="d-flex flex-column justify-content-center text-center">
						<h6 class="mb-0 text-sm text-primary"><?= $row['cores'] ?></h6>
					  </div>	
				  </td>					  
				  <td class="align-middle text-center">
					  <div class="d-flex flex-column justify-content-center text-center">
						<h6 class="mb-0 text-sm text-primary"><?= $row['ram'] ?> GB</h6>
					  </div>					  
				  </td>
				  <td class="align-middle text-sm">
					  <div class="d-flex flex-column justify-content-center text-center">
						<h6 class="mb-0 text-sm text-primary"><?= $row['storage'] ?> GB</h6>
					  </div>	
				  </td>						  
				  <td class="align-middle text-sm">
					<a title="Edit Package" data-name="<?= $row['title']; ?>" data-id="<?= $row['id']; ?>" alt="Edit Package" class="btn btn-success btn-xs mb-0 me-2" href="<?= base_role_url('hpc-package/edit-package/'.$row['id']) ?>"><i class="fa fa-pencil"></i></a>
					<?php if($row['id'] != 3){ ?>
					  
					  <a title="Delete Package" data-name="<?= $row['title']; ?>" data-id="<?= $row['id']; ?>" alt="Delete Package" class="btn del-package btn-danger btn-xs mb-0 me-2" href="javascript:void(0);"><i class="fa fa-trash"></i></a>	
					  
					<?php } ?>
				  </td>					  
				
				
				</tr>
			  <?php } ?>			  
			  </tbody>
			</table>
		  </div>
		</div>		
		
	</div>

<script>
$(document).ready(function(){
	$('.del-package').on('click', function()
	{
		thisBtn = $(this);
		Swal.fire({
		  title: 'Confirm Delete Package Name "'+thisBtn.data('name')+'".',
		  text: 'are you sure ?',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, Delete it!'
		}).then((result) => {
		  if (result.isConfirmed) 
		  {
			  $.post(ROLE_BASE_URL+'/hpc-package/delete-package',{
				  'id': thisBtn.data('id'),
				  'name' : thisBtn.data('name')
			  },function(response)
			  {
				  document.location.reload();
			  });
		  }
		});			
	});
});
</script>

<?= $this->endSection() ?>

<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>

	<div class="card p-3">

		<div class="card-body">
			<form method="POST" action="<?= base_role_url('hpc-package/save-package'); ?>">
				<label>Package name</label>
				<input type="hidden" name="app_package_hpc[id]" value="<?= (isset($data['id']) && !empty($data['id'])) ? $data['id']:'' ?>">
				<input class="form-control" type="text" name="app_package_hpc[title]" value="<?= (isset($data['title'])) ? $data['title']:'' ?>" onfocus="focused(this)" onfocusout="defocused(this)">
				<label class="mt-4">Operating System</label>
				<select required class="form-control" name="app_package_hpc[os_id]" onfocus="focused(this)" onfocusout="defocused(this)">
					<option></option>
					<?php foreach($osList as $os){ ?>
					<option <?= (isset($data['os_id']) && !empty($data['os_id']) && $data['os_id'] == $os['id']) ? 'selected':'' ?> value="<?= $os['id'] ?>" <?= $os['id'] == old('app_booking_hpc.os_id') ? 'selected':'' ?> data-os-softwares='<?= json_encode($os['os']) ?>'><?= $os['os'] ?></option>
					<?php } ?>
				</select>				
				<label class="mt-4">Cores</label>
				<input class="form-control" type="number" step="any" name="app_package_hpc[cores]" value="<?= (isset($data['cores'])) ? $data['cores']:'' ?>" onfocus="focused(this)" onfocusout="defocused(this)">
				<label class="mt-4">RAM(GB)</label>
				<input class="form-control" type="number" step="any" name="app_package_hpc[ram]" value="<?= (isset($data['ram'])) ? $data['ram']:'' ?>" onfocus="focused(this)" onfocusout="defocused(this)">			
				<label class="mt-4">Storage(GB)</label>
				<input class="form-control" type="number" step="any" name="app_package_hpc[storage]" value="<?= (isset($data['storage'])) ? $data['storage']:'' ?>" onfocus="focused(this)" onfocusout="defocused(this)">
				<div class="col-12 mt-4">
					<div class="d-flex">
						<button id="save" class="btn btn-primary btn-sm mb-0 me-2" type="submit" name="button">Save</button>
					</div>
				</div>					
			</form>
		</div>	
		
	</div>

<?= $this->endSection() ?>
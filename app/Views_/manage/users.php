<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>

		<div class="card p-3">
	
			<div class="table-responsive">
				<table class="table table-hover align-items-center mb-0">
				  <thead>
					<tr>
					  <th class="text-uppercase text-secondary text-sm font-weight-bolder ">Name</th>
					  <th class="text-uppercase text-secondary text-sm font-weight-bolder">Role</th>
					</tr>
				  </thead>
				  <tbody>
				  <?php foreach($data as $i => $row){ ?>
					<tr>
						<td class="font-weight-bolder text-sm">
							<div class="d-flex px-2 py-1">
							  <div>
								<img src="<?= base_url('assets/img/'.$row['role'].'.png') ?>" class="avatar avatar-sm me-3">
							  </div>
							  <div class="d-flex flex-column justify-content-center">
								<h6 class="mb-0 text-xs"><?= $row['name'] ?> (<?= $row['username'] ?>)</h6>
								<p class="text-xs text-secondary mb-0"><?= $row['email'] ?></p>
							  </div>
							</div>
						</td>						
						<td class="align-middle text-sm text-uppercase font-weight-bolder w-25">
						  <div class="form-group">
							<select class="form-control form-control-sm user-role">
							<?php foreach($roles as $role){ ?>
							  <option data-user="<?= $row['name'] ?>" data-username="<?= $row['username'] ?>" data-user-current-role="<?= $row['role'] ?>" value="<?= $role['identifier'] ?>" <?= ($role['identifier'] == $row['role'])?'selected':'' ?>><?= $role['name'] ?></option>
							<?php } ?>
							</select>
						  </div>							
						</td>
					</tr>
				  <?php } ?>
				  </tbody>
				</table>
			  </div>		
			
		</div>

<script>

$(document).ready(function()
{
	var ROLE_BASE_URL = '<?= base_role_url(); ?>';
	
	$('.user-role').on('change', function()
	{
		var current = $(this).find('option:selected').data('user-current-role');
		var selected = $(this).find('option:selected');
		Swal.fire({
		  title: 'Confirm User Role Changes.',
		  text: 'You about to change user('+$(this).find('option:selected').data('user')+') role from "'+$(this).find('option:selected').data('user-current-role')+'" to "'+$(this).val()+'" , are you sure ?',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, Change it!'
		}).then((result) => {
		  if (result.isConfirmed) 
		  {
			  $.post(ROLE_BASE_URL+'/api/update-user-role',{
				  'change' : {
					  user : selected.data('user'),
					  from : selected.data('user-current-role'),
					  to : $(this).val(),
				  },
				  'username': selected.data('username'),
				  'data':{
					  role: $(this).val()
				  }
			  },function(response)
			  {
				  if(response.status){
					  
					  document.location.reload();
				  }
			  },'json');
		  }else{
			  $(this).val(current);
		  }
		});	
	});
});
</script>
<?= $this->endSection() ?>
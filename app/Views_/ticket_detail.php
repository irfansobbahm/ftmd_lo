<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>

<?= has_ticket_access($ticket) ?>

<div class="row">
	<div class="col-sm-7 px-0 mb-3">
		<div class="card mb-3">
			<div class="card-header pb-0">
			  <h5><b><?= $ticket['service_name'] ?> - Ticket #<?= $ticket_identifier ?></b>
				<p><small class="text-xs">Created <b><?= date('d M Y h:i A', strtotime($ticket['created_at'])) ?></b> by user <b><?= $ticket['name'] ?></b></small><br>			
					<span class="badge badge-sm bg-gradient-danger <?= ($ticket['status'] == 'open')?'':'d-none' ?>"><?= $ticket['status'] ?></span>
					<span class="badge badge-sm bg-gradient-success <?= ($ticket['status'] == 'inprogress')?'':'d-none' ?>"><?= $ticket['status'] ?></span>
					<span class="badge bg-gradient-warning <?= ($ticket['status'] == 'pending')?'':'d-none' ?>"><?= $ticket['status'] ?></span>
					<span class="badge badge-sm bg-gradient-secondary <?= ($ticket['status'] == 'closed')?'':'d-none' ?>"><?= $ticket['status'] ?></span>						
					<?php if($ticket['status'] == 'inprogress'){ ?>
					<span class="badge badge-sm bg-gradient-secondary" title="<?= $ticket['agent_email'] ?>">
						<span class="btn-inner--icon"><i class="fa fa-lock"></i> <?= $ticket['role_name'] ?> - <?= $ticket['agent_name'] ?></span>
					</span>
					<?php } ?>		
					<span class="text-secondary text-xs font-weight-normal ">
						<i class="fa fa-clock me-1" aria-hidden="true"></i> Last update about  <?= date_diff_words($ticket['updated_at'], date('Y-m-d H:i:s')); ?>
					</span>	
					<hr class="horizontal dark my-3">
				</p>
			
			  </h5>
			</div>	
			<!--<hr class="horizontal dark my-3">-->
			<div class="card-body pt-0">
				<form id="form-ticket" method="POST" action="<?= base_role_url('ticket/save-progress-ticket') ?>">	
					<ul class="list-group">
						<li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
						  <div class="d-flex flex-column">
						  <?= $ticket['description']; ?> 
						  </div>
						</li>
					</ul>
				
				<!-- SERVICE FORM -->
				<?php if(isset($ticket['service']) && !empty($ticket['service'])){ ?>
						<?= include_view_service($this, $ticket['service_slug'].'-detail') ?>
				<?php } ?>
				
				<hr class="horizontal dark my-3">
			
				<h5>Progress <span id="label_percent_progress"><?= $ticket['progress'] ?>%</span></h5>
				<input type="hidden" name="data[progress]" id="percent_progress" value="<?= $ticket['progress'] ?>">
				<input type="hidden" name="data[status]" id="ticket_status" value="<?= $ticket['status'] ?>">
				<input type="hidden" name="data[agent]" id="ticket_pic" value="<?= $ticket['agent'] ?>">
				<?php if(isset($ticket['service_model']) && !empty($ticket['service_model'])){ ?>
					<input type="hidden" name="service_model" id="ticket_pic" value="<?= $ticket['service_model'] ?>">
				<?php } ?>
				<input type="hidden" name="action" value="">
				<input type="hidden" name="identifier" value="<?= $ticket['identifier'] ?>">
				<div id="sliderRegular" class="noUi-sm mb-4" <?= (!has_action($ticket,'inprogress')?'disabled':'') ?>></div>
				<hr class="horizontal dark my-3">
				
				<div class="row">
					<div class="col-md-7">
						<!-- STATUS INPROGRESS -->
						<?php if(has_action($ticket,'inprogress')){ ?>
							<a href="<?= base_url(session()->get('role').'/ticket/'.$ticket['identifier'].'/unlock-ticket') ?>" class="my-auto btn bg-gradient-danger btn-xs" type="button" title="Unlock Ticket And Back To Status Open">
								<span class="btn-inner--icon"><i class="fa fa-lock" aria-hidden="true"></i> Unlock Ticket</span>
							</a>				
							<button id="save-progress" class="my-auto btn bg-gradient-success btn-xs" type="submit" title="Save Change">
								<span class="btn-inner--icon"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save Change</span>
							</button>	
							<button id="close-ticket" class="my-auto btn bg-gradient-info btn-xs" type="submit" title="Close Ticket">
								<span class="btn-inner--icon"><i class="fa fa-check" aria-hidden="true"></i> Close Ticket</span>
							</button>											
							<?php } ?>						
						<!-- STATUS OPEN -->
						<?php if(has_action($ticket,'open')){ ?>
							<a href="<?= base_url(session()->get('role').'/ticket/'.$ticket['identifier'].'/lock-ticket') ?>" class="my-auto btn bg-gradient-warning btn-xs" type="button" title="Lock and assign to me.">
								<span class="btn-inner--icon"><i class="fa fa-lock-open" aria-hidden="true"></i> Lock And Assign To Me</span>
							</a>
						<?php } ?>	
									
						<!-- STATUS CLOSED -->
						<?php if(has_action($ticket,'closed')){ ?>
							<button id="re-open-ticket" class="my-auto btn bg-gradient-danger btn-xs" type="submit" title="Re-Open Ticket">
								<span class="btn-inner--icon"> Re-Open Ticket</span>
							</button>						
						<?php } ?>							
					</div>
					<div class="col-md-5">

					<?php if(has_action($ticket,'inprogress')){ ?>
						<select id="change_assignee" class="form-control form-control-sm" onfocus="focused(this)" onfocusout="defocused(this)">
							<option value="">- Change Assignee -</option>
							<?php foreach($agents as $agent){ ?>
							<option value="<?= $agent['username'] ?>"><?= $agent['name'] ?></option>
							<?php } ?>
						</select>		
					<?php } ?>

					</div>						
				</div>				
				</form>
			</div>			
		</div>
	</div>

	<div class="col-sm-5">
		<!-- COMMENTS SECTION -->
		<div class="card mb-3">
			<div class="card-body flex">
				<form method="POST" action="<?= base_url(session()->get('role').'/ticket/send-ticket-comment') ?>">
				<h5>Comments</h5>
				<hr class="horizontal dark my-3">					
				<div class="mb-1">
				<div id="comment_window" class="mb-3 px-0" style="max-height:410px;overflow:auto;">
					<?php if(!empty($comments)){ ?>
						<?php foreach($comments as $comment){ ?>
						<?php if($comment['user'] != session()->get('username')){ ?>
							<div class="d-flex mb-2 <?=$comment['hidden'] == 1 ? 'cu-hide':'' ?>">
							  <div class="flex-shrink-0">
								<img alt="Image placeholder" class="avatar rounded-circle" src="<?= base_url('assets/img/'.$comment['role'].'.png') ?>">
							  </div>
							  <div class="flex-grow-1 ms-3 bg-gray-100 border-radius-lg" style="padding: 4px 10px 0px 10px;">
								<h6 class="text-sm mt-0"><?= $comment['name'] ?> <?= $comment['hidden'] == 1 ? ' <i class="text-warning float-end fa fa-eye-slash" title="hidden for user" alt="hidden for user"></i>':'' ?></h6>
								<div class="text-start text-sm mb-2">
									<?= $comment['comment'] ?>
								</div>
							  </div>
							</div>
						<?php }else{ ?>
							<div class="d-flex mb-2 <?=$comment['hidden'] == 1 ? 'cu-hide':'' ?>">
							  <div class="flex-grow-1 ms-3 bg-gray-100 border-radius-lg" style="padding: 4px 10px 0px 10px;background-color:#e7ffe8 !important;">
								<h6 class="text-end text-sm mt-0"><?= $comment['name'] ?> <?= $comment['hidden'] == 1 ? ' <i class="text-warning float-start fa fa-eye-slash" title="hidden for user" alt="hidden for user"></i>':'' ?></h6>
								<div class="text-end text-sm mb-2">
									<?= $comment['comment'] ?>
								</div>
							  </div>
							  <div class="flex-shrink-0 ms-3">
								<img alt="Image placeholder" class="avatar rounded-circle" src="<?= base_url('assets/img/'.$comment['role'].'.png') ?>">
							  </div>							  
							</div>						
						<?php } ?>
						<?php } ?>
					<?php } ?>
				</div>
					<div class="d-flex">
					  <div class="flex-shrink-0">
						<img alt="Image placeholder" class="avatar rounded-circle me-3" src="<?= base_url('assets/img/'.session()->get('role').'.png') ?>">
					  </div>
					  <div class="flex-grow-1 my-auto">
						  
						  <textarea name="data[comment]" class="form-control" placeholder="Write your comments here" rows="1"></textarea>
						  <input type="hidden" name="data[ticket_identifier]" value="<?= $ticket['identifier'] ?>">
						  <input type="hidden" name="action" value="send_ticket_comment">
							<?php if(in_array(session()->get('role'),['agent','manager'])){ ?>
							<!--<div class="row">
							<div class="col-lg-6">
							<div class="form-check">
								<input name="data[hidden]" class="form-check-input" type="checkbox" value="0" id="fcustomCheck1" checked>
								<label class="custom-control-label" for="fcustomCheck1">Visible for user</label>
							</div>	
							</div>
							<div class="col-lg-6">
							<div class="form-check form-switch float-end">
							  <input id="toggle-hide-comment" class="form-check-input" type="checkbox" id="flexSwitchCheckDefault" checked="">
							  <label></label>
							</div>				
							</div>
							</div>-->
							<input name="data[hidden]" class="form-check-input" type="hidden" value="0">
							<?php }else{ ?>
							<input name="data[hidden]" class="form-check-input" type="hidden" value="0">
							<?php } ?>
					  </div>
					</div>
					<hr class="horizontal dark my-3">
				</div>								

				<button type="submit" class="my-auto btn btn-xs btn-secondary">Send Comments</button>	
				</form>
			</div>
		</div>			
		<!-- TIMELINE SECTION -->	
		<div class="card mb-3">
			<div class="card-body">
				<h5>Ticket Timeline</h5>
				<hr class="horizontal dark my-3">			
				<div class="timeline timeline-one-side">

					<?php foreach($ticket['logs'] as $logs){ ?>
					<div class="timeline-block mb-3">
					  <span class="timeline-step">
						<i class="<?= $logs->icon_class ?>"></i>
					  </span>
					  <div class="timeline-content">
						<h6 class="text-dark text-sm font-weight-bold mb-0"><?= $logs->title ?></h6>
						<p class="text-secondary font-weight-bold text-xs mt-1 mb-0"><?= date('d M Y h:i A', strtotime($logs->log_date)) ?></p>
						<?php 
							$logs->message_format = str_replace('{username}',$logs->actor_name,$logs->message_format); 
							$logs->message_format = str_replace('{TICKETNUMBER}',$logs->ticket_identifier,$logs->message_format);
							
							echo $logs->message_format;
						?>
						
					
					  </div>
					</div>
					<?php } ?>
				  </div>			
			</div>
		</div>			
	</div>
</div>
  <script>
  
  BASE_URL = "<?= base_role_url() ?>";
  TICKET_ID = "<?= $ticket['identifier'] ?>";
  
   var slider = document.getElementById('sliderRegular');
  $(document).ready(function(){
	

	
	$('#toggle-hide-comment').on('click',function(){
		checkBoxes = $(this);
		if(!checkBoxes.prop("checked")){
			$('.cu-hide').attr('style','display:none !important;');
			//$('.cu-hide').hide();
		}else{
			$('.cu-hide').attr('style','display:flex !important;');
			//$('.cu-hide').show();
		}
		
	});
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#comment_window')) {
      var options = {
        damping: '0.5'
      }
      Scrollbar.init(document.querySelector('#comment_window'), options);
    }
  	  
	  
      
        noUiSlider.create(slider, {
        start: <?= $ticket['progress'] ?>,
        connect: [true, false],
        range: {
          min: 0,
          max: 100
        },
		behaviour: 'tap-drag',
		//tooltips: true,
		format: wNumb({
			decimals: 0,
			suffix: '%'
		}),

		// Show a scale with the slider
		pips: {
			mode: 'steps',
			stepped: true,
			density: 4,
			format: wNumb({
				decimals: 0,
				suffix: '%'
			})			
		}		
      }); 
	  
	  slider.noUiSlider.on('slide', function(values, handle, unencoded, tap, positions, noUiSlider){
		  $('#percent_progress').val(parseInt(values));
		  $('#label_percent_progress').text(values);
	  });
	  //slider.setAttribute('disabled', true);
	  
	  $('#save-progress').on('click', function(e){
		  $('input[name="action"]').val('change_progress_ticket');
		  $( "#form-ticket").submit();
	  });

	  $('#re-open-ticket').on('click', function(e){
		  $('input[name="action"]').val('re_open_ticket');
		  $('#ticket_status').val('open');
		  $('#ticket_pic').val('');
		  
		  $( "#form-ticket").submit();
	  });	  
	  //SWAL
	  $('#close-ticket').on('click',function(e){
		e.preventDefault();
		Swal.fire({
		  title: 'Are you sure close this ticket?',
		  text: (parseInt($('#percent_progress').val()) < 100) ? "Currently ticket progress is "+$('#percent_progress').val()+"%" : "",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, close this ticket!'
		}).then((result) => {
		  if (result.isConfirmed) {
			// Swal.fire(
			  // 'Ticket Closed.',
			  // 'Ticket succefully closed.',
			  // 'success'
			// )
			$('input[name="action"]').val('close_ticket');
			$('#ticket_status').val('closed');
			
			$( "#form-ticket").submit();
		  }
		});		  
	  });
		
	  $('#change_assignee').on('change', function(e){
		  newAssigneeLabel = $(this).find(':selected').text();
		  newAssigneeValue = $(this).val();
		  if(newAssigneeValue != ''){
			Swal.fire({
			  title: 'Are you sure change ticket PIC to '+newAssigneeLabel+' ?',
			  text: "",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, change it for me!'
			}).then((result) => {
			  if (result.isConfirmed) 
			  {
				  $('input[name="action"]').val('change_agent_incharge');
				  $('#ticket_pic').val(newAssigneeValue);
				  $( "#form-ticket").submit();
			  }
			});		  
		  }
	  });
	  
	  
	  
	});  
		var password=document.getElementById("password");

		 function genPassword() {
			var chars = "0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*()ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			var passwordLength = 12;
			var password = "";
		 for (var i = 0; i <= passwordLength; i++) {
		   var randomNumber = Math.floor(Math.random() * chars.length);
		   password += chars.substring(randomNumber, randomNumber +1);
		  }
				document.getElementById("password").value = password;
				$('#lb-pass').text($('#password').val());
		 }

		function copyPassword() {
		  var copyText = document.getElementById("password");
		  copyText.select();
		  document.execCommand("copy");  
		}	
  </script>

<?= $this->endSection() ?>
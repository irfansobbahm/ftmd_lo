<!--
=========================================================
* Argon Dashboard 2 - v2.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard
* Copyright 2022 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://www.creative-tim.com/license)
* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url('assets/img/apple-icon.png'); ?>">
  <link rel="icon" type="image/png" href="<?= base_url('assets/img/favicon.png'); ?>">
  <title>
    FTMD - Learning Outcome
  </title>
  <!--     Fonts and icons     
  <link href="<?= base_url('assets/css/open-sans.css') ?>" rel="stylesheet" />-->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
  <!-- Nucleo Icons -->
  <link href="<?= base_url('assets/css/nucleo-icons.css'); ?>" rel="stylesheet" />
  <link href="<?= base_url('assets/css/nucleo-svg.css'); ?>" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <link href="<?= base_url('assets/fontawesome5/css/all.css'); ?>" rel="stylesheet" />
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  
  <link href="<?= base_url('assets/css/nucleo-svg.css'); ?>" rel="stylesheet" />
  <!-- CSS Files <link id="pagestyle" href="<?= base_url('assets/css/argon-dashboard.css?v=2.0.0'); ?>" rel="stylesheet" />-->
  
  <link id="pagestyle" href="<?= base_url('assets/css/argon-dashboard-pro.min.css?v=2.0.0'); ?>" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/css/select2.min.css'); ?>">
  
  <!-- MY CSS -->
  <link rel="stylesheet" href="<?= base_url('assets/css/custom.css'); ?>">
  
  <script src="<?= base_url("assets/js/plugins/jquery-3.6.0.min.js"); ?>"></script>
  
  
  <link href="<?= base_url('assets/signature-pad-main/assets/jquery.signaturepad.css'); ?>" rel="stylesheet">
  
  <script>
  var ROLE_BASE_URL = '<?= base_role_url() ?>';
  </script>
</head>

<body class="g-sidenav-show   bg-gray-100">
<?php
	$session = session();
	$menuModel = model('App\Models\MenuModel');
	$menus = $menuModel->getMenus(($session->get('role'))? $session->get('role') : '',1);
?>
  <div class="min-height-300 position-absolute w-100" style="background-color:#005aab;"></div>

<?php if(!empty($session->get('role'))){ ?>
  <aside style="overflow: hidden !important;" class="sidenav bg-white navbar navbar-vertical navbar-expand-xs border-0 fixed-start " id="sidenav-main">
     <div style="text-align:center;">
		<img class="text-center" src="<?= base_url('assets/img/logo_itb_1024.png') ?>" style="width: 140px !important;margin:10px;" alt="main_logo">
		
	</div>
    <hr class="horizontal dark mt-0">
    <div class="menu_ w-auto overflow-auto flex-shrink-1" id="sidenav-collapse-main">

<?php
	$html = '';
	$_menus = function(array $obj, &$html, $submenu=0,$id=0) use (&$_menus, $menuModel, $menus, $session)
	{
		$menu_path = $session->get('role_path');
		$rand = rand(1000,9999);
		$html .= '<ul class="navbar-nav'.($submenu?' collapse ms-3 show':'').'" '.($submenu?' id="'.$id.'"':'').'>'; 
		foreach($obj as $menu)
		{
			if(!empty($menu->method))
			{
				
				if(count($menu->submenu) > 0)
				{
					$html .= '<li class="nav-item dropdown">';
					$html .= '<a class="nav-link dropdown-toggle collapse text-uppercase text-uppercase font-weight-bolder" data-bs-toggle="collapse" aria-expanded="true" href="#'.slugify($menu->label).'">';
					$html .= '	<div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">';
					$html .= '	  <i class="ni '.$menu->icon_class.' text-primary text-sm opacity-10"></i>';
					$html .= '	</div>';
					$html .= '	<span class="nav-link-text ms-1">'.$menu->label.'</span>';					
					$html .= '  </a>';
					$_menus($menu->submenu, $html,count($menu->submenu),slugify($menu->label));	
					
					$html .= '</li>';					
								
				}else{
					$html .= '<li class="nav-item">';	
					$html .= '  <a class="nav-link '.(current_url(true)->getPath() == '/'.$menu_path[$menu->slug] ? 'active':'').'" data-color="primary" href="'.base_url($menu_path[$menu->slug]).'">';
					$html .= '	<div class="icon icon-shape icon-sm border-radius-md text-center me-2 d-flex align-items-center justify-content-center">';
					$html .= '	  <i class="ni '.$menu->icon_class.' text-primary text-sm opacity-10"></i>';
					$html .= '	</div>';
					$html .= '	<span class="nav-link-text ms-1">'.$menu->label.'</span>';
					// badge count ticket
					if($menu->has_badge){
						$badge_helpers = call_user_func_array($menu->badge_helpers,[$menu]);
						if($badge_helpers > 0){
							$badge_helpers = ($badge_helpers > 99) ? '99+' : $badge_helpers;
							$html .= '<span class="badge badge-sm badge-danger badge-sidenav">'.$badge_helpers.'</span>';
						}
					}
					$html .= '	';
					$html .= '  </a>';	
					$html .= '</li>';
				}
				
			}
		}
		$html .='</ul>';
		return $html;
	};
	$_menus($menus, $html);	
	
	
	echo $html;

?>	  
    </div>
	<!--
    <div class="sidenav-footer mx-3 ">
      <div class="card card-plain shadow-none" id="sidenavCard">
        <img class="w-50 mx-auto" src="../assets/img/illustrations/icon-documentation.svg" alt="sidebar_illustration">
        <div class="card-body text-center p-3 w-100 pt-0">
          <div class="docs-info">
            <h6 class="mb-0">Need help?</h6>
            <p class="text-xs font-weight-bold mb-0">Please check our docs</p>
          </div>
        </div>
      </div>
      <a href="https://www.creative-tim.com/learning-lab/bootstrap/license/argon-dashboard" target="_blank" class="btn btn-dark btn-sm w-100 mb-3">Documentation</a>
      <a class="btn btn-primary btn-sm mb-0 w-100" href="https://www.creative-tim.com/product/argon-dashboard-pro?ref=sidebarfree" type="button">Upgrade to pro</a>
    </div>
	-->
  </aside>
<?php } ?>
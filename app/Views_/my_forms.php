<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>

<div class="row">
<div class="card col-lg-8 m-auto">
<div class="card-body ps-0 pe-0">
  <div class="table-responsive">
    <table class="table align-items-center mb-0">
      <thead>
        <tr>
          <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">Form Title</th>
          <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">Question Count</th>
		  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">Answer</th>
		  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2 text-center">Action</th>
          <th class="text-secondary opacity-7"></th>
        </tr>
      </thead>
      <tbody>
		<?php foreach($userForms as $fields){ ?>
        <tr>
          <td>
            <div class="d-flex px-2 py-1">
              <div class="d-flex flex-column justify-content-center">
                <h6 class="mb-0 text-sm"><?= $fields->title ?></h6>
              </div>
            </div>
          </td>
          <td>
            <p class="text-sm font-weight-bold mb-0"><?= $fields->fieldscount ?></p>
          </td>
		  <td>
			0
		  </td>
          <td class="align-middle text-center text-sm">
            <a href="<?= base_role_url('forms/'.$fields->id.'/view') ?>" class="btn btn-xs btn-primary">View</a>
			<a href="javascript:void(0);" class="btn btn-sm btn-success">Share</a>
          </td>
        </tr>
		<?php } ?>
 


      </tbody>
    </table>
  </div>
  </div>
</div>
</div>


<?= $this->endSection() ?>
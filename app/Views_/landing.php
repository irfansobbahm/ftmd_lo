<?= $this->extend('layout/landing') ?>

<?= $this->section('content') ?>

<?php $session = session(); ?>
<main class="main-content  mt-0">
<section>
<div class="page-header min-vh-100">
<div class="container">
<div class="row">
<div class="col-xl-4 col-lg-5 col-md-7 d-flex flex-column mx-lg-0 mx-auto">
<div class="card" style="padding:20px;">
<div class="card-body">
	<div class=" pb-0 text-start">
	<h4 class="font-weight-bolder">Sign In</h4>
	<p class="mb-0">Login with your ITB SSO account</p><br>
	</div>
	<a href="<?= base_url($session->get('role').'/home/login') ?>" class="btn btn-primary btn" style="widht:100%;">Login With SSO</a>
</div>
</div>
</div>
<div style="max-height:700px !important;" class="col-6 d-lg-flex d-none h-100 my-auto pe-0 position-absolute top-0 end-0 text-center justify-content-center flex-column">
<div class="position-relative bg-gradient-primary h-100 m-3 px-7 border-radius-lg d-flex flex-column justify-content-center overflow-hidden" style="background-image: url('https://www.itb.ac.id/files/dokumentasi/1611992365-DSC_8353.JPG');
          background-size: cover;">
<span class="mask bg-gradient-secondary opacity-6"></span>
<h4 class="mt-5 text-white font-weight-bolder position-relative">"Attention is the new currency"</h4>
<p class="text-white position-relative">The more effortless the writing looks, the more effort the writer actually put into the process.</p>
</div>
</div>
</div>
</div>
</div>
</section>
</main>
<?= $this->endSection() ?>
<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>

<div class="row">
	<div class="col-lg-12 m-auto">
		<div class="card" id="sessions">
		<div class="card-header pb-3">

		</div>
		<div class="card-body pt-0">
		
		<?php if(count($data) > 0){ ?>
		<?php foreach($data as $row){ ?>
		<?php $bookingDates = $model->getBookingDates($row['id']); ?>
			<div class="d-flex align-items-center">
				<div class="text-center w-5">
					<p class="text-xs"><?= $row['os'] ?></p>
					<i class="fas fa-desktop text-lg opacity-6" aria-hidden="true"></i>
				</div>
				<div class="my-auto ms-3">
					<div class="h-100">
						<p class="text-md mb-1">
							 <?= $row['node_ip'] ?>
							 (<a href="<?= base_role_url('ticket/'.$row['ticket_identifier'].'/view-detail') ?>">#<?= $row['ticket_identifier'] ?></a>)
						</p>
						
						<p class="mb-0 text-xs">
							User : <?= $row['os_user'] ?>
						</p>
						<p class="mb-0 text-xs">
							Password : <?= $row['node_pass'] ?>
						</p>		

					</div>
				</div>
				<?php $vuntil = end($bookingDates); ?>
				<?php $vfrom = $bookingDates[0]; ?>
				
				<?php if(strtotime($vuntil['date']) >= strtotime(date('Y-m-d'))){ ?>
				<span class="badge badge-success badge-sm my-auto ms-auto me-3">
				
				<!--Active from <x style='color:red;'><?= date('j F Y', strtotime($vfrom['date'])) ?></x>-->
				
				Expire on <x style='color:red;'><?= date('j F Y', strtotime($vuntil['date'])) ?></x>
				
				</span>
				<?php }else{ ?>
				<span class="badge badge-secondary badge-sm my-auto ms-auto me-3">
					Inactive
				</span>				
				<?php } ?>
			</div>
			<hr class="horizontal dark">
		<?php } ?>
		<?php }else{ ?>
		<span class="text-sm">Theres no related HPC Account for you at the moments.</span>
		<?php } ?>
		</div>
		</div>
	</div>	
</div>

<?= $this->endSection() ?>


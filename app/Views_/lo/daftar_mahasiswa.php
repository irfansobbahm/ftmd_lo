<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>


	<div class="row">
		<div class="card mb-3 p-4">
			<div class="card-header p-0 pb-3">
				<div class="row">
					<div class="col-md-8 d-flex align-items-center">
					<h4 class="mb-0">FTMD - Teknik Dirgantara</h4>
					</div>
				</div>
			</div>	
			<form method="GET" action="">
				<div class="row mb-3">
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">Tahun Akademik </label>
						<select class="form-control form-control-md" id="tahun_akademik">
						<?php foreach($ws['tahunkuliah'] as $tahunkuliah){ ?>
							<option value="<?= $tahunkuliah ?>" <?= (isset($uriSegments[3]) && $uriSegments[3] == $tahunkuliah) ? 'selected':'' ?>><?= $tahunkuliah.'/'.($tahunkuliah +1) ?></option>
						<?php } ?>
						</select>
					
					</div>	
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">Semester</label>
						<?php if(session()->get('role') == 'dosen'){ ?>

							<select class="form-control form-control-md" id="semester">
							<?php foreach(['Ganjil','Genap'] as $sem){ ?>
								<option <?= (date('n')<=6 && $sem == 'Genap' ? 'selected':'') ?> value="<?= htmlentities(strtolower($sem)) ?>"><?= $sem ?></option>
							<?php } ?>
							<option>Semester Pendek</option>
							</select>


						<?php } else { ?>
							<select class="form-control form-control-md" id="semester">
								<option value="">Seluruh</option>
								<?php for($n=0;$n<=13;$n++){ ?>
								<option value=""><?= $n + 1 ?></option>
								<?php } ?>
							</select>
						<?php } ?>
						

					
					</div>				
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">Matakuliah</label>
						<select class="form-control form-control-md"  id="kd_kuliah">
						<?php foreach($ws['matakuliah'] as $kd_mk => $matakuliah){ ?>
							<option value="<?= $kd_mk ?>" <?= (isset($uriSegments[2]) && $uriSegments[2] == $kd_mk) ? 'selected':'' ?>><?= $matakuliah.' (<b>'.$kd_mk.'</b>)' ?></option>
						<?php } ?>
						</select>
					
					</div>					
						
				</div>
				<div class="row mt-1 mb-2">
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">NIM Mahasiswa</label>
						<div class="form-group">
							<input type="text" id="nim_mhs" name="nim" class="form-control">
						</div>
					
					</div>	
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">Nama Mahasiswa</label>
						<div class="form-group">
							<input type="text" id="nama_mhs" name="nama" class="form-control">
						</div>
					
					</div>			
				</div>
				<div class="row">
					<div class="col">
						<button type="submit" class="btn btn-primary btn-sm">Tampilkan Data</button>					
					</div>	
					<div class="col text-end">
						<button type="button" class="btn btn-primary btn-sm">Download Rekap Nilai</button>
						<button type="button" class="btn btn-success btn-sm">Upload Rekap Nilai</button>				
					</div>
				</div>
			</form>
		</div>
	</div>
	
	<div class="row">
		<div class="card mb-3 col" style="margin-right:20px;">	
			<div class="row mt-2" style="margin-right:10px;margin-left:10px;">
				<div class="col-md-8 d-flex align-items-center">
					<h4 class="mb-0">Keterangan Nilai</h4>
				</div>
				<div class="col-md-4 text-end">				
					<a href="javascript:;" title="Edit">
						<i class="fas fa-pencil-alt text-secondary text-sm" data-bs-toggle="tooltip" data-bs-placement="top" aria-hidden="true" aria-label="Edit Profile"></i><span class="sr-only">Edit Profile</span>
					</a>
				</div>
			</div>		
			<div class="row">
				<div class="table-responsive">
					<table class="table align-items-center mb-0">
					  <thead>
						<tr>
						  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">Nilai</th>
						  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">Batas</th>
						  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">Jumlah</th>
						  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">Bobot</th>
						</tr>
					  </thead>
					  <tbody>
					  <tr>
						<td class="text-sm text-center">A</td>
						<td contenteditable class="text-sm text-center">80</td>
						<td contenteditable class="text-sm text-center">11</td>
						<td contenteditable class="text-sm text-center">4</td>
					  </tr>
					  <tr>
						<td class="text-sm text-center">AB</td>
						<td contenteditable class="text-sm text-center">75</td>
						<td contenteditable class="text-sm text-center">2</td>
						<td contenteditable class="text-sm text-center">3,5</td>
					  </tr>
					  <tr>
						<td class="text-sm text-center">B</td>
						<td contenteditable class="text-sm text-center">70</td>
						<td contenteditable class="text-sm text-center">0</td>
						<td contenteditable class="text-sm text-center">3</td>
					  </tr>
					  <tr>
						<td class="text-sm text-center">BC</td>
						<td contenteditable class="text-sm text-center">65</td>
						<td contenteditable class="text-sm text-center">0</td>
						<td contenteditable class="text-sm text-center">2,5</td>
					  </tr>
					  <tr>
						<td class="text-sm text-center">C</td>
						<td contenteditable class="text-sm text-center">60</td>
						<td contenteditable class="text-sm text-center">0</td>
						<td contenteditable class="text-sm text-center">2</td>
					  </tr>
					  <tr>
						<td class="text-sm text-center">D</td>
						<td contenteditable class="text-sm text-center">50</td>
						<td contenteditable class="text-sm text-center"v>0</td>
						<td contenteditable class="text-sm text-center">1</td>
					  </tr>
					  <tr>
						<td class="text-sm text-center">E</td>
						<td contenteditable class="text-sm text-center">0</td>
						<td contenteditable class="text-sm text-center">0</td>
						<td contenteditable class="text-sm text-center">0</td>
					  </tr>					  
					  </tbody>
					</table>
				  </div>		
			</div>
		</div>
		<div class="card mb-3 p-4 col">
			<div style="  margin: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);"><h2>IP Kelas : 3,92</h2></div>
		</div>
	</div>

	<div class="row mt-0">
		<div class="card">
<div class="table-responsive">
			<table class="table align-items-center mb-0">
			  <thead>
				<tr>
				  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
				  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">UAS</th>
				  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">UTS</th>
				  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">Praktikum</th>
				  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">Kehadiran</th>
				  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">Quiz</th>
				  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">PR</th>
				  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">#</th>
				  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">#</th>
				</tr>
			  </thead>
			  <tbody>
				<tr>
					<td class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">RATA2</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
				</tr>
				<tr>
					<td class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">DEVIASI</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
				</tr>
				<tr>
					<td class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">BOBOT (0-1)</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
					<td contenteditable class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">&nbsp;</td>
				</tr>				
			  </tbody>
			</table>
		  </div>
		</div>
	</div>

	<div class="row mt-3">
		<div class="card">
			<div class="row mt-2" style="margin-right:10px;margin-left:10px;">
				<div class="card-header col-md-8 p-0 pb-3">
					<div class="row">
						<div class="col-md-10 d-flex align-items-center">
						<h4 class="mb-0">Mahasiswa - <b>Matakuliah 
						<?= isset($uriSegments[2]) ? $uriSegments[2] : 'MT3103'?>
						(<?= isset($uriSegments[3]) ? $uriSegments[3].'/'.($uriSegments[3] + 1) : date('Y').'/'.(date('Y') + 1) ?>) </b></h4>
						</div>
					</div>
				</div>			
				<div class="col-md-4 text-end">
					<a href="javascript:;" onclick="inputKategoriNilaiBaru()" title="Tambah kategori nilai">
						<i class="fas fa-plus text-secondary text-sm" data-bs-toggle="tooltip" data-bs-placement="top" aria-hidden="true" aria-label="Edit Profile"></i><span class="sr-only">Edit Profile</span>
					</a>
&nbsp;					
					<a href="javascript:;" onclick="confirm()" title="Edit">
						<i class="fas fa-pencil-alt text-secondary text-sm " data-bs-toggle="tooltip" data-bs-placement="top" aria-hidden="true" aria-label="Edit Profile"></i><span class="sr-only">Edit Profile</span>
					</a>
				</div>
			</div>				
		  <div class="table-responsive">
			<table class="table align-items-center mb-0">
			  <thead>
				<tr>
				  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">Mahasiswa</th>
				  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">UAS</th>
				  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">UTS</th>
				  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">Praktikum</th>
				  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">Kehadiran</th>
				  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">Quiz</th>
				  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">PR</th>
				  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">Total</th>
				  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">Huruf</th>
				</tr>
			  </thead>
			  <tbody>
			  <?php foreach($ws['mahasiswa'] as $row){ ?>
				<tr>
				  <td>
					<div class="d-flex px-2 py-1">
					  <div>
						<img src="https://upload.wikimedia.org/wikipedia/id/9/95/Logo_Institut_Teknologi_Bandung.png" class="avatar avatar-sm me-3">
					  </div>
					  <div class="d-flex flex-column justify-content-center">
						<h6 class="mb-0 text-xs"><?= $row->nama; ?></h6>
						<p class="text-xs text-secondary mb-0">NIM <?= $row->nim; ?></p>
					  </div>
					</div>
				  </td>
				  <td contenteditable class="align-middle text-center">
					<?= $a = rand(80,90); ?>
				  </td>
				  <td contenteditable class="align-middle text-center text-sm">
					<?= $b = rand(80,90); ?>
				  </td>
				  <td contenteditable class="align-middle text-center">
					<?= $c =rand(80,90); ?>
				  </td>
				  <td contenteditable class="align-middle text-center">
					<?= $d =rand(80,90); ?>
				  </td>
				  <td contenteditable class="align-middle text-center">
					<?= $e =rand(80,90); ?>
				  </td>
				  <td contenteditable class="align-middle text-center">
					<?= $f =rand(80,90); ?>
				  </td>
				  <td contenteditable class="align-middle text-center">
					<?= $g =rand(80,90); ?>
				  </td>	
				  <td class="align-middle text-center">
					<?= $h = substr(str_shuffle('ABCDE'),1,1); ?>
				  </td>				  
				</tr>
			  <?php } ?>
			  </tbody>
			</table>
		  </div>
			
		</div>
	</div>
	<script>
	
	function confirm(){
		Swal.fire({
		  title: 'Konfirmasi',
		  text: "Anda akan melakukan pengajuan perubahan nilai.",
		  icon: 'question',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Saya mengerti, dan Ajukan.',
		  cancelButtonText: 'Batal'
		}).then((result) => {
		  if (result.isConfirmed) {
			Swal.fire(
			  'Berhasil!',
			  'Pengajuan telah berhasil di kirim.',
			  'success'
			)
		  }
		})		
	}
	
	function inputKategoriNilaiBaru()
	{
		form = '';
		form += '<div class="form-group">';
		form += '	<label for="example-text-input" class="form-control-label">Nama Kategori Nilai</label>';
		form += '	<input required="" value="" class="form-control" type="text" onfocus="focused(this)" onfocusout="defocused(this)">';
		form += '</div>';
		
		Swal.fire({
		  title: 'Input Kategori Nilai Baru',
		  html:form,
		  showCloseButton: true,
		  showCancelButton: false,
		  focusConfirm: false,
		  confirmButtonText:
			'Simpan',
		  cancelButtonText:
			'<i class="fa fa-thumbs-down"></i>',
		  cancelButtonAriaLabel: 'Thumbs down'
		})		
	}
	
	$(function()
	{
		$('#kd_kuliah, #tahun_akademik, #semester').on('change',function(){
			var kd_kuliah = $('#kd_kuliah').val();
			var tahun_akademik = $('#tahun_akademik').val();
			var semester = $('#semester').val();
			var nim_mhs = $('#nim_mhs').val();
			var nama_mhs = $('#nama_mhs').val();	
			
			document.location.href='<?= base_role_url(); ?>/rekap-nilai/'+kd_kuliah+'/'+tahun_akademik+'/'+semester;
		});
	});
	
	</script>
<?= $this->endSection() ?>
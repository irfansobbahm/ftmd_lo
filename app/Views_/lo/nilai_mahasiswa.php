<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>
	<div class="row">
		<div class="card mb-3 p-4">
			<div class="card-header p-0 pb-3">
				<div class="row">
					<div class="col-md-8 d-flex align-items-center">
					<h4 class="mb-0">FTMD - Teknik Dirgantara</h4>
					</div>
				</div>
			</div>	
			<div class="row mb-3">
				<div class="col-sm-4 col-6">
					<label class="form-label text-lg">Matakuliah</label>
					<select class="form-control form-control-md" name="choices-gender" id="choices-gender">
						<option value="AE4025">Teknik Simulasi Terbang  ( AE4025 )</option>
					</select>
				
				</div>	
				<div class="col-sm-4 col-6">
					<label class="form-label text-lg">Tahun Akademik</label>
					<select class="form-control form-control-md" name="choices-gender" id="choices-gender">
						<option value="2020/2021">2020/2021</option>
					</select>
				
				</div>	
				<div class="col-sm-4 col-6">
					<label class="form-label text-lg">Semester</label>
					<select class="form-control form-control-md" name="choices-gender" id="choices-gender">
						<option value="">Seluruh</option>
						<?php for($n=0;$n<=13;$n++){ ?>
						<option <?php echo $n == 0 ? 'selected':''; ?> value=""><?= $n + 1 ?></option>
						<?php } ?>
					</select>
				
				</div>			
			</div>
			<div class="row">
				<div class="col-sm-6 col-6">
					<button type="button" class="btn btn-primary btn-sm">Tampilkan Data</button>
				</div>	
			</div>
		</div>
	</div>

	<div class="row">
		<div class="card p-4">
			<div class="card-header p-0 pb-3">
				<div class="row">
					<div class="col-md-8 d-flex align-items-center">
					<h4 class="mb-0">Semester 1</h4>
					</div>
				</div>
			</div>		
			<div class="table-responsive">
			<table class="table align-items-center mb-0">
			  <thead>
				<tr>
				  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">Kategori Nilai</th>
				  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">Nilai</th>
				</tr>
			  </thead>			
			  <tbody>
			  <tr>
				<th>UAS</th>
				<th><?= $h = substr(str_shuffle('AB'),1,1); ?></th>
			  </tr>
			  <tr>
				<th>UTS</th>
				<th><?= $h = substr(str_shuffle('AB'),1,1); ?></th>
			  </tr>
			  <tr>
				<th>Praktikum</th>
				<th><?= $h = substr(str_shuffle('AB'),1,1); ?></th>
			  </tr>		
			  <tr>
				<th>Kehadiran</th>
				<th><?= $h = substr(str_shuffle('AB'),1,1); ?></th>
			  </tr>
			  <tr>
				<th>Quiz</th>
				<th><?= $h = substr(str_shuffle('AB'),1,1); ?></th>
			  </tr>
			  <tr>
				<th>PR</th>
				<th><?= $h = substr(str_shuffle('AB'),1,1); ?></th>
			  </tr>			  
			  </tbody>
			</table>
		  </div>		
		</div>
	</div>
<?= $this->endSection() ?>
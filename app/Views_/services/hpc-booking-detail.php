					<hr class="horizontal dark my-3">
					<div class="row">
					<div class="col-lg-6">
					<h6 class="mb-3">User Information</h6>	
					<ul class="list-group">
						<li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
						  <div class="d-flex flex-column">
							<h6 class="mb-3 text-sm"><?= $ticket['full_name']; ?></h6>
							<span class="mb-2 text-sm">NIM/NIK : <span class="text-dark font-weight-bold ms-2"><?= $ticket['service']['nim_nik']; ?></span></span>
							<span class="mb-2 text-sm">E-Mail : <span class="text-dark ms-2 font-weight-bold"><?= $ticket['service']['email']; ?></span></span>
							<span class="mb-2 text-sm">Telephone : <span class="text-dark ms-2 font-weight-bold"><?= $ticket['service']['tlp']; ?></span></span>
							<span class="mb-2 text-sm">HPC Used For : <span class="text-dark ms-2 font-weight-bold"><?= $ticket['service']['purpose']; ?></span></span>
							<span class="mb-2 text-sm">Reference Document : <span class="text-light ms-2 font-weight-bold"><a class="text-info" href="<?= base_url('docs/'.$ticket['service']['doc_id'].'.pdf') ?>">Document</a></span></span>
						  </div>
						</li>
					</ul>
					</div>
					<div class="col-lg-6">
					<h6 class="mb-3">Booking Information</h6>	
					<ul class="list-group">
						<li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
						  <div class="d-flex flex-column">
							<h6 class="mb-3 text-sm">
								
								<span class="badge badge-info text-dark text-lg">
								<input type="text" name="package_id" value="<?= $ticket['service']['package_id']; ?>">
								<?= $ticket['service']['package_title']; ?>(Package)</span>
								<br>
								<span class="text-dark font-weight-bold text-xs">Cores : <?= $ticket['service']['package_cores']; ?></span>,
								<span class="text-dark font-weight-bold text-xs">RAM : <?= $ticket['service']['package_ram']; ?> GB</span>,
								<span class="text-dark font-weight-bold text-xs">Storages : <?= $ticket['service']['package_storage']; ?> GB</span>

							</h6>
							<div class=" mb-2">
								<div class="col">
									<span for="example-text-input" class="text-xs form-control-label">Booking Date</span>
								</div>
							</div>
				
							<div class=" mb-2">
								<!--<div class="col">
									<span for="example-text-input" class="text-xs form-control-label">Booking Date</span>
								</div>-->
									
									<?php $dates = $ticket['service']['booking_dates']; $flatpd =[];?>
									<?php foreach($dates as $date){ $flatpd[] = $date['date'];?>
									<span class="bookingdates badge badge-success d-inline text-xxs p-1 m-1 text-dark " data-date="<?= date('Y-m-d',strtotime($date['date'])) ?>">
									<?= date('d M Y',strtotime($date['date'])) ?>
									</span>
									<?php } ?>
							</div>
							<!-- CALENDAR -->
							<div class="row">
								<input id="datepicker" type="hidden">						
							</div><br>		
							<div class="row mb-2">
								<div class="col">
									<span for="example-text-input" class="text-sm form-control-label">Operating System</span>
								</div>
								
								<div class="col px-0">
									<?php if(has_action($ticket,'inprogress') && $ticket['service']['booking_status'] == 'new'){ ?>
									<select required="" id="hpc_os" class="form-control form-control-sm" data-linux='' data-windows='' name="<?= (isset($ticket['service_table']) && !empty($ticket['service_table']))?$ticket['service_table'].'[os_id]':'' ?>" onfocus="focused(this)" onfocusout="defocused(this)">
										<?php foreach(model('App\Models\App\hpcOSModel')->findAll() as $row){ ?>
										<option value="<?= $row['id'] ?>" <?= ($row['id'] == $ticket['service']['os_id'])?'selected':'' ?>><?= $row['os'] ?></option>
										<?php } ?>
									</select>
									<?php }else{ ?>
										<span class="text-sm"><?= $ticket['service']['os']; ?></span>
									<?php } ?>
								</div>
							</div>	
							
							
							<!-- NODE -->
							<div class="row mb-2">
								<div class="col">
									<span for="example-text-input" class="text-xs form-control-label">Node</span>
								</div>

								<div class="col col-sm px-0" id="nodes">
									<?php if($ticket['service']['booking_status'] == 'new' && has_action($ticket,'inprogress')){ ?>
									<select id="hpc_os_nodes" required="" data-os-user="<?= (isset($ticket['service']['os_user']) && !empty($ticket['service']['os_user'])) ? $ticket['service']['os_user'] : $ticket['username'] ?>" data-os-password="<?= (isset($ticket['service']['os_user']) && !empty($ticket['service']['os_password'])) ? $ticket['service']['os_password'] : '' ?>" class="form-control form-control-sm" name="app_hpc_node_users[node_id]" onfocus="focused(this)" onfocusout="defocused(this)">
										
										<?php foreach($defaultNodes as $node){ ?>
										<option value="<?= $node['id'] ?>" <?= ($ticket['service']['node_id'] == $node['id'])? 'selected':'' ?>><?= $node['ip'] ?></option>
										<?php } ?>
									</select>
									<?php } else{ ?>
										<?php if(in_array($ticket['service']['booking_status'],['new','canceled','rejected'])){ ?>
											<span>-</span>
										<?php }else{ ?>									
											<span class="text-sm"><?= $ticket['service']['node_ip']; ?></span>
										<?php } ?>
									<?php } ?>
								</div>
							
							</div>
							
							
							<!-- NODE USER -->
							<div class="row mb-2">
								<div class="col">
									<span for="example-text-input" class="text-xs form-control-label">OS User</span>
								</div>

								<div class="col col-sm px-0" id="ct-username">
									<?php if($ticket['service']['booking_status'] == 'new' && has_action($ticket,'inprogress')){ ?>
										<span id="show_node_users" style="display:none;cursor:pointer;position:absolute;margin-left: -20px;">
											<i class="fa fa-users text-xxs" title="Select from node user lists" alt="Select from node user lists"></i>
										</span>
										
										<input type="hidden" name="app_hpc_node_users[id]" value="<?= (isset($ticket['service']['os_user_id']) && !empty($ticket['service']['os_user_id'])) ? $ticket['service']['os_user_id'] : '' ?>">
										<input type="hidden" id="username" style="display:block !important;" value="<?= (isset($ticket['service']['os_user']) && !empty($ticket['service']['os_user'])) ? $ticket['service']['os_user'] : '' ?>" class="form-control form-control-sm" name="app_hpc_node_users[user]" onfocus="focused(this)" onfocusout="defocused(this)">
										<?php if($ticket['service']['os_id'] == 1 && $ticket['service']['booking_status'] == 'new'){ ?>
											<select id="os-user-list" name="<?= (isset($ticket['service_table']) && !empty($ticket['service_table']))?$ticket['service_table'].'[node_user_id]':'' ?>" class="form-control form-control-sm">
												<?php foreach($defaultNodes[$ticket['service']['node_ip']]['users'] as $userNode){ ?>
												<option data-ulist-uname='<?= $userNode['user'] ?>' data-ulist-pass='<?= $userNode['password'] ?>' value="<?= $userNode['id'] ?>" <?= (($ticket['service']['node_user_id'] == $userNode['id']) ? 'selected':'') ?>><?= $userNode['user'] ?></option>
												<?php } ?>
											</select>
										<?php }else{ ?>
											<span class="text-sm" id="lb-uname"><?= (isset($ticket['service']['os_user']) && !empty($ticket['service']['os_user'])) ? $ticket['service']['os_user'] : '' ?></span>
										<?php } ?>
									<?php } else{ ?>
										<?php if(in_array($ticket['service']['booking_status'],['new','canceled','rejected'])){ ?>
											<span>-</span>
										<?php }else{ ?>
											<span class="text-sm"><?= (isset($ticket['service']['os_user']) && !empty($ticket['service']['os_user'])) ? $ticket['service']['os_user'] : '' ?></span>
										<?php } ?>
									<?php } ?>
								</div>
							</div>	

							<!-- NODE PASS -->
							<div class="row mb-2">
								<div class="col">
									<span for="example-text-input" class="text-xs form-control-label">OS Password</span>

								</div>
								
								<div class="col col-sm px-0" id="ct-password">
									<?php if($ticket['service']['booking_status'] == 'new' && has_action($ticket,'inprogress')){ ?>
										<?php if(empty($ticket['service']['node_pass'])){ ?>
										<span id="reload_password" style="display:none;cursor:pointer;position:absolute;margin-left: -20px;">
											<i class="fa fa-refresh text-xxs" title="Reload Password" alt="Reload Password"></i>
										</span>								
										<?php } ?>
									<input type="text" id="password" style="display:block !important;" value="<?= (isset($ticket['service']['node_pass']) && !empty($ticket['service']['node_pass'])) ? $ticket['service']['node_pass'] : '' ?>" class="form-control form-control-sm" name="app_hpc_node_users[password]" onfocus="focused(this)" onfocusout="defocused(this)">
									<span class="text-sm" style="visibility:hidden;" id="lb-pass"><?= $ticket['service']['node_pass'] ?></span>
									
									<?php } else{ ?>
										<?php if(in_array($ticket['service']['booking_status'],['new','canceled','rejected'])){ ?>
											<span>-</span>
										<?php }else{ ?>
											<span class="text-sm"><?= $ticket['service']['node_pass'] ?></span>
										<?php } ?>
									<?php } ?>
								</div>
							</div>			
							
							
								<?php if($ticket['service']['booking_status'] == 'new' && has_action($ticket,'inprogress')){ ?>
								<div class="row">
									<input type="text" id="booking_status"  name="<?= (isset($ticket['service_table']) && !empty($ticket['service_table']))?$ticket['service_table'].'[booking_status]':'' ?>" value="<?= $ticket['service']['booking_status'] ?>">
									<!--<div class="col"><button type="button" id="cancel_booking" class="my-auto btn btn-xs btn-warning">Cancel</button></div>-->
									<div class="col"><button type="button" id="approve_booking" class="btn btn-xs btn-success">Approve</button></div>
									<div class="col"><button type="button" id="reject_booking" class="btn btn-xs btn-danger">Reject</button></div>
								</div>
								<?php }else{ ?>
									<?php $bgcol = ['new'=>'bg-info','canceled'=>'bg-warning','rejected'=>'bg-danger','approved'=>'bg-success']; ?>
									<?php if($ticket['service']['booking_status'] != 'new'){ ?>
									<div class="row mb-2">
										<div class="col">
											Status
										</div>
										<div class="col col-sm px-0" id="ct-password">
											<div class="my-auto badge badge-secondary <?= $bgcol[$ticket['service']['booking_status']] ?> text-white"><?= $ticket['service']['booking_status'] ?></div>
										</div>
									</div>
									<?php } ?>
								<?php } ?>
							
						  </div>
						  
						</li>
					</ul>
					</div>
					</div>
					<script>

					$(document).ready(function(){
						var currentBookingDate = <?= json_encode($flatpd) ?>;
						
						var bookedCal = null;
						var hasInvalidBookingDate = {
							dates:new Array(),
							status:false
						};
						
						function drawBookingCal(){
							// VALIDATE BOOKING DATE
							BASE_ROLE_URL = '<?= base_role_url() ?>';
							$.get(BASE_ROLE_URL+'/api/get-booked-dates',{
								package_id:$('input[name="package_id"]').val(),
								os_id:$('select[name="app_booking_hpc[os_id]"]').val()
							},function(response){
								disabledDate = response;
								bookedCal = flatpickr('#datepicker', {
								  mode: "multiple", //"range",
								  inline: true,
									//minDate: "today",
									position:"top",
									//disable: disabledDate,
									defaultDate:currentBookingDate,
									onDayCreate: function(dObj, dStr, fp, dayElem){
										
											var date = new Date(dayElem.dateObj);
											cdate = date.getFullYear()+'-'+("0" + (date.getMonth() + 1)).slice(-2)+'-'+("0" + date.getDate()).slice(-2);
											if (disabledDate.indexOf(cdate) != -1 && currentBookingDate.indexOf(cdate) != -1){
												dayElem.innerHTML += "<span class='event busy'></span>";
												if(hasInvalidBookingDate.dates.indexOf(cdate) == -1){
													hasInvalidBookingDate.dates.push(cdate);
												}
												hasInvalidBookingDate.status = true;
											}
									},							
									onReady: function(selectedDates, dateStr,instance){
										instance.days.parentElement.addEventListener('click', function (event) {
											event.stopPropagation();
										}, true);	
										$('.flatpickr-calendar').addClass('cal-ticket-detail');
										
										var selectedDatesStr = selectedDates.reduce(function(acc, ele) {
											var str = instance.formatDate(ele, "d/m/Y");
											acc = (acc == '') ? str : acc + ';' + str;
											return acc;
										}, '');				
										
										instance.set('disable', [function(date) 
										{
											var currDateStr = instance.formatDate(date, "Y-m-d");
											var x = currentBookingDate.indexOf(currDateStr);
											return x == -1;
											
										}]);								
										
									}
								});							
							});
						}
						
						var hpcOS = $('#hpc_os');
						var hpcOSNodes = $('#hpc_os_nodes');
						var osUserList = $('#os-user-list');
						var labelOSusername = $('#lb-uname');
						var labelOSpassword = $('#lb-pass');
						var osUsername = $('#username');
						var osPassword = $('#password');
						
						// Generate password if no user os found inside node.
						if(osPassword.val() == ''){
							genPassword();
							$('#reload_password').show();
						}
						//osUserList.hide();
						
						// OS CHANGED EVENTS.
						$('#hpc_os').on('change', function()
						{	
							hpcOSNodes.find('option').remove();
							if($(this).val() == 2){
								osUsername.val('<?= $ticket["owner"] ?>');
								if($('#lb-uname').length == 0){
									$('#ct-username').prepend('<span class="text-sm" id="lb-uname">'+osUsername.val()+'</span>');
								}
							}							
							$.get(BASE_URL+'/api/get-os-nodes/'+$(this).val(),function(response){
								option = '';
								if(response.length > 0){
									for(x in response)
									{
										option += '<option value="'+response[x].id+'">'+response[x].ip+'</option>';
									}
								}
								hpcOSNodes.append(option);
								hpcOSNodes.trigger('change');
							},'json');
						});
						$('#hpc_os').trigger('change');
						// OS NODES CHANGE EVENTS.
						$('#hpc_os_nodes').on('change', function()
						{	
							osUserList.find('option').remove();
							
							$('#reload_password').show();
							
							$.post(BASE_URL+'/api/get-users-node/'+$(this).val(),{
								'username':($('#hpc_os').val() == 2) ? $('#username').val() : ''
							},function(response)
							{
								if(response.length > 0)
								{
									if(hpcOS.val() == 1)
									{
										$('#lb-uname').hide();
										$('#reload_password').hide();
										
										htmlSelect = '<select id="os-user-list" name="app_booking_hpc[node_user_id]" class="form-control form-control-sm">';
										htmlSelect += '</select>';
										
										if($('#os-user-list').length == 0){
											$('#ct-username').append(htmlSelect);
											osUserList = $('#os-user-list');
										}
										
										osUserList.find('option').remove();
										//osUserList.append("<option></option>");
										for(n in response)
										{
											osUserList.append("<option value='"+response[n].id+"' data-ulist-uname='"+response[n].user+"' data-ulist-pass='"+response[n].password+"'>"+response[n].user+"</option>");
										}
										osUserList.show();
										osUserList.trigger('change');
										
										$('input[name="app_hpc_node_users[id]"]').val($('#os-user-list').val());
										
									}
									if(hpcOS.val() == 2){ // LINUX USER EXISTING
									
										$('#lb-uname').show();
										osUserList.remove();
										$('#reload_password').hide();
										osUsername.val(response[0].user);
										labelOSusername.text(osUsername.val());
										labelOSusername.show();
										osPassword.val(response[0].password);
										labelOSpassword.text(osPassword.val());	
										
										$('input[name="app_hpc_node_users[id]"]').val(response[0].id);
									}
									
									
									
									
								}else{
									
									// if(hpcOS.val() == 1)
									// {
										
										// labelOSusername.hide();
										// $('#reload_password').hide();
										
										// //osUserList.show();
										// //osUserList.trigger('change');
									// }				
									if(hpcOS.val() == 2){ // LINUX NO USER EXISTING
									
										osUserList.remove();
										$('#reload_password').show();
										$.post(BASE_URL+'/api/get-ticket-user-node',{
											'ticketId': TICKET_ID
										},function(response)
										{ 
											if(response.user == null){ // NO EXISTING USER NODE							
												osUsername.val('<?= $ticket["username"] ?>');
												if($('#lb-uname').length == 0){
													$('#ct-username').prepend('<span class="text-sm" id="lb-uname">'+osUsername.val()+'</span>');
												}
												$('#lb-uname').text(osUsername.val());
												$('#lb-uname').show();
												
												genPassword();
												labelOSpassword.text(osPassword.val());
												
												if($('#reload_password').length == 0){
													
													reload_password = '<span id="reload_password" style="cursor:pointer;position:absolute;margin-left: -20px;">';
													reload_password += '	<i class="fa fa-refresh text-xxs text-info" title="Reload Password" alt="Reload Password"></i>';
													reload_password += '</span>';
													$('#ct-password').prepend(reload_password);
												}
											}else{ // HAS EXISTING USER NODE
												
												$('#reload_password').hide();
											}
										});
									}					
								}
							},'json');
						});
						
						$(document).on('change', '#os-user-list', function()
						{
							osUsername.val($(this).find('option:selected').data('ulist-uname'));
							labelOSusername.text(osUsername.val());
							labelOSusername.hide();
							osPassword.val($(this).find('option:selected').data('ulist-pass'));
							labelOSpassword.text(osPassword.val());	
							$('input[name="app_hpc_node_users[id]"]').val($('#os-user-list').val());							
						});
						
						$(document).on('click', '#reload_password', function(){
							genPassword();
						});
						
						$('#service_approval').on('change', function(e){
						  approval = $(this).val();
						  percent = $('#percent_progress').val();									 
									 
							if(approval == 'approved'){
								Swal.fire({
								  title: 'Confirm Approve This Booking',
								  text: "Approve booking on this date range, are you sure ?",
								  icon: 'warning',
								  showCancelButton: true,
								  confirmButtonColor: '#3085d6',
								  cancelButtonColor: '#d33',
								  confirmButtonText: 'Yes, Approve!'
								}).then((result) => {
								  if (result.isConfirmed) {
									  if(approval == 'approved')
									  {
										$('#label_percent_progress').text('100%');
										slider.noUiSlider.set(100);
										percent = 100;
									  }else if(approval == 'canceled'){
										$('#service_approval').find('option[value="canceled"]').prop('selected', true);
										$('#label_percent_progress').text('0%');
										slider.noUiSlider.set(0);
										percent = 0;							  
									  }
								  }else{
									  $('#service_approval').find('option:eq(0)').prop('selected', true);
								  }
								});				
							}else if(approval == 'canceled'){
								$('#service_approval').find('option[value="canceled"]').prop('selected', true);
								$('#label_percent_progress').text('0%');
								slider.noUiSlider.set(0);
								percent = 0;					
							}else if(approval == 'rejected'){
								$('#label_percent_progress').text('0%');
								slider.noUiSlider.set(0);
								percent = 0;			
							}
						  
						  
							$('#percent_progress').val(percent);
						});	
						
						var elBookingStatus = $('#booking_status');
						$('#cancel_booking').on('click',function()
						{
							Swal.fire({
							  title: 'Confirm Cancel This Booking',
							  text: "Action can't be undo, Cancel this booking ?",
							  icon: 'warning',
							  showCancelButton: true,
							  confirmButtonColor: '#3085d6',
							  cancelButtonColor: '#d33',
							  confirmButtonText: 'Yes, Cancel it!'
							}).then((result) => {
							  if (result.isConfirmed){
								  elBookingStatus.val('canceled');
								  $('input[name="action"]').val('service_action_cancel');				  
								  
								  $( "#form-ticket").submit();			  
							  }
							});
						});
						
						<!-- APPROVE BOOKING -->
						$('#approve_booking').on('click',function()
						{
							// if(hasInvalidBookingDate.status === true){
								// alert(hasInvalidBookingDate.dates);
								// return false;
							// }
														
							BASE_ROLE_URL = '<?= base_role_url() ?>';
							$.get(BASE_ROLE_URL+'/api/get-booked-dates',{
								package_id:$('input[name="package_id"]').val(),
								os_id:$('select[name="app_booking_hpc[os_id]"]').val()
							},function(response){
								busyDate = false;
								disabledDate = response;
								// $('.flatpickr-day').each(function(i){
									// if($(this).hasClass('selected')){
										// date = new Date($(this).attr('aria-label'));
										// cdate = date.getFullYear()+'-'+("0" + (date.getMonth() + 1)).slice(-2)+'-'+("0" + date.getDate()).slice(-2);
										
										// if(disabledDate.indexOf(cdate) != -1){
											// $(this).find('span').remove();
											// $(this).append('<span class="event busy"></span>');
											// busyDate = true;
										// }
										
									// }
								// });
								$('.bookingdates').each(function(i){
									date = $(this).data('date');
									if(disabledDate.indexOf(date) != -1){
										$(this).removeClass('badge-success');
										$(this).addClass('badge-danger');
										busyDate = true;
									}										
								});

								if(busyDate === false){
									Swal.fire({
									  title: 'Confirm Approve This Booking',
									  text: "Action can't be undo, are you sure ?",
									  icon: 'warning',
									  showCancelButton: true,
									  confirmButtonColor: '#3085d6',
									  cancelButtonColor: '#d33',
									  confirmButtonText: 'Yes, Approve!'
									}).then((result) => {
									  if (result.isConfirmed){
										elBookingStatus.val('approved');	
										$('#label_percent_progress').text('100%');
										slider.noUiSlider.set(100);
										percent = 100;			  
										$('#percent_progress').val(percent);
										$('#save-progress').trigger('click');
									  }
									});									
								}else{
									Swal.fire({
									  title: 'Can\'t Approve Booking.',
									  text: "Some of date is busy.",
									  icon: 'warning',
									});									
								}
							});
															
						});
						$('#reject_booking').on('click',function()
						{
							Swal.fire({
							  title: 'Confirm Reject This Booking',
							  text: "Action can't be undo,Are you sure want to reject this booking ?",
							  icon: 'warning',
							  showCancelButton: true,
							  confirmButtonColor: '#3085d6',
							  cancelButtonColor: '#d33',
							  confirmButtonText: 'Yes, do it for me.'
							}).then((result) => {
							  if (result.isConfirmed){
								  elBookingStatus.val('rejected');
								  $('input[name="action"]').val('service_action_reject');
									$('#label_percent_progress').text('0%');
									slider.noUiSlider.set(0);
									percent = 0;				  
								  $('#percent_progress').val(percent);
								  $( "#form-ticket").submit();	
							  }
							});
						});							
						
					});
					</script>
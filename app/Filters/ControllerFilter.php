<?php

namespace App\Filters;

use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;

class ControllerFilter implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
	
    	if (!session()->isLoggedIn)
	    {
			return redirect()->to(base_url('/home'))->with('errors', "Invalid Credential");
		}		
		
		helper('Common_helpers');
		$session = session();
		$userRole = $session->get('role');
		$userRoleMenu = $session->get('role_path');
		$uri = current_url(true);
		$basename = $uri->getSegment($uri->getTotalSegments());
		$allowed = false;	
		foreach($userRoleMenu as $path){
			$pathroute = $path;
			$path = str_replace('(:any)','(.*)',$path);
			$path = str_replace('/','\\/',$path);
			if(preg_match('('.$path.')', $uri->getPath(), $matches))
			{
				if("/".$matches[0] == $uri->getPath())
				{
					$allowed = true;
					break;
				}
			}
		}

		//echo '<pre>';print_r(current_url(true)->getPath());exit();
		// if(array_key_exists($basename, $userRoleMenu)){
			// $allowed = true;
		// }	
		if (!$allowed) {
			$session->setFlashdata('back_to', previous_url());
			throw new \CodeIgniter\Router\Exceptions\RedirectException('403');
		}
    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        
    }
}

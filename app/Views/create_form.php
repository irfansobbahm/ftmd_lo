<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>

<div class="row">
	<div class="card col-12 col-lg-8 m-auto">
		<div class="card-body">		
		
		  <form enctype="multipart/form-data" class="form-inline needs-validation" method="POST" action="<?= base_role_url('forms/save-form') ?>">
			<div class="row">
				<div class="col-md-2">
					<div class="form-group mb-3 pb-1">
					<label class="form-label">Form Title</label>
					
					</div>
				</div>
				<div class="col-md-10">
					<div class="form-group mb-1 pb-1">
					<input required type="text" name="user_forms[title]" class="form-control form-control-sm">
					<input type="hidden" name="user_forms[user_id]" value="<?= $session->get('id') ?>">
					</div>
				</div>			
			</div>
			
			<div class="row">
				<div class="col-md-2">
					<div class="form-group mb-1 pb-1">
					<label class="form-label">Description</label>
					
					</div>
				</div>
				<div class="col-md-10">
					<div class="form-group mb-1 pb-1">
						<textarea class="form-control" name="user_forms[description]" id="exampleFormControlTextarea1" rows="3"></textarea>
					</div>
				</div>			
			</div>			
			
			
		<hr class="horizontal dark">
			<div class="row newrow">				
				<div class="col-md-2">
					<div class="form-group mb-1 pb-1">
					<label class="form-label">Field Type</label>
					</div>
				</div>
				<div class="col-md-4">
					<div class="input-group mb-1 pb-1">
						<select name="user_form_fields[type][]" class="form-control field-type form-control-sm" name="choices-button" id="choices-button" placeholder="Departure">
						  <option selected="">- Choose Field type - </option>
						  <?php foreach($types as $field){ ?>
						  <option value="<?= $field->type ?>"><?= $field->descr ?></option>
						  <?php } ?>
						</select>
						<button type="button" class="btn btn-outline-secondary btn-primary btn-xs add-new-field-type">+</button>
					</div>
				</div>
				<div class="row form-new">
		
				</div>
			</div>






		  <hr class="horizontal dark">
		  <button type="submit" class="btn btn-success btn-xsall">Save</button>
		  <button type="submit" class="btn btn-primary">Save & Publish</button>
		  </form>
		  
		</div>		
		
	</div>
</div>


<script>
$(document).ready(function()
{
	
	$('body').on('click','.add-new-field-type', function()
	{
		fieldType = '<hr class="horizontal dark">';
		fieldType += '			<div class="row newrow">';			
		fieldType += '				<div class="col-md-2">';
		fieldType += '					<div class="form-group mb-1 pb-1">';
		fieldType += '					<label class="form-label">Field Type</label>';
		fieldType += '					</div>';
		fieldType += '				</div>';
		fieldType += '				<div class="col-md-4">';
		fieldType += '					<div class="input-group">';
		fieldType += '					<select name="user_form_fields[type][]" class="form-control field-type form-control-sm" name="choices-button">';
		fieldType += '					  <option selected="">- Choose Field type - </option>';
							  <?php foreach($types as $field){ ?>
		fieldType += '					  <option value="<?= $field->type ?>"><?= $field->descr ?></option>';
							  <?php } ?>
		fieldType += '					</select>';
		fieldType += '					<button type="button" class="btn btn-outline-secondary btn-primary btn-xs add-new-field-type">+</button>';
		fieldType += '					<button type="button" class="btn btn-outline-secondary btn-secondary btn-xs remove-field-type">-</button>';
		fieldType += '					</div>';
		fieldType += '				</div>';				
		fieldType += '				<div class="row form-new">	';					
		fieldType += '				</div>';
		fieldType += '			</div>';		
		
		$(fieldType).insertAfter($(this).parent().parent().parent());

	});
	$('body').on('click','.remove-field-type', function()
	{
		$(this).parent().parent().parent().prevAll('hr:first').remove();
		$(this).parent().parent().parent().remove();
		//$(this).parent().parent().parent().prev().remove();
		//console.log();
	});		
	$('body').on('click','.add-row-radio', function()
	{
		alert($(this).parent().parent().html());
		ftype = $(this).data('type');
		html = '<div class="input-group mb-3">';
		html += '  <span class="input-group-text" id="basic-addon1"><input type="'+ftype+'" disabled></span>';
		html += '  <input type="text" class="form-control form-control-sm"  name="'+ftype+'[][]">';		
		html += '  <button class="btn btn-xs btn-danger btn-outline-secondary remove-row-radio" type="button">-</button>';			
		html += '</div>';		
		
		$(this).parent().parent().append(html);
		
	});
	function getLenFieldType(ftype){
		n = 0;
		$('.field-type').each(function(i)
		{
			
			if($(this).val() == ftype){
				n++;
			}
		});
	}
	$('body').on('click','.remove-row-radio', function()
	{	
		
		$(this).parent().remove();
	});	
	$('body').on('change','.field-type',function()
	{
		label = 'Pertanyaan ';
		fieldType = '';		
		pertanyaan = '<div class="input-group"><span class="input-group-text num"></span>';
		pertanyaan += '<input type="text" name="user_form_fields[descr][]" required class="form-control col-mb-2 form-control-sm" placeholder="'+label+'"></div>';
		ftype = $(this).val();
		
		

		switch(ftype){
			case 'text':
				html = fieldType+'<div class="form-group mb-1 pb-1">';
				html += 	pertanyaan;
				html += 	'<input disabled type="text" class="form-control mt-3" placeholder="Input Jawaban">';
				html += '</div>';	
			break;
			case 'textarea':
				html = fieldType+'<div class="form-group mb-1 pb-1">';
				html += 	pertanyaan;
				html += '	<textarea disabled class="form-control mt-3" placeholder="Input jawaban : Paragraf"></textarea>';
				html += '</div>';
			break;
			case 'date':
				html = fieldType+'<div class="form-group mb-1 pb-1">';
				html += 	pertanyaan;
				html += 	'<input disabled type="date" class="form-control mt-3" placeholder="Input jawaban : dd/mm/yyyy">';
				html += '</div>';			
			break;
			case 'email':
				html = fieldType+'<div class="form-group mb-1 pb-1">';
				html += 	pertanyaan;
				html += 	'<input disabled type="email" class="form-control mt-3" placeholder="Input jawaban : saya@domain.com">';
				html += '</div>';			
			break;
			case 'checkbox':
				html = fieldType+'<div class="form-group mb-1 pb-1">';
				html += 	pertanyaan;
				//html += 	'<input disabled type="email" class="form-control mt-3" placeholder="Input jawaban : saya@domain.com">';
				html += '</div>';			
				html += '<div class="input-group mb-3">';
				html += '  <span class="input-group-text" id="basic-addon1"><input type="'+ftype+'" disabled></span>';
				html += '  <input type="text" class="form-control form-control-sm" name="user_form_fields[sub]['+ftype+($('.num').length)+'][]">';
				html += '  <button class="btn btn-xs btn-secondary btn-outline-secondary add-row-radio" data-type="'+ftype+'" type="button">+</button>';	
				//html += '  <button class="btn btn-xs btn-danger btn-outline-secondary add-row-radio" type="button">-</button>';
				html += '</div>';							
			break;
			case 'radio':
				html = fieldType+'<div class="form-group mb-1 pb-1">';
				html += 	pertanyaan;
				html += '</div>';			
				html += '<div class="input-group mb-3">';
				html += '  <span class="input-group-text" id="basic-addon1"><input type="'+ftype+'" disabled></span>';
				html += '  <input type="text" class="form-control form-control-sm" name="user_form_fields[sub]['+ftype+($('.num').length)+'][]">';
				html += '  <button class="btn btn-xs btn-secondary btn-outline-secondary add-row-radio" data-type="'+ftype+'" type="button">+</button>';
				//html += '  <button class="btn btn-xs btn-danger btn-outline-secondary add-row-radio" type="button">-</button>';				
				html += '</div>';
			break;		
			case 'signature':
				html = fieldType+'<div class="form-group mb-1 pb-1">';
				html += 	pertanyaan;
				html += '</div>';			
				html += '<div class="input-group mb-3">';
					html += '<div class="sigPad">';
					//html += '<p class="drawItDesc">Draw your signature</p>';
					html += '<ul class="sigNav">';
					html += '  <li class="drawIt"><a href="#draw-it" >Draw</a></li>';
					html += '  <li class="clearButton"><a href="#clear">Clear</a></li>';
					html += '</ul>';
					html += '<div class="sig sigWrapper">';
					html += '  <div class="typed"></div>';
					html += '  <canvas class="pad"></canvas>';
					html += ' <input type="hidden" name="output" class="output">';
					html += '</div>';
					html += '</div>';				
				html += '</div>';
				

				
			break;
			case 'phone_num':
				html = fieldType+'<div class="form-group mb-1 pb-1">';
				html += 	pertanyaan;
				html +=		'<div class="row">';
				html +=		'<div class="col-2">';
				html += 	'<select readonly disabled class="form-control mt-3">';
				html +=		'<option selected>+62</option>';
				html += 	'</select>';
				html +=		'</div>';
				html +=		'<div class="col-sm"><input disabled type="text" class="form-control col-md-6 mt-3" placeholder="Input Jawaban"></div>';
				html += 	'</div>';
				html += '</div>';			
			break;			
		}
		
		$(this).parent().parent().parent().find('.form-new').html(html);
		
		$('.sigPad').signaturePad({drawOnly :true});
		
		$('.num').each(function(i)
		{
			n = (i+1);
			numlabel = ''+n+'.';
			$(this).html(numlabel);
		});
	});
	
	$('#preview-button').on('click',function() {
		alert('test')
		$('form').attr("action", "<?= base_role_url('forms/preview-form') ?>");  //change the form action
		$('form').submit();  // submit the form
	});
	
	
});

</script>
<script src="<?= base_url('assets/signature-pad-main/json2.js'); ?>"></script>
<script src="<?= base_url('assets/signature-pad-main/jquery.signaturepad.js'); ?>"></script>

<script>
$(document).ready(function() {
  
});
</script>
<?= $this->endSection() ?>
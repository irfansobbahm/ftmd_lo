<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>

<div class="row">
	<div class="card mb-4">
		<div class="card-body">
			<a class="btn btn-primary btn-sm mb-0 me-2" href="<?= base_role_url('hpc-software/new-software') ?>"><i class="fa fa-plus"></i> New Software</a>
		</div>		
	</div>
		<div class="card mb-4">
	
			<div class="card-body px-0 pb-2">
			<div class="table-responsive p-0 mt-2">
				<h4 class="text-uppercase text-md">Linux</h4>
				<table class="table align-items-center mb-0">
				  <thead>
					<tr>
					  <th class="text-uppercase text-secondary text-sm font-weight-bolder ">Software name</th>	
					  <th class="text-uppercase text-secondary text-sm font-weight-bolder ">&nbsp;</th>	
					</tr>
				  </thead>
				  <tbody>
				  <?php foreach($data['linux'] as $row){ ?>
					<tr>									
					  <td class="align-middle text-sm">
						  <div class="d-flex flex-column justify-content-center">
							<h6 class="mx-3 mb-0 text-sm  text-primary"><?= $row['software'] ?></h6>
						  </div>	
					  </td>					
					  <td class="align-middle text-sm">
						<a title="Edit Software" data-name="<?= $row['software']; ?>" data-id="<?= $row['id']; ?>" alt="Edit Software" class="btn btn-success btn-xs mb-0 me-2" href="<?= base_role_url('hpc-software/edit-software/'.$row['id']) ?>"><i class="fa fa-pencil"></i></a>
						<?php if($row['id'] != 3){ ?>
						  
						  <a title="Delete Software" data-name="<?= $row['software']; ?>" data-id="<?= $row['id']; ?>" alt="Delete Software" class="btn del-software btn-danger btn-xs mb-0 me-2" href="javascript:void(0);"><i class="fa fa-trash"></i></a>	
						  
						<?php } ?>
					  </td>						
					</tr>
				  <?php } ?>				  
				  </tbody>
				</table>
			  </div>
			  <!--
			  <div class="row ps-3 pt-3">
				<ul class="pagination pagination-info">
				<li class="page-item">
				  <a class="page-link" href="#link" aria-label="Previous">
					<span aria-hidden="true"><i class="ni ni-bold-left" aria-hidden="true"></i></span>
				  </a>
				</li>
				<li class="page-item active">
				  <a class="page-link " href="#link">1</a>
				</li>
				<li class="page-item">
				  <a class="page-link" href="#link" aria-label="Next">
					<span aria-hidden="true"><i class="ni ni-bold-right" aria-hidden="true"></i></span>
				  </a>
				</li>
				</ul>	
				</div>
				-->
				
				
				
			</div>		
			
		</div>
		<div class="card mb-4">
			<div class="card-body px-0 pb-2">
				<!-- WINDOWS -->
				<div class="table-responsive p-0 mt-2">
				<h4 class="text-uppercase text-md">Windows</h4>
				<table class="table align-items-center mb-0">
				  <thead>
					<tr>
					  <th class="text-uppercase text-secondary text-sm font-weight-bolder ">Software name</th>	
					  <th class="text-uppercase text-secondary text-sm font-weight-bolder ">&nbsp;</th>						  
					</tr>
				  </thead>
				  <tbody>
				  <?php foreach($data['windows'] as $row){ ?>
					<tr>									
					  <td class="align-middle text-sm">
						  <div class="d-flex flex-column justify-content-center">
							<h6 class="mx-3 mb-0 text-sm  text-primary"><?= $row['software'] ?></h6>
						  </div>	
					  </td>					
					  <td class="align-middle text-sm">
						<a title="Edit Software" data-name="<?= $row['software']; ?>" data-id="<?= $row['id']; ?>" alt="Edit Software" class="btn btn-success btn-xs mb-0 me-2" href="<?= base_role_url('hpc-software/edit-software/'.$row['id']) ?>"><i class="fa fa-pencil"></i></a>
						<?php if($row['id'] != 3){ ?>
						  
						  <a title="Delete Software" data-name="<?= $row['software']; ?>" data-id="<?= $row['id']; ?>" alt="Delete Software" class="btn del-software btn-danger btn-xs mb-0 me-2" href="javascript:void(0);"><i class="fa fa-trash"></i></a>	
						  
						<?php } ?>
					  </td>						
					</tr>
				  <?php } ?>				  
				  </tbody>
				</table>
			  </div>
			  <!--<div class="row ps-3 pt-3">
				<ul class="pagination pagination-info">
				<li class="page-item">
				  <a class="page-link" href="#link" aria-label="Previous">
					<span aria-hidden="true"><i class="ni ni-bold-left" aria-hidden="true"></i></span>
				  </a>
				</li>
				<li class="page-item active">
				  <a class="page-link " href="#link">1</a>
				</li>
				</li>
				<li class="page-item">
				  <a class="page-link" href="#link" aria-label="Next">
					<span aria-hidden="true"><i class="ni ni-bold-right" aria-hidden="true"></i></span>
				  </a>
				</li>
				</ul>				
				</div>	-->	
		</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
	$('.del-software').on('click', function()
	{
		thisBtn = $(this);
		Swal.fire({
		  title: 'Confirm Delete Software Name "'+thisBtn.data('name')+'".',
		  text: 'are you sure ?',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, Delete it!'
		}).then((result) => {
		  if (result.isConfirmed) 
		  {
			  $.post(ROLE_BASE_URL+'/hpc-software/delete-software',{
				  'id': thisBtn.data('id'),
				  'name' : thisBtn.data('name')
			  },function(response)
			  {
				  document.location.reload();
			  });
		  }
		});			
	});
});
</script>
<?= $this->endSection() ?>
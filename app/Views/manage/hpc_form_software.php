
<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>

	<div class="card p-3">

		<div class="card-body">
			<form method="POST" action="<?= base_role_url('hpc-software/save-software'); ?>">
				<label>Software name</label>
				<input type="hidden" name="app_software_hpc[id]" value="<?= (isset($data['id']) && !empty($data['id'])) ? $data['id']:'' ?>">
				<input class="form-control" type="text" name="app_software_hpc[software]" value="<?= (isset($data['software'])) ? $data['software']:'' ?>" onfocus="focused(this)" onfocusout="defocused(this)">
				<label class="mt-4">Operating System</label>
				<select required class="form-control" name="app_software_hpc[os_id]" onfocus="focused(this)" onfocusout="defocused(this)">
					<option></option>
					<?php foreach($osList as $os){ ?>
					<option <?= (isset($data['os_id']) && !empty($data['os_id']) && $data['os_id'] == $os['id']) ? 'selected':'' ?> value="<?= $os['id'] ?>" <?= $os['id'] == old('app_booking_hpc.os_id') ? 'selected':'' ?> data-os-softwares='<?= json_encode($os['os']) ?>'><?= $os['os'] ?></option>
					<?php } ?>
				</select>
				<div class="col-12 mt-4">
					<div class="d-flex">
						<button id="save" class="btn btn-primary btn-sm mb-0 me-2" type="submit" name="button">Save</button>
					</div>
				</div>					
			</form>
		</div>	
		
	</div>

<?= $this->endSection() ?>
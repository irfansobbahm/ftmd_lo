<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>

		<div class="card p-3">
	
			<div class="table-responsive">
				<table class="table table-hover align-items-center mb-0">
				  <thead>
					<tr>
					  <th class="w-2 text-uppercase text-secondary text-sm font-weight-bolder">#</th>
					  <th class="text-uppercase text-secondary text-sm font-weight-bolder ">Page name</th>
					  <th class="text-uppercase text-secondary text-sm font-weight-bolder ">&nbsp;</th>
					</tr>
				  </thead>
				  <tbody>
				  <?php $n=1;foreach($data as $i => $row){ ?>
					<tr>
						<td><?= ($n) ?></td>
						<td class="font-weight-bolder text-sm">
							<div class="d-flex px-2 py-1">
							  <div class="d-flex flex-column justify-content-center">
								<h6 class="mb-0 text-sm"><?= $row->label ?></h6>							
							  </div>
							</div>						
						</td>
						<td>
							<button data-pageid="<?= $row->id ?>" class="btn btn-primary btn-xs">Set Guide</span>
						</td>
					</tr>
					<?php if(!empty($row->submenu)){ ?>
						<?php foreach($row->submenu as $sub){ $n++;?>
							<tr>
								<td><?= ($n) ?></td>
								<td>
									<div class="d-flex px-2 py-1">
									  <div class="d-flex flex-column justify-content-center">
										<h6 class="px-3 mb-0 text-sm"><?= $sub->label ?></h6>							
									  </div>
									</div>								
								</td>
								<td class="justify-content-center">
									<button data-pageid="<?= $sub->id ?>" class="btn btn-primary btn-xs">Set Guide</span>
								</td>
							</tr>
						<?php } ?>
					<?php } ?>				
				  <?php $n++;} ?>
				  </tbody>
				</table>
			  </div>		
			
		</div>

<script>

$(document).ready(function()
{
	var selected;
	$('button').on('click',function()
	{
		_html ="<textarea class='abc'></textarea>";
		_html += '<button class="btn btn-sm save-guide btn-success mt-4">Save</button>';
		btn = $(this);
		Swal.fire({
		  title: 'Page Guide',
		  html: _html,
		  customClass: 'swal-wide',
		  showCancelButton: false,
		  showConfirmButton: false
		});	
		tinymce.init({
		  selector: '.abc',
		  height: 500,
		  plugins: [
			'image',
		  ],
		  toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
		  content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
		});	
		
		$.get(ROLE_BASE_URL+'/page-guide-management/get-page-guide/'+btn.data('pageid'),function(payload)
		{
			$('.abc').text(payload.content);
			$('.save-guide').data('id',payload.id);
		},'json');
		
		$(document).on('click','.save-guide',function()
		{
			$.post(ROLE_BASE_URL+'/page-guide-management/save-page-guide',{
				page_guide:{
					id:($('.save-guide').data('id') == undefined)? '' : $('.save-guide').data('id'),
					page_id:btn.data('pageid'),
					content:tinyMCE.activeEditor.getContent()
				}
			},function(payload)
			{
				location.reload();
			});
		});
		
	});	
});

</script>
<?= $this->endSection() ?>
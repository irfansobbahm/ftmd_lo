<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>

<?= $this->include('ticket_views/filter_nostatus') ?>
	
<div class="row">

		<div class="card mb-4">
			<!--<div class="card-header pb-0">
			  <div class="d-lg-flex">
				<div>
				  <h5 class="mb-0">All Products</h5>
				  <p class="text-sm mb-0">
					A lightweight, extendable, dependency-free javascript HTML table plugin.
				  </p>
				</div>
				<div class="ms-auto my-auto mt-lg-0 mt-4">
				  <div class="ms-auto my-auto">
					<a href="./new-product.html" class="btn bg-gradient-primary btn-sm mb-0" target="_blank">+&nbsp; New Product</a>
					<button type="button" class="btn btn-outline-primary btn-sm mb-0" data-bs-toggle="modal" data-bs-target="#import">
					  Import
					</button>
					<div class="modal fade" id="import" tabindex="-1" aria-hidden="true">
					  <div class="modal-dialog mt-lg-10">
						<div class="modal-content">
						  <div class="modal-header">
							<h5 class="modal-title" id="ModalLabel">Import CSV</h5>
							<i class="fas fa-upload ms-3" aria-hidden="true"></i>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						  </div>
						  <div class="modal-body">
							<p>You can browse your computer for a file.</p>
							<input type="text" placeholder="Browse file..." class="form-control mb-3" onfocus="focused(this)" onfocusout="defocused(this)">
							<div class="form-check">
							  <input class="form-check-input" type="checkbox" value="" id="importCheck" checked="">
							  <label class="custom-control-label" for="importCheck">I accept the terms and conditions</label>
							</div>
						  </div>
						  <div class="modal-footer">
							<button type="button" class="btn bg-gradient-secondary btn-sm" data-bs-dismiss="modal">Close</button>
							<button type="button" class="btn bg-gradient-primary btn-sm">Upload</button>
						  </div>
						</div>
					  </div>
					</div>
					<button class="btn btn-outline-primary btn-sm export mb-0 mt-sm-0 mt-1" data-type="csv" type="button" name="button">Export</button>
				  </div>
				</div>
			  </div>
			</div>	-->	

			<div class="card-body px-0 pb-2 pt-0">
			<div class="table-responsive p-0 mt-2">
				<table class="table align-items-center mb-0">
				  <thead>				  
					<tr>
					  <th class="text-uppercase text-secondary text-sm font-weight-bolder ">Ticket #</th>
					  <th class="text-uppercase text-secondary text-sm font-weight-bolder ">Service Name</th>
					  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder">Status</th>
					  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder">Progress</th>
					  <th class="text-uppercase text-secondary text-sm font-weight-bolder ps-2">Agent In Charge</th>					  
					  <!--<th class="text-uppercase text-secondary text-sm font-weight-bolder ">Ticket Owner</th>-->	
					  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder">Ticket Last Update</th>
					  <th  class="text-secondary opacity-7"></th>
					</tr>
				  </thead>
				  <tbody>
				  <?php foreach($data as $ticket){ ?>
					<tr>
					  <td>
						  <div class="d-flex flex-column justify-content-center">
							<h6 class="mb-0 text-sm"><a  style="color:#005aab;" href="<?= $ticket['identifier'].'/view-detail' ?>">#<?= $ticket['identifier'] ?></a></h6>
							<p class="text-xs text-secondary mb-0"><i class="fa fa-clock me-1" aria-hidden="true"></i> <?= date_diff_words($ticket['created_at'], date('Y-m-d H:i:s')); ?></p>
						  </div>					
					  </td>										
					  <td>
						  <div class="d-flex flex-column justify-content-center ps-3">
							<h6 class="mb-0 text-sm"><?= $ticket['service_title'] ?></h6>
							<span class="text-xs text-secondary mb-0">
								<?php
									if($ticket['service_slug'] == 'hpc-booking'){
										$serviceData = model($ticket['service_model'])->getTicketData($ticket['identifier']); 
								?>
								<span class="badge badge-floating badge-sm bg-success <?= ($serviceData['booking_status'] == 'approved')?'':'d-none' ?>"><i class="fa fa-check"></i> <?= $serviceData['booking_status'] ?></span>
								<span class="badge badge-sm bg-danger <?= ($serviceData['booking_status'] == 'rejected')?'':'d-none' ?>"><i class="fa fa-times"></i> <?= $serviceData['booking_status'] ?></span>
								<span class="badge bg-warning <?= ($serviceData['booking_status'] == 'canceled')?'':'d-none' ?>"><?= $serviceData['booking_status'] ?></span>
								<span class="badge bg-info <?= ($serviceData['booking_status'] == 'new')?'':'d-none' ?>"><?= $serviceData['booking_status'] ?></span>						
								
								<?php } ?>							
							</span>
						  </div>					
					  </td>
					  <td class="align-middle text-sm">
						<span class="badge badge-sm bg-gradient-danger <?= ($ticket['status'] == 'open')?'':'d-none' ?>"><?= $ticket['status'] ?></span>
						<span class="badge badge-sm bg-gradient-success <?= ($ticket['status'] == 'inprogress')?'':'d-none' ?>"><?= $ticket['status'] ?></span>
						<span class="badge bg-gradient-warning <?= ($ticket['status'] == 'pending')?'':'d-none' ?>"><?= $ticket['status'] ?></span>
						<span class="badge badge-sm bg-gradient-secondary <?= ($ticket['status'] == 'closed')?'':'d-none' ?>"><?= $ticket['status'] ?></span>
					  </td>					  
					  <td class="align-middle text-center">
						<div class="d-flex align-items-center justify-content-center">
						  <span class="me-2 text-xs font-weight-bold"><?= $ticket['progress']; ?>%</span>
						  <div>
							<div class="progress">
							  <div class="progress-bar bg-gradient-info" role="progressbar" aria-valuenow="<?= $ticket['progress']; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $ticket['progress']; ?>%;"></div>
							</div>
						  </div>
						</div>				  
					  </td>
					  <td class="align-middle text-sm">
						<?php if(empty($ticket['agent_name'])){ ?>
							<span class="badge badge-warning text-xxs text-warning">Waiting for response</span>
						<?php }else{ ?>
							<p class="font-weight-bold text-sm mb-0"><?= $ticket['agent_name'] ?></p>
							<p class="text-xs text-secondary mb-0"><?= $ticket['agent_email'] ?></p>
						<?php } ?>
					  </td>						  
					  <!--<td>
						<div class="d-flex px-2 py-1">-->
						  <!--<div>
							<img src="../assets/img/team-2.jpg" class="avatar avatar-sm me-3" alt="user1">
						  </div>-->
						  <!--<div class="d-flex flex-column justify-content-center">
							<h6 class="mb-0 text-sm"><?= $ticket['name'] ?></h6>
							<p class="text-xs text-secondary mb-0"><?= $ticket['email'] ?></p>
						  </div>
						</div>
					  </td>	-->			
					  <td class="align-middle px-3">
						<span class="text-secondary text-xs font-weight-normal ">
						<i class="fa fa-clock me-1" aria-hidden="true"></i> <?= date_diff_words($ticket['updated_at'], date('Y-m-d H:i:s')); ?>
						</span>
					  </td>					  
					  <td class="align-middle">
					  
						<!-- ACTION COLUMN -->
						<?php if(isset($state) && $state == 'action'){ ?>
							<?php if(in_array(session()->get('role'),['agent','manager'])){ ?>
								<?php if($ticket['status'] == 'open'){ ?>
								<a href="<?= base_url(session()->get('role').'/ticket/'.$ticket['identifier'].'/lock-ticket') ?>" class="btn bg-gradient-warning btn-xs" type="button" title="Lock and assign to me.">
									<span class="btn-inner--icon"><i class="fa fa-lock-open"></i></span>
								</a>					  
								<?php } ?>
								<?php if($ticket['status'] == 'inprogress'){ ?>
									<?php if($ticket['agent'] != session()->get('username') && session()->get('role') == 'agent'){ ?>
										<button type="button" class="btn btn-secondary btn-xs btn-tooltip" data-bs-toggle="tooltip" data-bs-placement="top" title="Currently locked by <?= $ticket['agent'] ?>" data-container="body" data-animation="true" disabled>
											<span class="btn-inner--icon"><i class="fa fa-lock"></i></span>
										</button>
									<?php } ?>				  
								<?php } ?>							
							<?php } ?>	
							<?php if(($ticket['status'] == 'inprogress' && $ticket['agent'] == session()->get('username')) || ($ticket['status'] == 'inprogress' && session()->get('role') == 'manager')){ ?>
								<a href="<?= base_url(session()->get('role').'/ticket/'.$ticket['identifier'].'/unlock-ticket') ?>" class="btn bg-gradient-danger btn-xs" type="button" title="Unlock Ticket And Back To Status Open.">
									<span class="btn-inner--icon"><i class="fa fa-lock"></i></span>
								</a>					  
							<?php } ?>
						<?php } ?>						
							
						<a href="<?= $ticket['identifier'].'/view-detail' ?>" class="btn bg-gradient-primary btn-xs" data-toggle="tooltip" data-original-title="View Detail" title="View Detail">
						  <span class="btn-inner--icon"><i class="fa fa-info"></i></span>
						</a>
						
					  </td>
					
					
					</tr>
				  <?php } ?>
				  </tbody>
				</table>
			  </div>
			  
			</div>		
			
		</div>
	</div>
</div>


<?= $this->endSection() ?>
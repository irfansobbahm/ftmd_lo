<div class="row">
	<div class="card mb-2">
		<div class="card-body p-3">
			<form method="GET">
				<div class="row">
					<div class="col-2">
						<label class="text-sm">Ticket ID</label>
						<div class="input-group input-group-sm">			
						  <input name="identifier" type="text" placeholder=" Ticket #" class="form-control" value="<?= _get_('identifier'); ?>">
						</div>
					</div>	
					<div class="col-2">
						<label class="text-sm">Service</label>
						<div class="input-group input-group-sm">			
						  <select name="services" class="form-control">
							<option value=""> All Service</option>
							<option <?= _get_('services') == '876ee1cc-7d82-4d8e-a9de-70a7df148c84' ? 'selected':''; ?> value="876ee1cc-7d82-4d8e-a9de-70a7df148c84"> HPC Booking</option>
							<option <?= _get_('services') == '4e3076c4-9b86-4726-a9d2-bbee815ee6f6' ? 'selected':''; ?> value="4e3076c4-9b86-4726-a9d2-bbee815ee6f6"> Software Installation</option>
							<option <?= _get_('services') == 'c5c140d1-b2ad-4888-bcb7-7a6a525a4e1b' ? 'selected':''; ?> value="c5c140d1-b2ad-4888-bcb7-7a6a525a4e1b"> Troubleshoot</option>
						  </select>
						</div>
					</div>
					<div class="col-2">		
						<label class="text-sm">Ticket Status</label>
						<div class="input-group input-group-sm">			
						  <select name="status" class="form-control">
							<option value=""> All Ticket Status</option>
							<option <?= _get_('status') == 'open' ? 'selected':''; ?> value="open"> Open</option>
							<option <?= _get_('status') == 'inprogress' ? 'selected':''; ?> value="inprogress"> In Progress</option>
							<option <?= _get_('status') == 'closed' ? 'selected':''; ?> value="closed"> Closed</option>
						  </select>
						</div>
					</div>
					<div class="col-2">
						<label class="text-sm">Agent Name(PIC)</label>
						<div class="input-group input-group-sm">			
						  <input name="agent" type="text" placeholder=" Agent In Charge" class="form-control" value="<?= _get_('agent'); ?>">
						</div>
					</div>
					<div class="col-2">
						<label class="text-sm">Ticket Owner Name</label>
						<div class="input-group input-group-sm">			
						  <input name="owner" type="text" placeholder=" Ticket Owner" class="form-control" value="<?= _get_('owner'); ?>">
						</div>
					</div>
					<div class="col-2">
						<label class="text-sm">&nbsp;</label>
						<div class="input-group input-group-sm">			
						  <input type="submit" class="form-control btn btn-primary" value="Apply">
						  <input type="reset" onclick="document.location.href='<?= current_url(); ?>';" class="form-control btn btn-secondary" value="Reset">
						</div>
					</div>					
				</div>	
			</form>
		</div>		
	</div>
</div>
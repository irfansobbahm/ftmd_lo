<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>

<?php if(role("user")){ ?>
	<?= $this->include('layout/user_dashboard') ?>
<?php } ?>

<?php if(role("agent") || role("manager")){ ?>
	<?= $this->include('layout/manajemen_dashboard') ?>
<?php } ?>

<?= $this->endSection() ?>
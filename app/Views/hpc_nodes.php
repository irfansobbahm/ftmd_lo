<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>

<div class="row">
	
		<div class="card mb-4">
			<div class="card-body px-0 pb-2">
			<div class="table-responsive p-0 mt-2">
				<h4 class="text-uppercase text-md">Linux</h4>
				<table class="table align-items-center mb-0">
				  <thead>
					  <th class=" text-left text-uppercase text-secondary text-sm font-weight-bolder ">Node IP Address</th>
					  <th class="text-left text-uppercase text-secondary text-sm font-weight-bolder">Core</th>
					  <th class="text-left  text-uppercase text-secondary text-sm font-weight-bolder">RAM(GB)</th>
					  <th class=" text-left text-uppercase text-secondary text-sm font-weight-bolder ps-2">Storage(GB)</th>
					  <th class=" text-left text-uppercase text-secondary text-sm font-weight-bolder ps-2">&nbsp;</th>					  
					</tr>
				  </thead>
				  <tbody>
				  <?php $clinux = 0; $rlinux = 0; $slinux = 0; ?>
				  <?php foreach($linux['data'] as $row){ ?>
					<tr>									
					  <td>
						  <div class="d-flex flex-column justify-content-center">
							<h6 class="mb-0 text-sm"><?= $row['ip'] ?></h6>
							<input type="hidden" value="<?= $row['ip'] ?>">
						  </div>						
					  </td>
					  <td class="align-middle text-sm">
						  <div class="d-flex flex-column justify-content-center text-center">
							<h6 class="mb-0 text-sm"><?php $clinux = $clinux + $row['cores'] ?>
								<input type="text" class="form-control w-50" value="<?= $row['cores'] ?>">
							</h6>
						  </div>	
					  </td>					  
					  <td class="align-middle text-center">
						  <div class="d-flex flex-column justify-content-center text-center">
							<h6 class="mb-0 text-sm"><?php  $rlinux = $rlinux + $row['ram'] ?>
							<input type="text" class="form-control w-50" value="<?= $row['ram'] ?>"></h6>
						  </div>					  
					  </td>
					  <td class="align-middle text-sm">
						  <div class="d-flex flex-column justify-content-center">
							<span class="mb-0 text-sm"><?php  $slinux = $slinux + $row['storage'] ?>
							<input type="text" class="form-control w-25" value="<?= $row['storage'] ?>"> </span><span></span>
						  </div>	
					  </td>						  
					  <td>
						<button class="save btn btn-sm btn-success">Save</button>
					  </td>
					</tr>
				  <?php } ?>
					<tr class="">									
					  <td>
						  <div class="d-flex flex-column justify-content-center">
							<h6 class="mb-0 text-lg text-primary">Total Resources</h6>
						  </div>						
					  </td>
					  <td class="align-middle text-left text-sm">
						  <div class="d-flex flex-column justify-content-center">
							<h6 class="mb-0 text-lg text-left  text-primary"><?= $clinux ?></h6>
						  </div>	
					  </td>					  
					  <td class="align-middle text-left">
						  <div class="d-flex flex-column justify-content-center">
							<h6 class="mb-0 text-lg  text-primary"><?= $rlinux ?> GB</h6>
						  </div>					  
					  </td>
					  <td class="align-middle text-left text-sm">
						  <div class="d-flex flex-column justify-content-center">
							<h6 class="mb-0 text-lg  text-primary"><?= $slinux ?> GB</h6>
						  </div>	
					  </td>						  
					  
					
					
					</tr>				  
				  </tbody>
				</table>
			  </div>
			  <!--
			  <div class="row ps-3 pt-3">
				<ul class="pagination pagination-info">
				<li class="page-item">
				  <a class="page-link" href="#link" aria-label="Previous">
					<span aria-hidden="true"><i class="ni ni-bold-left" aria-hidden="true"></i></span>
				  </a>
				</li>
				<li class="page-item active">
				  <a class="page-link " href="#link">1</a>
				</li>
				<li class="page-item">
				  <a class="page-link" href="#link" aria-label="Next">
					<span aria-hidden="true"><i class="ni ni-bold-right" aria-hidden="true"></i></span>
				  </a>
				</li>
				</ul>	
				</div>
				-->
				
				
				
			</div>		
			
		</div>
		<div class="card mb-4">
			<div class="card-body px-0 pb-2">
				<!-- WINDOWS -->
				<div class="table-responsive p-0 mt-2">
				<h4 class="text-uppercase text-md">Windows</h4>
				<table class="table align-items-center mb-0">
				  <thead>
					  <th class="text-uppercase text-secondary text-sm font-weight-bolder ">Node IP Address</th>
					  <th class="text-left text-uppercase text-secondary text-sm font-weight-bolder">Core</th>
					  <th class="text-left text-uppercase text-secondary text-sm font-weight-bolder">RAM</th>
					  <th class="text-uppercase text-secondary text-sm font-weight-bolder ps-2">Storage</th>
					  <th class=" text-left text-uppercase text-secondary text-sm font-weight-bolder ps-2">&nbsp;</th>					  
					</tr>
				  </thead>
				  <tbody>
				  <?php $cwin = 0; $rwin = 0; $swin = 0; ?>
				  <?php foreach($windows['data'] as $row){ ?>
					<tr>									
					  <td>
						  <div class="d-flex flex-column justify-content-center">
							<h6 class="mb-0 text-sm"><?= $row['ip'] ?></h6>
							<input type="hidden" value="<?= $row['ip'] ?>">
						  </div>						
					  </td>
					  <td class="align-middle text-center">
						  <div class="d-flex flex-column justify-content-center text-center">
							<h6 class="mb-0 text-sm"><?php $cwin = $cwin + $row['cores'] ?>
							<input type="text" class="form-control w-25" value="<?= $row['cores'] ?>"></h6>
						  </div>	
					  </td>					  
					  <td class="align-middle text-center">
						  <div class="d-flex flex-column justify-content-center">
							<h6 class="mb-0 text-sm"><?php  $rwin = $rwin + $row['ram'] ?>
							<input type="text" class="form-control w-25" value="<?= $row['ram'] ?>"></h6>
						  </div>					  
					  </td>
					  <td class="align-middle text-sm">
						  <div class="d-flex flex-column justify-content-center">
							<h6 class="mb-0 text-sm"><?php  $swin = $swin + $row['storage'] ?>
							<input type="text" class="form-control w-25" value="<?= $row['storage'] ?>"></h6>
						  </div>	
					  </td>						  
					  <td>
						<button class="save btn btn-sm btn-success">Save</button>
					  </td>					  
					
					
					</tr>
				  <?php } ?>
					<tr class="">									
					  <td>
						  <div class="d-flex flex-column justify-content-center">
							<h6 class="mb-0 text-lg text-primary">Total Resources</h6>
						  </div>						
					  </td>
					  <td class="align-middle text-lg">
						  <div class="d-flex flex-column justify-content-center">
							<h6 class="mb-0 text-lg text-left text-primary"><?= $cwin ?></h6>
						  </div>	
					  </td>					  
					  <td class="align-middle text-left">
						  <div class="d-flex flex-column justify-content-center">
							<h6 class="mb-0 text-lg text-primary"><?= $rwin ?> GB</h6>
						  </div>					  
					  </td>
					  <td class="align-middle text-sm">
						  <div class="d-flex flex-column justify-content-center">
							<h6 class="mb-0 text-left text-lg text-primary"><?= $swin ?> GB</h6>
						  </div>	
					  </td>						  
					  
					
					
					</tr>				  
				  </tbody>
				</table>
			  </div>
			  <!--<div class="row ps-3 pt-3">
				<ul class="pagination pagination-info">
				<li class="page-item">
				  <a class="page-link" href="#link" aria-label="Previous">
					<span aria-hidden="true"><i class="ni ni-bold-left" aria-hidden="true"></i></span>
				  </a>
				</li>
				<li class="page-item active">
				  <a class="page-link " href="#link">1</a>
				</li>
				</li>
				<li class="page-item">
				  <a class="page-link" href="#link" aria-label="Next">
					<span aria-hidden="true"><i class="ni ni-bold-right" aria-hidden="true"></i></span>
				  </a>
				</li>
				</ul>				
				</div>	-->	
		</div>
		</div>
	</div>
</div>

<script>

$(document).ready(function()
{
	$('.save').on('click',function(){
		var cell = $(this).parent().parent();
		var ip = cell.find('input:eq(0)');
		var cores = cell.find('input:eq(1)');
		var ram = cell.find('input:eq(2)');
		var storage = cell.find('input:eq(3)');
		
		Swal.fire({
		  title: 'Confirm Node Info Changes.',
		  text: 'are you sure ?',
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, Change it!'
		}).then((result) => {
		  if (result.isConfirmed) 
		  {
			  var data = [];
			  cell.find('input').each(function(i){
				  data.push($(this).val());
			  });
			  $.post(ROLE_BASE_URL+'/api/update-node',{
				  'data': data
			  },function(response)
			  {
				  document.location.reload();
			  },'json');
		  }
		});		
	});
});

</script>
<?= $this->endSection() ?>
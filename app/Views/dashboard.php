<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>

<div class="col-12">
	<div class="card">
		<div class="card-header">
			<h4>Matakuliah berlangsung</h4>
		</div>
		<div class="table-responsive">
			<table class="table align-items-center mb-0">
				<thead>
					<tr>
						<th class=" text-center text-uppercase text-primary text-sm font-weight-bolder opacity-7">No</th>
						<th class=" text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">Kode</th>
						<th class=" text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">Nama Matakuliah</th>
						<th class=" text-center text-uppercase text-primary text-sm font-weight-bolder opacity-7">Status Nilai</th>
						<th class=" text-center text-uppercase text-primary text-sm font-weight-bolder opacity-7">Status Asesmen LO</th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 1;foreach($ws['matakuliah'] as $kdmk => $nama_matkul){ ?>
					<tr>
						<td class="col-0 text-center"><?= $i ?></td>
						<td><?= $kdmk ?></td>
						<td><?= $nama_matkul ?></td>
						<td class="text-center">
							<i class="fa fa-times text-danger"></i>
							<span class="text-dark text-xs">Belum</span>
						</td>
						<td class="text-center">
							<i class="fa fa-times text-danger"></i>
							<span class="text-dark text-xs">Belum</span>
						</td>						
					</tr>
					<?php $i++;} ?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="card mt-3">
		<div class="card-header pb-0 p-3">
			<h4>Histori Asesmen Matakuliah</h4>
		</div>
		<div class="card-body pb-0 p-3 mb-4">
			<div class="form-group">
				<label class="h5">Matakuliah</label>
				<select class="form-control" id="exampleFormControlSelect1">
					<option><< Pilih Matakuliah >></option>
					<?php foreach($ws['matakuliah'] as $kdmk => $nama_matkul){ ?>
					<option value="<?= $kdmk ?>"><?= $kdmk.' '.$nama_matkul ?></option>
					<?php } ?>
				</select>			
			</div>
			<div class="row">
				<div class="col-sm-1">
					<?php foreach(get_lo() as $lo){ ?>
					<div class="form-check mb-2">
					  <input name="same" class="form-check-input" type="radio">
					  <label>LO <?=$lo?></label>
					</div>
					<?php } ?>
				</div>
				<div class="col">
					<canvas id="chart-line"></canvas>
				</div>
				<div class="col-sm-3">
					<table class="table table-bordered align-items-center mb-0">
						<thead>
							<tr>
								<th class=" text-center text-uppercase text-primary text-sm font-weight-bolder opacity-7">Nilai CA</th>
								<th class=" text-center text-uppercase text-primary text-sm font-weight-bolder opacity-7">Kategori</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach(nilai_ca() as $ca){ ?>
							<tr class="text-center text-bolder">
								<td><?= $ca->nilai ?></td>
								<td><?= $ca->kategori ?></td>
							</tr>
							<?php } ?>		
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>

$(document).ready(function()
{
    var ctx1 = document.getElementById("chart-line").getContext("2d");

    var gradientStroke1 = ctx1.createLinearGradient(0, 230, 0, 50);

    gradientStroke1.addColorStop(1, 'rgba(94, 114, 228, 0.2)');
    gradientStroke1.addColorStop(0.2, 'rgba(94, 114, 228, 0.0)');
    gradientStroke1.addColorStop(0, 'rgba(94, 114, 228, 0)');
 const xValues = [2019,2020,2021,2022,2023,2024];

new Chart('chart-line', {
  type: "line",
  data: {
    labels: xValues,
    datasets: [
		{
		  data: [1,2,2,1,3,2,2,3,4,4],
		  
		  label: "LO Terases",
          tension: 0.4,
          borderWidth: 0,
          pointRadius: 0,
          borderColor: "#2dce89",
          backgroundColor: gradientStroke1,
          borderWidth: 3,
          fill: true,
          maxBarThickness: 3
		},{
		  data: [2,3,2,2,1,3,3,3,3,4],
		  label: "LO Aktual",
          tension: 0.4,
          borderWidth: 0,
          pointRadius: 0,
          borderColor: "#5e72e4",
          backgroundColor: gradientStroke1,
          borderWidth: 3,
          fill: true,
          maxBarThickness: 3
		}
	]
  },
  options: {
    legend: {display: false},
	title: {
      display: true,
      text: "XXXXXXXXXXXXXXXXXXXXXXXXX",
      fontSize: 16
    }
  }
});
	
	
});

</script>
<?= $this->endSection() ?>
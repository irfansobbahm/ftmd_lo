			  <?php $session = session(); ?>
			  <div class="row">	  
				<div class="col-md-6">
				  <p class="text-uppercase text-md">Personal Information(<span style="color:red;">Required</span>).</p>
				  <hr class="horizontal dark">				
				  <div class="form-group">
					<label for="example-text-input" class="form-control-label">Your Name</label>
					<input required value="<?= ($session->has('name') ? $session->get('name') : old('app_booking_hpc.name')); ?>" class="form-control" type="text" name="app_booking_hpc[name]" onfocus="focused(this)" onfocusout="defocused(this)">
				  </div>
				  <div class="form-group">
					<label for="example-text-input" class="form-control-label">Provide NIM/NIK</label>
					<input required value="<?= old('app_booking_hpc.nim_nik');?>" class="form-control" type="text" name="app_booking_hpc[nim_nik]" onfocus="focused(this)" onfocusout="defocused(this)">
				  </div>
				  <div class="form-group">
					<label for="example-text-input" class="form-control-label">Your Telephone Number</label>
					<input required value="<?= old('app_booking_hpc.tlp');?>" class="form-control" type="text" name="app_booking_hpc[tlp]" onfocus="focused(this)" onfocusout="defocused(this)">
				  </div>			  
				  <div class="form-group">
					<label for="example-text-input" class="form-control-label">E-Mail Address</label>
					<input required value="<?= $session->has('email') ? $session->get('email') : old('app_booking_hpc.email');?>" class="form-control" type="email" name="app_booking_hpc[email]" onfocus="focused(this)" onfocusout="defocused(this)">
				  </div>
				  <div class="form-group">
					<label for="example-text-input" class="form-control-label">HPC Used For ?</label>
					<select required class="form-control" name="app_booking_hpc[purpose_id]" onfocus="focused(this)" onfocusout="defocused(this)">
						<?php foreach($model['bookingPurpose'] as $purpose){ ?>
						<option value="<?= $purpose['id'] ?>" <?= $purpose['id'] == old('app_booking_hpc.purpose_id') ? 'selected':'' ?>><?= $purpose['title'] ?></option>
						<?php } ?>
					</select>
				  </div>
				  <div class="form-group <?= isset(session()->get('error')['doc']) ? 'has-danger':'' ?>">
					<label for="example-text-input" class="form-control-label"><span class="text-danger">Required document template</span> can be download <a class="text-info" href="https://ftmd.itb.ac.id/wp-content/uploads/sites/385/2022/01/Form-Layanan-HPC-ver2.pdf">here</a> (PDF <i class="fa fa-file-pdf" style="color:red;"></i>).</label>
					<input required class="form-control <?= isset(session()->get('error')['doc']) ? 'is-invalid':'' ?>" type="file" name="doc" onfocus="focused(this)" onfocusout="defocused(this)">
					  <?php if(session()->has('error') && isset(session()->get('error')['doc'])){ ?>
					  <span id="error-doc" class="text-danger" role="alert">
						  <strong>Error : </strong> <?= session()->get('error')['doc'] ?>
					  </span>					
					  <?php } ?>
					  
				  </div>
				</div>
				<div class="col-md-6">
				  <p class="text-uppercase text-md">HPC Booking request(<span style="color:red;">Required</span>).</p>
				  <hr class="horizontal dark">
				  <div class="form-group">
					<label for="example-text-input" class="form-control-label">HPC Package</label>
					
					<select required class="form-control" name="app_booking_hpc[package_id]" onfocus="focused(this)" onfocusout="defocused(this)">
						<option></option>
						<?php foreach($model['package'] as $package){ ?>
						<option data-os="<?= $package['os_id'] ?>" value="<?= $package['id'] ?>" <?= $package['id'] == old('app_booking_hpc.package_id') ? 'selected':'' ?> data-package-specs='<?= json_encode($package) ?>'><?= $package['title'] ?></option>
						<?php } ?>
					</select>
				  </div>		
				  <div class="form-group" id="list-package-specs">
					<label class="form-control-label">Package Specs <span id="package-specs-label"></span></label>
					<ul class="list-group" id="package-specs"></ul>
				  </div>					
				  
				  <div class="form-group">
					<label for="example-text-input" class="form-control-label">Operating System</label>
					<label for="example-text-input" id='os-label' class="form-control-label"></label>
					<select required class="form-control d-none" name="app_booking_hpc[os_id]" onfocus="focused(this)" onfocusout="defocused(this)">
						<option></option>
						<?php foreach($model['os'] as $os){ ?>
						<option value="<?= $os['id'] ?>" <?= $os['id'] == old('app_booking_hpc.os_id') ? 'selected':'' ?> data-os-softwares='<?= json_encode($os['softwares']) ?>'><?= $os['os'] ?></option>
						<?php } ?>
					</select>
				  </div>
				  <div class="form-group" id="list-os-softwares">
					<label for="example-text-input" class="form-control-label">Available Software <span id="os-softwares-label"></span></label>
					<ul class="list-group">
					  <ul class="list-group" id="os-softwares"></ul>
					</ul>
				  </div>
				  <div class="form-group">
					<label for="example-text-input" class="form-control-label">Choose Booking Date ( Max to 7 days )</label>
					<div class="input-group input-group-alternative mb-4">				
						<span class="input-group-text"><i class="fa fa-calendar"></i></span>				
						<input disabled required readonly name="booking_date" class="form-control datepicker" placeholder="Please select date after choose your package" type="date" onfocus="focused(this)" onfocusout="defocused(this)">					
					</div>				
					
				  </div>
								  
				</div>		  
			  </div>
			  
			  <!--- SCRIPTS SECTIONS -->
			  <script>
				var BASE_ROLE_URL = "<?= base_role_url() ?>";
				  $(function(){
					  $('select[name="app_booking_hpc[package_id]"]').on('change',function()
					  {
						  if($(this).val() != ''){
							  $('input[name="booking_date"]').prop('disabled',false);
								function rmydays(date) {
									return (date.getDay() === 0 || date.getDay() === 6);
								}	
							  //var disabledDate = ["2022-03-10"];

							  $('select[name="app_booking_hpc[os_id]"]').val($(this).find(':selected').data('os'));
							  $('select[name="app_booking_hpc[os_id]"]').trigger('change');
							  osText = $('select[name="app_booking_hpc[os_id]"]').find(':selected').text();
							  $('#os-label').html('<h6 class="text-primary">'+osText+'</h6>');
							  validateBookingDate();
																  
						  }
					  });
					  
					  //alert(disable.indexOf("2022-03-11"));
					$("input[name='doc']").change(function () {
						$(this).parent().removeClass('has-danger');
						$(this).removeClass('is-invalid');	
						$(this).parent().removeClass('has-success');
						$(this).removeClass('is-valid');						
						if(this.files[0].type != 'application/pdf'){
							$(this).parent().addClass('has-danger');
							$(this).addClass('is-invalid');
						}else{							
							$(this).parent().addClass('has-success');
							$(this).addClass('is-valid');
							$('#error-doc').remove();
						}
					});					  
					  
					  // HPC Package dropdown field.
					  $('#list-package-specs').hide();
					  $('select[name="app_booking_hpc[package_id]"]').on('change',function(e)
					  {
						  $('#list-package-specs').hide();
						  $(this).find('option').each(function(i)
						  {
							  if($(this).attr('value') == $('select[name="app_booking_hpc[package_id]"]').val())
							  {
								 $('#list-package-specs').show();
								 
								 data = $(this).data('package-specs');
								 $('#package-specs').html('');
								 $('#package-specs-label').text('');
								 $('#package-specs-label').text('- '+data.title);
								 $('#package-specs').append('<li class="list-group-item">CORES : <span style="color:blue;">'+data.cores+'</span></li>');
								 $('#package-specs').append('<li class="list-group-item">RAM : <span style="color:blue;">'+data.ram+' GB</span></li>');
								 $('#package-specs').append('<li class="list-group-item">STORAGE : <span style="color:blue;">'+data.storage+' GB</span></li>');
							  }
						  });					    
					  });
					  $('select[name="app_booking_hpc[package_id]"]').trigger('change');

					  // HPC OS software dropdown field.
					  var objListCtrEl = $('#list-os-softwares');
					  var objListEl = $('#os-softwares');
					  var objListlabelEl = $('#os-softwares-label');
					  objListCtrEl.hide();
					  $('select[name="app_booking_hpc[os_id]"]').on('change',function(e)
					  {
						  validateBookingDate();
						  
						  
						  objListCtrEl.hide();
						  $(this).find('option').each(function(i)
						  {
							  if($(this).attr('value') == $('select[name="app_booking_hpc[os_id]"]').val())
							  {
								 objListCtrEl.show();
								 
								 data = $(this).data('os-softwares');
								 
								 objListEl.html('');
								 objListlabelEl.text('');
								 objListlabelEl.text('- '+$(this).text());
								 $(data).each(function(i,val){
									 objListEl.append('<li class="list-group-item"><span style="color:blue;">'+val.software+'</span></li>');
								 });
								 
							  }
						  });	
					  });
					  $('select[name="app_booking_hpc[os_id]"]').trigger('change');
					  
					function validateBookingDate()
					{
						$.get(BASE_ROLE_URL+'/api/get-booked-dates',{
							package_id:$('select[name="app_booking_hpc[package_id]"]').val(),
							os_id:$('select[name="app_booking_hpc[os_id]"]').val()
						},function(response){
											  
						 var disabledDate = response;
						  if (document.querySelector('.datepicker')) {
							flatpickr('.datepicker', {
							  mode: "range",//"multiple", //"range",
								minDate: "today",
								position:"top",
								disable: disabledDate,
								defaultDate:"<?= old('booking_date');?>",
								onDayCreate: function(dObj, dStr, fp, dayElem){
									//$(dayElem).addClass('badge badge-danger');
									// if (Math.random() < 0.15)
										// dayElem.innerHTML += "<span class='event'></span>";

									// else
										var date = new Date(dayElem.dateObj);
									
										if (disabledDate.indexOf(date.getFullYear()+'-'+("0" + (date.getMonth() + 1)).slice(-2)+'-'+("0" + date.getDate()).slice(-2)) != -1)
											dayElem.innerHTML += "<span class='event busy'></span>";
								},
								// onChange: function(selectedDates, dateStr, instance) 
								// {
									// var selectedDatesStr = selectedDates.reduce(function(acc, ele) {
										// var str = instance.formatDate(ele, "d/m/Y");
										// acc = (acc == '') ? str : acc + ';' + str;
										// return acc;
									// }, '');				
									
									// instance.set('enable', [function(date) 
									// {
										// if (selectedDates.length == 7) {
											// var currDateStr = instance.formatDate(date, "d/m/Y")
											// var x = selectedDatesStr.indexOf(currDateStr);
											// return x != -1;
										// }else{
											
											// var currDateStr = instance.formatDate(date, "Y-m-d");
											// return disabledDate.indexOf(currDateStr) == -1 ? true : false;
										// }
										
									// }]);
									
								// }			
								// maxDate: new Date().fp_incr(14),		  
								onChange: function(selectedDates, dateStr, instance) { //FOR MODE RANGE
									var selectedDatesStr = selectedDates.reduce(function(acc, ele) {
										var str = instance.formatDate(ele, "d/m/Y");
										acc = (acc == '') ? str : acc + ';' + str;
										return acc;
									}, '');
									
									var date = new Date(selectedDates);
									date.setDate(date.getDate() + 6);

									instance.set('maxDate',date);
								}
							}); // flatpickr
						  }  	
						},'json');	
					}
				  });
			  </script>
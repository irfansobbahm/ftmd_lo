<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>

<div class="row">
	<div class="col-12">
		<div class="card card-calendar">
			<div class="card-body p-3">
			<!--<hr class="horizontal dark my-3">-->
			<div class="calendar" data-bs-toggle="calendar" id="calendar"></div>		
			</div>
		</div>
	</div>	
</div>
<script>
    var base_role_url = "<?= base_role_url() ?>";
	$(document).ready(function(){
	var calendar = new FullCalendar.Calendar(document.getElementById("calendar"), {
	  initialView: "dayGridMonth",
	  headerToolbar: {
		start: 'title', // will normally be on the left. if RTL, will be on the right
		center: '',
		end: 'today prev,next' // dayGridMonth,timeGridWeek,
	  },
	  selectable: false,
	  editable: false,
	  initialDate: new Date(),
	  allDay:true,
	  eventSources: [

		// your event source
		{
		  url: base_role_url+'/api/get-booked-calendar',
		  method: 'GET',
		  failure: function() {
			alert('there was an error while fetching events!');
		  }
		}
		

		// any other sources...

	  ],
		eventClassNames: function(arg) {
		  switch (parseInt(arg.event.extendedProps.title_id)) {
			case (1 || 2):
				return ['bg-gradient-success'];
			break;
			case 3 :
				return ['bg-gradient-warning'];
			break;	
			case (4 || 5) :
				return ['bg-gradient-danger'];
			break;
			case 6 :
				return ['bg-gradient-info'];
			break;
			case (7 || 8) :
				return ['bg-gradient-primary'];
			break;
			case 9 :
				return ['bg-gradient-primary'];
			break;
			default:
				return ['bg-gradient-success'];
			break;
		  }
		},	  
	  views: {
		month: {
		  titleFormat: {
			month: "long",
			year: "numeric"
		  }
		},
		agendaWeek: {
		  titleFormat: {
			month: "long",
			year: "numeric",
			day: "numeric"
		  }
		},
		agendaDay: {
		  titleFormat: {
			month: "short",
			year: "numeric",
			day: "numeric"
		  }
		}
	  },
	});

	calendar.render();  
	});
  </script>  
<?= $this->endSection() ?>


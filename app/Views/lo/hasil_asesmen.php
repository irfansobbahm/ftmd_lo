<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>


	<div class="row">
		<div class="card mb-3 p-4">
			<div class="card-header p-0 pb-3">
				<div class="row">
					<div class="col-md-8 d-flex align-items-center">
					<h4 class="mb-0">FTMD - Teknik Dirgantara</h4>
					</div>
				</div>
			</div>	
			<form method="GET" action="">
				<div class="row mb-3">
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">Tahun Akademik </label>
						<select class="form-control form-control-md" id="tahun_akademik">
						<?php foreach($ws['tahunkuliah'] as $tahunkuliah){ ?>
							<option value="<?= $tahunkuliah ?>" <?= (isset($uriSegments[4]) && $uriSegments[4] == $tahunkuliah) ? 'selected':'' ?>><?= $tahunkuliah.'/'.($tahunkuliah +1) ?></option>
						<?php } ?>
						</select>
					
					</div>	
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">Semester</label>
						<?php if(session()->get('role') == 'dosen'){ ?>

							<select class="form-control form-control-md" id="semester">
							<?php foreach(['Ganjil','Genap'] as $sem){ ?>
								<option <?= (date('n')<=6 && strtolower($sem) == strtolower('Genap') ? 'selected':'') ?> value="<?= htmlentities(strtolower($sem)) ?>"><?= $sem ?></option>
							<?php } ?>
							<option>Semester Pendek</option>
							</select>


						<?php } else { ?>
							<select class="form-control form-control-md" id="semester">
								<option value="">Seluruh</option>
								<?php for($n=0;$n<=13;$n++){ ?>
								<option value=""><?= $n + 1 ?></option>
								<?php } ?>
							</select>
						<?php } ?>
						

					
					</div>				
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">Matakuliah</label>
						<select class="form-control form-control-md"  id="kd_kuliah">
						<?php foreach($ws['matakuliah'] as $kd_mk => $matakuliah){ ?>
							<option value="<?= $kd_mk ?>" <?= (isset($uriSegments[3]) && $uriSegments[3] == $kd_mk) ? 'selected':'' ?>><?= $matakuliah.' (<b>'.$kd_mk.'</b>)' ?></option>
						<?php } ?>
						</select>
					
					</div>					
						
				</div>
			</form>
		</div>
	</div>

	<div class="row mt-3">
		<div class="card pb-3">
			<div class="row mt-2" style="margin-right:10px;margin-left:10px;">
				<div class="card-header col-md-8 p-0 pb-3">
					<div class="row">
						<div class="col-md-10 d-flex align-items-center">
						<h4 class="mb-0"><?= $catNilai; ?> - <b>Matakuliah 
						<?= isset($uriSegments[3]) ? $uriSegments[3] : 'MT3103'?>
						(<?= isset($uriSegments[4]) ? $uriSegments[4].'/'.($uriSegments[4] + 1) : date('Y').'/'.(date('Y') + 1) ?>) </b></h4>
						</div>
					</div>
				</div>			
			</div>
			<div class="row">
			  <div class="col-8 table-responsive">
				<table class="table table-sm table-bordered table-striped align-items-center mb-0">			
				  <thead>
					<?php $x = []; ?>
					<tr>
						<td class="text-end text-uppercase text-primary text-sm font-weight-bolder opacity-7" colspan="3">Rata-rata</td>
						<?php foreach(range('A','G') as $v){ ?>
						<td class="rr-lo<?= $v ?> font-weight-bolder"></td>
						<?php } ?>
					</tr>
					<tr>
					  <td class="col-sm-1 text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">No</td>
					  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">NIM</td>
					  <td class=" text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">Nama</td>
					  <?php foreach(range('A','G') as $v){ ?>
						<td class=" text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">LO <?= $v ?></td>
					  <?php } ?>
					</tr>
				  </thead>
				  <tbody>
				  <?php foreach($ws['mahasiswa'] as $n => $row){ ?>
					<tr class="rMhs">
					  <td class="text-center"><?= $n + 1 ?></td>
					  <td><?= $row->nim; ?></td>
					  <td class="cNama" style="max-width:400px !important;">
						<div class="d-flex px-2 py-1">
						  <div class="d-flex flex-column justify-content-center">
							<h6 class="mb-0 text-xs"><?= $row->nama; ?></h6>
							
						  </div>
						</div>
					  </td>	
					  <?php foreach(range('A','G') as $v){ ?>
						<?php //$x[$v][] = (isset($row->nilaiLo[$v]) ? $row->nilaiLo[$v] : 0); ?>
						<td data-lo="<?= $v ?>" class="nilai-lo text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7"><?= (isset($row->nilaiLo[$v]) ? $row->nilaiLo[$v] : '0.00'); ?></td>
					  <?php } ?>					  
					</tr>
				  <?php } ?>
				  </tbody>
				</table>
			  </div>
			  <div class="col-4">
				  <div class="card ps-2 pe-2 mb-2">
					<div class="card-header text-center">
						<h5>Course Outcome</h5>
					</div>
					<div>
						<h6>LO Terases</h6>
						<input type="hidden" id="id_loterases" value="<?= @$lo_terases->id ?>">
						<table class="table-sm table-bordered align-items-center mb-2" style="width:100%;border-color:#e9ecef;">
							<thead>
								<tr>
									<?php foreach(range('A','G') as $v){ ?>
									<td class=" text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">LO <?= $v ?></td>
									<?php } ?>
								</tr>
							</thead>
							<tbody>
								<tr>
									<?php $lot = @json_decode($lo_terases->hasil_ases); ?>
									
									<?php foreach(range('A','G') as $v){ ?>
									<td class=" text-left text-uppercase text-primary text-sm " <?= (@$loAktual->{'lo_'.strtolower($v)} == @$lot->{strtolower($v)}) ? 'style="background-color:#2dce89;"':'style="background-color:#ffcb19;"' ?>>
										<select class="form-control loter-<?=strtolower($v)?>" data-lo="<?= strtolower($v) ?>">
											<option value="" <?= (@$lot->{strtolower($v)} == '') ? 'selected' : '' ?>></option>
											<option value="H" <?= (@$lot->{strtolower($v)} == 'H') ? 'selected' : '' ?>>H</option>
											<option value="M" <?= (@$lot->{strtolower($v)} == 'M') ? 'selected' : '' ?>>M</option>
											<option value="L" <?= (@$lot->{strtolower($v)} == 'L') ? 'selected' : '' ?>>L</option>
										</select>
									</td>
									<?php } ?>
								</tr>						
							</tbody>
						</table>
						
						<h6>LO Aktual</h6>
						<table class="table-sm table-bordered align-items-center mb-0" style="width:100%;border-color:#e9ecef;">
							<thead>
								<tr>
									<?php foreach(range('A','G') as $v){ ?>
									<td class=" text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">LO <?= $v ?></td>
									<?php } ?>
								</tr>
							</thead>
							<tbody>
								<tr>
								<?php foreach(range('A','G') as $v){ ?>
									<td class="loak-<?=strtolower($v)?> text-left text-uppercase text-primary text-sm "><?= @$loAktual->{'lo_'.strtolower($v)} ?></td>
								<?php } ?>	
								</tr>						
							</tbody>
						</table>
						<div class="row mt-3">
							<div class="col text-center">
								<h6>Course Outcome Terases</h6>
								<h2 id="co-terases-label">0.00</h2>
							</div>
							<div class="col text-center">
								<h6>Course Outcome Aktual</h6>
								<h2 id="co-aktual-label">0.00</h2>
							</div>
						</div>
					</div>
				  </div>
				  <div class="card ps-2 pe-2">
					<div class="card-header text-center">
						<h5>Course Assessment (CA)</h5>
					</div>
					<div class="">
						<h6>Portofolio</h6>
						<table class="table-sm table-bordered align-items-center mb-2" style="width:100%;border-color:#e9ecef;">
							<thead>
								<tr>
									<td class="text-left text-uppercase text-primary text-sm font-weight-bolder">Kehadiran(%)</td>
									<td class="text-left text-uppercase text-primary text-sm font-weight-bolder">Nilai</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="font-weight-bolder">100</td>
									<td class="font-weight-bolder">4</td>
								</tr>						
							</tbody>
						</table>	
						
						<h6>Kuesioner</h6>
						<table class="table-sm table-responsive table-bordered align-items-center mb-2" style="width:100%;border-color:#e9ecef;">
							<thead>
								<tr>
									<?php foreach(range(1,12) as $coln){ ?>
									<td class="text-left text-uppercase text-primary text-sm font-weight-bolder"><?= $coln ?></td>
									<?php } ?>
								</tr>
							
							</thead>
							<tbody>
								<tr>
									<?php foreach(range(1,12) as $xn){ ?>
									<td class="text-left text-uppercase"><?= (rand(35, 38) / 10) ?></td>
									<?php } ?>
								</tr>								
								<tr>
									<td colspan="6" class="text-left text-uppercase text-primary text-sm font-weight-bolder">Rata-rata</td>
									<td colspan="6" class="text-left text-uppercase text-primary text-sm font-weight-bolder">-</td>
								</tr>						
							</tbody>
						</table>
						<div class="row mt-3">
							<div class="col text-center">
								<h6>Course Assesment Terases</h6>
								<h2>3.12</h2>
							</div>
							<div class="col text-center">
								<h6>Course Assesment Aktual</h6>
								<h2>2.82</h2>
							</div>
						</div>						
					</div>
				  </div>
			  </div>
			</div>
		</div>
	</div>
	<script>
	var totalBobot = 0;
	function confirm(){
		Swal.fire({
		  title: 'Konfirmasi',
		  text: "Anda akan melakukan pengajuan perubahan nilai.",
		  icon: 'question',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Saya mengerti, dan Ajukan.',
		  cancelButtonText: 'Batal'
		}).then((result) => {
		  if (result.isConfirmed) {
			Swal.fire(
			  'Berhasil!',
			  'Pengajuan telah berhasil di kirim.',
			  'success'
			)
		  }
		})		
	}
	
	$(function()
	{
		$('#kd_kuliah, #tahun_akademik, #semester').on('change',function(){
			var kd_kuliah = $('#kd_kuliah').val();
			var tahun_akademik = $('#tahun_akademik').val();
			var semester = $('#semester').val();
			var nim_mhs = $('#nim_mhs').val();
			var nama_mhs = $('#nama_mhs').val();	
			
			document.location.href='<?= base_role_url(); ?>/learning-outcome/<?= $catNilaiSlug ?>/'+kd_kuliah+'/'+tahun_akademik+'/'+semester;
		});

		$(document).on('keyup','.val-soal',function(){
			//$(this).parent().find(".val-total-nilai-mhs").text();
			var valSoal = [];
			$(this).parent().find('.val-soal').each(function(){

				if(!isNaN(parseFloat($(this).text()))){
					
					valSoal.push(parseFloat($(this).text()));
				}else{
					valSoal.push(0);
				}
				
			});
			var valBobot = [];
			$('.val-bobot').each(function(){

				if(!isNaN(parseFloat($(this).text()))){
					
					valBobot.push(parseFloat($(this).text()));
				}else{
					valBobot.push(0);
				}
				
			});
			x = valSoal;
			y = valBobot;
			var totalNilaiMhs = SUMPRODUCT((x, y) => x > 0, x, y).toFixed(2);
			$(this).parent().find(".val-total-nilai-mhs").text(totalNilaiMhs);
			var currElIndex = $(this).index();
			var avg = [];
			$('.rMhs').find('td:eq('+currElIndex+')').each(function()
			{
				if(!isNaN(parseFloat($(this).text())))
				{
					avg.push(parseFloat($(this).text()));
				}
			});

			var average = avg.reduce((a, b) => a + b, 0) / avg.length;
			var stdev = DEVIATION(avg);
			
			average = (isNaN(average)) ? 0 : average;
			stdev = (isNaN(stdev)) ? 0 : stdev;
			
			$('#rRataRata').find('td').eq(currElIndex - 1).text(average.toFixed(2));
			$('#rDeviasi').find('td').eq(currElIndex - 1).text(stdev.toFixed(2));
			
		});
		
		$(document).on('blur','.val-bobot',function(evt){
			var maxbobot = 1;
			var curEl = $(this);
			numberStr = curEl.text();
			currVal = limitNumberWithinRange(numberStr, 0, maxbobot);
			
			sVal = 0;
			curEl.siblings().each(function(i){
				if(i > 0){
					sVal = (isNaN(parseFloat($(this).text())) ? 0 : parseFloat($(this).text())) + sVal;
					
				}
			});
			sumBobot = (sVal + currVal);
			$('.sum-bobot').html(sumBobot.toFixed(2));
			if(sumBobot <= maxbobot){
				curEl.html(currVal);
			}else{
				curEl.html(0);
				val = 0;
				$('.val-bobot').each(function()
				{
					if(!isNaN(parseFloat($(this).text()))){
						val = val + parseFloat($(this).text());
					}
					
				});
				$('.sum-bobot').html(val.toFixed(2));
			}
			$('.row-lo-total').each(function(i)
			{
				var loTotal = $(this);
				var yy = 0;
				$(this).parent().find('.val-val').each(function(i)
				{
					
					_val = (parseFloat($('.val-bobot').eq(i).text()) * parseFloat($(this).text()));
					
					if(!isNaN(_val)){
						yy = yy + _val;
						loTotal.text(yy.toFixed(2));
					}
				});
			});			
			
			
		});		
	
       function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
             return false;
		  }

          return true;
       }		
		function limitNumberWithinRange(num, min, max){
		  const MIN = min || 0;
		  const MAX = max || 1;
		  const parsed = parseFloat(num).toFixed(2)
		  return Math.min(Math.max(parsed, MIN), MAX)
		}		
		
		$('td').on('keyup',function(){
			if($(this).attr('id') == 'cel-jumlah-soal')
			{
				jmlSoal = parseInt($(this).text());
				$('#cel-jumlah-soal').attr('colspan',jmlSoal);
				
				$('#rata-rata').nextAll('td').remove();
				$('#deviasi').nextAll('td').remove();
				$('#bobot').nextAll('td').remove();
				$('#nilai-total-mhs').nextAll('td').remove();
				$('.val-total-nilai-mhs').nextAll('td').remove();
				
				for(n=0;n < jmlSoal;n++)
				{
					$('#rata-rata').after("<td></td>");
					
					$('#deviasi').after("<td></td>");
					
					$('#bobot').after("<td class='val-bobot' type='number' min='0' max='2.1' step='0.1' contenteditable='true'></td>");
					
					$('#nilai-total-mhs').after("<td class='font-weight-bolder text-primary align-middle text-center text-sm'>Soal "+(jmlSoal - n)+"</td>");
					$('.val-total-nilai-mhs').after("<td class='val-soal text-center' contenteditable='true'></td>");
				}
			}		

			
		});
	});
	
	$('.lo-checkbox').on('change',function()
	{
		if ($(this).is(":checked")) {
			
			selectedLo = $(this).data('lo');
		} 
		
		$('.rem').remove();
		
		lohead = '<tr class="rem text-uppercase text-primary text-sm font-weight-bolder opacity-7">';
		lohead += '<td colspan="3" rowspan="2" class="rem dt text-end">Bobot(0-1)</td>';
		for(n=0;n <=3;n++){
			lohead += '<td rowspan="2" contenteditable class="val-bobot"></td>';
		}
		lohead += '<td class=" text-center text-uppercase text-primary text-sm font-weight-bolder opacity-7">Total Bobot</td>';
		lohead += '</tr>';
		lohead += '<tr class="rem text-uppercase text-primary text-sm font-weight-bolder opacity-7">';
		lohead += '<td class="sum-bobot">'+totalBobot+'</td>';
		lohead += '</tr>';
	
		if(selectedLo == 'A'){
			loakomp = '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UAS (Soal 1)</td>';
			loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UAS (Soal 3)</td>';
			loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UTS (Soal 2)</td>';
			loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">Kuis (Kuis 3)</td>';
			loakomp += '<td class="rem text-center text-uppercase text-primary text-sm font-weight-bolder opacity-7">LO '+selectedLo+'</td>';
		}
		
		if(selectedLo == 'B'){
			loakomp = '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UAS (Soal 1)</td>';
			loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UAS (Soal 2)</td>';
			loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UAS (Soal 4)</td>';
			loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">Kuis (Kuis 3)</td>';
			loakomp += '<td class="rem text-center text-uppercase text-primary text-sm font-weight-bolder opacity-7">LO '+selectedLo+'</td>';
		}
		
		if(selectedLo == 'C'){
			loakomp = '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UTS (Soal 2)</td>';
			loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UTS (Soal 3)</td>';
			loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UTS (Soal 4)</td>';
			loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">Kuis (Kuis 5)</td>';
			loakomp += '<td class="rem text-center text-uppercase text-primary text-sm font-weight-bolder opacity-7">LO '+selectedLo+'</td>';
		}
		
		if(selectedLo == 'D'){
			loakomp = '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UAS (Soal 2)</td>';
			loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UAS (Soal 3)</td>';
			loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UTS (Soal 1)</td>';
			loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">Kuis (Kuis 1)</td>';
			loakomp += '<td class="rem text-center text-uppercase text-primary text-sm font-weight-bolder opacity-7">LO '+selectedLo+'</td>';
		}		
		$('thead').prepend(lohead);
		
		$('thead tr:eq(2)').append(loakomp);
		for(n=0;n <=3;n++){
			$('.rMhs').append('<td class="rem val-val"></td>');
		}
		$('.rMhs').append('<td class="rem row-lo-total"></td>');
		$('.val-val').each(function()
		{
			$(this).text((randomNumber(50,100)));
		});
		
		$('.rMhs').each(function(){
			$(this).find('.cNama').nextUntil( ".row-lo-total" ).each(function(){
				cellnum = parseInt($(this).text());
				
			});
		});
		

		
	});	
	function randomNumber(min, max) {
		min = Math.ceil(min);
		max = Math.floor(max);
		
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}	
	
		// $.post('<?= base_role_url('learning-outcome/data-kelompok-nilai') ?>',{
			// tahun:$('#tahun_akademik').val(),
			// semester:$('#semester').val(),
			// kd_kuliah:$('#kd_kuliah').val(),
		// },function(payload)
		// {
			// // $('#id_komponen_asesmen').val(payload.komponen_asesmen.id);
			// console.log(payload);
		// });
		
		$(document).ready(function()
		{
			val = {};
			avg = {};
			$('.nilai-lo').each(function()
			{
				lo = $(this).data('lo');
				avg[lo] = 0;
				if(val.hasOwnProperty(lo))
				{
					val[lo].push($(this).text());
				}else{
					val[lo] = [];
					val[lo].push($(this).text());
				}		
			});
			sumLoak = 0;
			sumLoakIdx = 0;
			for(n in avg){
				perloavg = (val[n].reduce((a, b) => parseFloat(a) + parseFloat(b), 0) / val[n].length).toFixed(2);
				$('.rr-lo'+n).text(perloavg);
				
				loidx = $('.loak-'+n.toLowerCase()).text();
				//console.log(loidx);
				if( loidx == 'H'){
					sumLoak = (parseFloat(perloavg) * 3) + sumLoak;
					sumLoakIdx = sumLoakIdx + 3;
				}
				if( loidx == 'M'){
					sumLoak = (parseFloat(perloavg) * 2) + sumLoak;
					sumLoakIdx = sumLoakIdx + 2;
				}	
				if( loidx == 'L'){
					sumLoak = (parseFloat(perloavg) * 1) + sumLoak;
					sumLoakIdx = sumLoakIdx + 1;
				}					
			}
			$('#co-aktual-label').text((sumLoak.toFixed(2) / sumLoakIdx).toFixed(2));
			
			$('select[class*=loter-]').on('change',function(){
				lo = $(this).data('lo');
				if($(this).val().trim() == $('.loak-'+lo).text().trim()){
					$(this).parent().css('background-color','#2dce89');
				}else{
					$(this).parent().css('background-color','#ffcb19');
				}
				
				val = {};
				avg = {};
				$('.nilai-lo').each(function()
				{
					lo = $(this).data('lo');
					avg[lo] = 0;
					if(val.hasOwnProperty(lo))
					{
						val[lo].push($(this).text());
					}else{
						val[lo] = [];
						val[lo].push($(this).text());
					}		
				});
				sumLoak = 0;
				sumLoakIdx = 0;
				for(n in avg){
					perloavg = (val[n].reduce((a, b) => parseFloat(a) + parseFloat(b), 0) / val[n].length).toFixed(2);
					//$('.rr-lo'+n).text(perloavg);
					
					loidx = $('.loter-'+n.toLowerCase()).val();
					//console.log(loidx);
					if( loidx == 'H'){
						sumLoak = (parseFloat(perloavg) * 3) + sumLoak;
						sumLoakIdx = sumLoakIdx + 3;
					}
					if( loidx == 'M'){
						sumLoak = (parseFloat(perloavg) * 2) + sumLoak;
						sumLoakIdx = sumLoakIdx + 2;
					}	
					if( loidx == 'L'){
						sumLoak = (parseFloat(perloavg) * 1) + sumLoak;
						sumLoakIdx = sumLoakIdx + 1;
					}					
				}
				$('#co-terases-label').text((sumLoak.toFixed(2) / sumLoakIdx).toFixed(2));


				hasil_ases = {};
				$('select[class*=loter-]').each(function()
				{
					hasil_ases[$(this).data('lo')] = $(this).val();
				});
				$.post('<?= base_role_url('learning-outcome/save-lo-terases') ?>',{
					lo_terases:{
						'id':$('#id_loterases').val(),
						tahun_akademik:$('#tahun_akademik').val(),
						semester:$('#semester').val(),
						kode_kuliah:$('#kd_kuliah').val(),
						dosen_uuid:'<?= session()->get('id'); ?>',
						'hasil_ases' : hasil_ases				
					}
				},function(payload){
					if(payload.rowid != undefined)
					{
						$('#id_loterases').val(payload.rowid)
									
					}else{
						Swal.fire(
						  'Data berhasil di simpan.',
						  '',
						  'danger'
						)						
					}
				},'json');				
			});
				
			//$('select[class*=loter-]').trigger('change');
		});
	</script>
<?= $this->endSection() ?>
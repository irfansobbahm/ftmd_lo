<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>


	<div class="row">
		<div class="card mb-3 p-4">
			<div class="card-header p-0 pb-3">
				<div class="row">
					<div class="col-md-8 d-flex align-items-center">
					<h4 class="mb-0">FTMD - Teknik Dirgantara</h4>
					</div>
				</div>
			</div>	
			<form method="GET" action="">
				<div class="row mb-3">
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">Tahun Akademik </label>
						<select class="form-control form-control-md" id="tahun_akademik">
						<?php foreach($ws['tahunkuliah'] as $tahunkuliah){ ?>
							<option value="<?= $tahunkuliah ?>" <?= (isset($uriSegments[4]) && $uriSegments[4] == $tahunkuliah) ? 'selected':'' ?>><?= $tahunkuliah.'/'.($tahunkuliah +1) ?></option>
						<?php } ?>
						</select>
					
					</div>	
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">Semester</label>
						<?php if(session()->get('role') == 'dosen'){ ?>

							<select class="form-control form-control-md" id="semester">
							<?php foreach(['Ganjil','Genap'] as $sem){ ?>
								<option <?= (date('n')<=6 && strtolower($sem) == strtolower('Genap') ? 'selected':'') ?> value="<?= htmlentities(strtolower($sem)) ?>"><?= $sem ?></option>
							<?php } ?>
							<option>Semester Pendek</option>
							</select>


						<?php } else { ?>
							<select class="form-control form-control-md" id="semester">
								<option value="">Seluruh</option>
								<?php for($n=0;$n<=13;$n++){ ?>
								<option value=""><?= $n + 1 ?></option>
								<?php } ?>
							</select>
						<?php } ?>
						

					
					</div>				
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">Matakuliah</label>
						<select class="form-control form-control-md"  id="kd_kuliah">
						<?php foreach($ws['matakuliah'] as $kd_mk => $matakuliah){ ?>
							<option value="<?= $kd_mk ?>" <?= (isset($uriSegments[3]) && $uriSegments[3] == $kd_mk) ? 'selected':'' ?>><?= $matakuliah.' (<b>'.$kd_mk.'</b>)' ?></option>
						<?php } ?>
						</select>
					
					</div>					
						
				</div>
				<div class="row mt-1 mb-2">
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">NIM Mahasiswa</label>
						<div class="form-group">
							<input type="text" id="nim_mhs" name="nim" class="form-control">
						</div>
					
					</div>	
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">Nama Mahasiswa</label>
						<div class="form-group">
							<input type="text" id="nama_mhs" name="nama" class="form-control">
						</div>
					
					</div>			
				</div>
				<div class="row">
					<div class="col">
						<button type="submit" class="btn btn-primary btn-sm">Tampilkan Data</button>					
					</div>	
					<div class="col text-end">
						<button type="button" class="btn btn-primary btn-sm">Download Rekap Nilai</button>
						<button type="button" class="btn btn-success btn-sm">Upload Rekap Nilai</button>				
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class="row mt-3">
		<div class="card">
			<div class="row mt-2" style="margin-right:10px;margin-left:10px;">
				<div class="card-header col-md-8 p-0 pb-3">
					<div class="row">
						<div class="col-md-10 d-flex align-items-center">
						<h4 class="mb-0">LO Formula - <b>Matakuliah 
						<?= isset($uriSegments[3]) ? $uriSegments[3] : 'MT3103'?>
						(<?= isset($uriSegments[4]) ? $uriSegments[4].'/'.($uriSegments[4] + 1) : date('Y').'/'.(date('Y') + 1) ?>) </b></h4>
						</div>
					</div>
				</div>			
				<div class="col-md-4 text-end">
					<a href="javascript:;" onclick="inputKategoriNilaiBaru()" title="Tambah kategori nilai">
						<i class="fas fa-plus text-secondary text-sm" data-bs-toggle="tooltip" data-bs-placement="top" aria-hidden="true" aria-label="Edit Profile"></i><span class="sr-only">Edit Profile</span>
					</a>
&nbsp;					
					<a href="javascript:;" onclick="confirm()" title="Edit">
						<i class="fas fa-pencil-alt text-secondary text-sm " data-bs-toggle="tooltip" data-bs-placement="top" aria-hidden="true" aria-label="Edit Profile"></i><span class="sr-only">Edit Profile</span>
					</a>
				</div>
			</div>				
		  <div class="table-responsive">
			<table class="table table-sm table-bordered table-striped align-items-center mb-0">			
			  <thead>
								
				<tr>
				  <td class=" text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7" rowspan="2">Mahasiswa</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7" colspan="7">UAS</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7" colspan="7">UTS</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7" colspan="7">Praktikum</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7" colspan="7">Quiz</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7" colspan="7">PR</td>
				</tr>
				<tr>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">A</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">B</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">C</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">D</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">E</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">F</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">G</td>
				  
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">A</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">B</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">C</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">D</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">E</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">F</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">G</td>

				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">A</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">B</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">C</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">D</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">E</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">F</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">G</td>

				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">A</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">B</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">C</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">D</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">E</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">F</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">G</td>

				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">A</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">B</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">C</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">D</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">E</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">F</td>
				  <td class="text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">G</td>				  
				</tr>
				
			  </thead>
			  <tbody>
			  <?php foreach($ws['mahasiswa'] as $row){ ?>
				<tr class="rMhs">
				  <td class="" style="max-width:400px !important;">
					<div class="d-flex px-2 py-1">
					  <div>
						<img src="https://upload.wikimedia.org/wikipedia/id/9/95/Logo_Institut_Teknologi_Bandung.png" class="avatar avatar-sm me-3">
					  </div>
					  <div class="d-flex flex-column justify-content-center">
						<h6 class="mb-0 text-xs"><?= $row->nama; ?></h6>
						<p class="text-xs text-secondary mb-0">NIM <?= $row->nim; ?></p>
					  </div>
					</div>
				  </td>
				  <td class="val-total-nilai-mhs text-primary font-weight-bolder align-middle text-center">
					
				  </td>
				  <td></td>				  
				</tr>
			  <?php } ?>
			  </tbody>
			</table>
		  </div>
			
		</div>
	</div>
	<script>
	
	function confirm(){
		Swal.fire({
		  title: 'Konfirmasi',
		  text: "Anda akan melakukan pengajuan perubahan nilai.",
		  icon: 'question',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Saya mengerti, dan Ajukan.',
		  cancelButtonText: 'Batal'
		}).then((result) => {
		  if (result.isConfirmed) {
			Swal.fire(
			  'Berhasil!',
			  'Pengajuan telah berhasil di kirim.',
			  'success'
			)
		  }
		})		
	}
	
	function inputKategoriNilaiBaru()
	{
		form = '';
		form += '<div class="form-group">';
		form += '	<label for="example-text-input" class="form-control-label">Nama Kategori Nilai</label>';
		form += '	<form method="POST" action="<?= base_role_url('kategori-nilai/save-kategori-nilai') ?>">';
		form += '<input name="label" required="" value="" class="form-control" type="text" onfocus="focused(this)" onfocusout="defocused(this)">';
		form += '<button class="btn btn-default" type="submit">Simpan</button>';
		form += '	</form>';
		form += '</div>';
		
		Swal.fire({
		  title: 'Input Kategori Nilai Baru',
		  html:form,
		  showCloseButton: true,
		  showCancelButton: false,
		  showConfirmButton: false,
		  focusConfirm: false,
		  confirmButtonText:
			'Simpan',
		  cancelButtonText:
			'Batal',
		  cancelButtonAriaLabel: 'Thumbs down'
		}, function() {
            confirmed = true;
            $this.submit();
        });		
	}	
	
	$(function()
	{
		$('#kd_kuliah, #tahun_akademik, #semester').on('change',function(){
			var kd_kuliah = $('#kd_kuliah').val();
			var tahun_akademik = $('#tahun_akademik').val();
			var semester = $('#semester').val();
			var nim_mhs = $('#nim_mhs').val();
			var nama_mhs = $('#nama_mhs').val();	
			
			document.location.href='<?= base_role_url(); ?>/learning-outcome/<?= $catNilaiSlug ?>/'+kd_kuliah+'/'+tahun_akademik+'/'+semester;
		});

		$(document).on('keyup','.val-soal',function(){
			//$(this).parent().find(".val-total-nilai-mhs").text();
			var valSoal = [];
			$(this).parent().find('.val-soal').each(function(){

				if(!isNaN(parseFloat($(this).text()))){
					
					valSoal.push(parseFloat($(this).text()));
				}else{
					valSoal.push(0);
				}
				
			});
			var valBobot = [];
			$('.val-bobot').each(function(){

				if(!isNaN(parseFloat($(this).text()))){
					
					valBobot.push(parseFloat($(this).text()));
				}else{
					valBobot.push(0);
				}
				
			});
			x = valSoal;
			y = valBobot;
			var totalNilaiMhs = SUMPRODUCT((x, y) => x > 0, x, y).toFixed(2);
			$(this).parent().find(".val-total-nilai-mhs").text(totalNilaiMhs);
			var currElIndex = $(this).index();
			var avg = [];
			$('.rMhs').find('td:eq('+currElIndex+')').each(function()
			{
				if(!isNaN(parseFloat($(this).text())))
				{
					avg.push(parseFloat($(this).text()));
				}
			});

			var average = avg.reduce((a, b) => a + b, 0) / avg.length;
			var stdev = DEVIATION(avg);
			
			average = (isNaN(average)) ? 0 : average;
			stdev = (isNaN(stdev)) ? 0 : stdev;
			
			$('#rRataRata').find('td').eq(currElIndex - 1).text(average.toFixed(2));
			$('#rDeviasi').find('td').eq(currElIndex - 1).text(stdev.toFixed(2));
			
		});
		
		$(document).on('blur','.val-bobot',function(evt){
			var maxbobot = 1;
			var curEl = $(this);
			numberStr = curEl.text();
			currVal = limitNumberWithinRange(numberStr, 0, maxbobot);
			
			sVal = 0;
			curEl.siblings().each(function(i){
				if(i > 0){
					sVal = (isNaN(parseFloat($(this).text())) ? 0 : parseFloat($(this).text())) + sVal;
					
				}
			});
			sumBobot = (sVal + currVal);
			
			if(sumBobot <= maxbobot){
				curEl.html(currVal);
			}else{
				curEl.html(0);
			}
			
			// Validasi bobot harus sama dengan 1
			if(sumBobot != maxbobot){
				$('.val-bobot').each(function(){
					$(this).css('border','1px solid red');
				});
			}else{
				$('.val-bobot').each(function(){
					$(this).removeAttr('style');
				});				
			}
			
			$('.val-soal').trigger('keyup');
		});		
       function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
             return false;
		  }

          return true;
       }		
		function limitNumberWithinRange(num, min, max){
		  const MIN = min || 0;
		  const MAX = max || 1;
		  const parsed = parseFloat(num).toFixed(2)
		  return Math.min(Math.max(parsed, MIN), MAX)
		}		
			
	
		
		$('td').on('keyup',function(){
			if($(this).attr('id') == 'cel-jumlah-soal')
			{
				jmlSoal = parseInt($(this).text());
				$('#cel-jumlah-soal').attr('colspan',jmlSoal);
				
				$('#rata-rata').nextAll('td').remove();
				$('#deviasi').nextAll('td').remove();
				$('#bobot').nextAll('td').remove();
				$('#nilai-total-mhs').nextAll('td').remove();
				$('.val-total-nilai-mhs').nextAll('td').remove();
				
				for(n=0;n < jmlSoal;n++)
				{
					$('#rata-rata').after("<td></td>");
					
					$('#deviasi').after("<td></td>");
					
					$('#bobot').after("<td class='val-bobot' type='number' min='0' max='2.1' step='0.1' contenteditable='true'></td>");
					
					$('#nilai-total-mhs').after("<td class='font-weight-bolder text-primary align-middle text-center text-sm'>Soal "+(jmlSoal - n)+"</td>");
					$('.val-total-nilai-mhs').after("<td class='val-soal text-center' contenteditable='true'></td>");
				}
			}		

			
		});
	});
	
	</script>
<?= $this->endSection() ?>
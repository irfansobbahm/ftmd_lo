<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>


	<div class="row">
		<div class="card mb-3 p-4">
			<div class="card-header p-0 pb-3">
				<div class="row">
					<div class="col-md-8 d-flex align-items-center">
					<h4 class="mb-0">FTMD - Teknik Dirgantara</h4>
					</div>
				</div>
			</div>	
			<form method="GET" action="">
				<div class="row mb-3">
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">Tahun Akademik </label>
						<select class="form-control form-control-md" id="tahun_akademik">
							<option></option>
						<?php foreach($ws['tahunkuliah'] as $tahunkuliah){ ?>
							<option value="<?= $tahunkuliah ?>" <?= (isset($uriSegments[4]) && $uriSegments[4] == $tahunkuliah) ? 'selected':'' ?>><?= $tahunkuliah.'/'.($tahunkuliah +1) ?></option>
						<?php } ?>
						</select>
					
					</div>	
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">Semester</label>
						<?php if(session()->get('role') == 'dosen'){ ?>

							<select class="form-control form-control-md" id="semester">
								<option></option>
							<?php foreach(['Ganjil','Genap'] as $sem){ ?>
								<option <?= (strtolower($sem) == strtolower($uriSegments[5]) ? 'selected':'') ?> value="<?= htmlentities(strtolower($sem)) ?>"><?= $sem ?></option>
							<?php } ?>
							<option>Semester Pendek</option>
							</select>


						<?php } else { ?>
							<select class="form-control form-control-md" id="semester">
								<option value="">Seluruh</option>
								<?php for($n=0;$n<=13;$n++){ ?>
								<option value=""><?= $n + 1 ?></option>
								<?php } ?>
							</select>
						<?php } ?>
						

					
					</div>				
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">Matakuliah</label>
						<select class="form-control form-control-md"  id="kd_kuliah">
							<option></option>
						<?php foreach($ws['matakuliah'] as $kd_mk => $matakuliah){ ?>
							<option value="<?= $kd_mk ?>" <?= (isset($uriSegments[3]) && $uriSegments[3] == $kd_mk) ? 'selected':'' ?>><?= $kd_mk.' '.$matakuliah ?></option>
						<?php } ?>
						</select>
					
					</div>					
						
				</div>
				<!--
				<div class="row">
					<div class="col">
						<button type="submit" class="btn btn-primary btn-sm">Tampilkan Data</button>					
					</div>	
				</div>
				-->
			</form>
		</div>
	</div>

	<div class="row mt-3">
		<div class="card">
			<div class="row mt-2 ms-2 me-2">
				<div class="card-header col-md-8 p-0 pb-3">
					<div class="row">
						<div class="col-md-10 d-flex align-items-center">
						<h4 class="mb-0">Komponen Asesmen - <b>Matakuliah 
						<?= isset($uriSegments[3]) ? $uriSegments[3] : 'MT3103'?> </b></h4>
						<input id="id_komponen_asesmen" type="hidden" value="">
						</div>
					</div>
					
				</div>			
<hr class="horizontal dark">
			</div>				
		  <div class="mb-3">
				<div class="text-end">
					<button type="button" id="atur-ulang-komponen-asesmen" class="btn bg-gradient-primary">Atur ulang</button>
					<button type="button" id="simpan-komponen-asesmen" class="btn bg-gradient-primary">Simpan</button>
					<!--<button type="button" class="btn bg-gradient-primary">Verifikasi</button>-->
				</div>		  
			  <div class="row">
			  <div class="col-md ms-2">
			  <h5>Learning Outcome</h5>
			  <div class="row" style="margin-right:10px;margin-left:10px;">
				
				<?php foreach(range('A','G') as $v){ ?>
					<div class="form-check col-md">
					  <input class="form-check-input lo-checkbox" data-lo="<?= $v ?>" type="radio" value="" name="LO">
					  <label class="custom-control-label" for="customCheck1">LO <?= $v ?></label>
					</div>
				<?php } ?>
			  </div>		  
				<label>Komponen Nilai</label>
				
				<table id="tbl-komp-nilai" class="table table-sm table-bordered table-striped align-items-center mb-0">			
				  <thead>				
					<tr>
					  <?php foreach(default_kategori_nilai() as $defkat => $list){ ?>
					  <td class=" text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7"><?= $defkat ?></td>
					  <?php } ?>
					</tr>
				  </thead>
				  <tbody>
					
					<?php 
					if(count($kategori_nilai) > 0){
					for($n=1;$n<=get_higher_soal($kategori_nilai);$n++){ ?>
						<tr>
							<?php 
							$defkatnil = default_kategori_nilai();
							foreach($defkatnil as $defkat => $list){ ?>
							<td>
							<?php 
							$katnil = find_kategori_nilai($kategori_nilai, $defkat);
							if($katnil && $katnil->jumlah_soal >= $n){ ?>
								<div class="form-check">
								  <input class="form-check-input komp-<?= strtolower($defkat) ?>" data-kategori_nilai="<?= strtolower($defkat) ?>" type="checkbox" value="<?= $defkatnil[$defkat].' '. $n ?>">
								  <label class="custom-control-label"><?= $defkatnil[$defkat].' '. $n ?></label>
								</div>		
							<?php } ?>
							</td>						
							<?php } ?>
						</tr>
						<?php } ?>
					<?php } ?>
					
				  </tbody>
				</table>
			</div>
			
			<div class="col-md">

				<label>Rangkuman</label>
				<table class="table table-sm table-bordered table-striped align-items-center mb-0" style="word-wrap:break-word;">			
				  <thead>				
					<tr>
					  <td class="col-sm-2 text-center text-uppercase text-primary text-sm font-weight-bolder opacity-7">LO</td>
					  <td class=" text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">Komponen Asesmen</td>
					</tr>
				  </thead>
				  <tbody>
					<?php foreach(range('A','G') as $v){ ?>
						<tr>
							<td class="text-center fw-bolder">LO <?= $v ?></td>
							<td class="rankuman-lo-<?= $v ?> text-wrap "></td>
						</tr>
					<?php } ?>			  
				  </tbody>
				  </table>
			</div>
			</div>
		  </div>
			
		</div>
	</div>
	<script>
	
	var selectedLo ='';
	var rangkum = emptyRangkum = {A:{
			uas:[],
			uts:[],
			praktikum:[],
			quiz:[],
			pr:[],
		},B:{
			uas:[],
			uts:[],
			praktikum:[],
			quiz:[],
			pr:[],
		},C:{
			uas:[],
			uts:[],
			praktikum:[],
			quiz:[],
			pr:[],
		},D:{
			uas:[],
			uts:[],
			praktikum:[],
			quiz:[],
			pr:[],
		},E:{
			uas:[],
			uts:[],
			praktikum:[],
			quiz:[],
			pr:[],
		},F:{
			uas:[],
			uts:[],
			praktikum:[],
			quiz:[],
			pr:[],
		},G:{
			uas:[],
			uts:[],
			praktikum:[],
			quiz:[],
			pr:[],
		}};
	const merge = (first, second) => {
	  for(let i=0; i<second.length; i++) {
		first.push(second[i]);
	  }
	  return first;
	}
	if(!$.isEmptyObject(<?= json_encode(@$komponen_asesmen['komponen_rangkuman']) ?>)){
		rangkum = merge(<?= json_encode(@$komponen_asesmen['komponen_rangkuman']) ?>, rangkum);
	}
	$.post('<?= base_role_url('learning-outcome/data-komponen-asesmen') ?>',{
		tahun:$('#tahun_akademik').val(),
		semester:$('#semester').val(),
		kd_kuliah:$('#kd_kuliah').val(),
	},function(payload)
	{
		$('#id_komponen_asesmen').val(payload.komponen_asesmen.id);
		// console.log(payload.komponen_rangkuman.length);
		if(!$.isEmptyObject(payload.komponen_rangkuman)){
			rangkum = merge(payload.komponen_rangkuman,rangkum);
		}

	},'json');
	

	
	// CHECKBOX RANGKUMAN EVENTS
	$('body').on('change','input[class*=komp-]',function()
	{
		kategori_nilai = $(this).data('kategori_nilai');
		rangkum[selectedLo][kategori_nilai] ={};
		x = [];
		$('.komp-'+kategori_nilai).each(function(v,i){
			if ($(this).is(":checked")) {
				
				if($(this).data('id')){
					rangkum[selectedLo][kategori_nilai]['id-'+$(this).data('id')] = $(this).val().split(" ")[1];
				}else{
					
					x.push($(this).val().split(" ")[1]);
					//rangkum[selectedLo][kategori_nilai].push( $(this).val().split(" ")[1] );
					rangkum[selectedLo][kategori_nilai] = Object.assign(rangkum[selectedLo][kategori_nilai], x);
				}
				
			}else{
				if($(this).data('id')){
					rangkum[selectedLo][kategori_nilai]['id-'+$(this).data('id')] = '';
				}				
			}
		});
		rangkum_();
	});	
	
	$('.lo-checkbox').on('change',function()
	{
		if ($(this).is(":checked")) {
			
			selectedLo = $(this).data('lo');
		} 
		
			
		defkatnil = <?= json_encode(default_kategori_nilai()) ?>;
		
		$('[class*=komp-]').removeAttr('data-id');
		
		$('#tbl-komp-nilai').find('input[type="checkbox"]').prop('checked', false);
	
		for(i in rangkum[selectedLo]){
			
			for(x in rangkum[selectedLo][i]){
				y = x.split('-');
				v = rangkum[selectedLo][i][x];
				
				if(v != "" && v != undefined){
					$('.komp-'+i+'[value="'+defkatnil[i.toUpperCase()]+' '+v+'"]').prop('checked', true);
					if(y.length > 1){
						$('.komp-'+i+'[value="'+defkatnil[i.toUpperCase()]+' '+v+'"]').attr('data-id',y[1]);
					}
				}
			}
		}
		rangkum_();		
		// rangkum = emptyRangkum;
		// rangkum_();
	});
	
	$('#simpan-komponen-asesmen').on('click',function(){
		console.log(rangkum);

		$.post('<?= base_role_url('learning-outcome/save-komponen-asesmen') ?>',{
			komponen_asesmen:{
				'id':$('#id_komponen_asesmen').val(),
				tahun_akademik:$('#tahun_akademik').val(),
				semester:$('#semester').val(),
				kode_kuliah:$('#kd_kuliah').val(),
				dosen_uuid:'<?= session()->get('id'); ?>'			
			},
			komponen_asesmen_rangkuman : rangkum
		},function(payload){
			payload = $.parseJSON(payload);
			if(payload.rowid != undefined)
			{
				Swal.fire(
				  'Data berhasil di simpan.',
				  '',
				  'success'
				)				
			}
		});
	});
	
	$('#atur-ulang-komponen-asesmen').on('click',function()
	{
		for(lo in rangkum){
			locheckbox = $('.lo-checkbox[data-lo='+lo+']');
			locheckbox.trigger('click');
			
			if(locheckbox.prop( "checked"))
			{
				$('input[class*=komp-]').each(function(i)
				{
					console.log($(this).prop( "checked"));
					if($(this).prop( "checked"))
					{
						$(this).prop( "checked", false);
						$(this).trigger( "change");
						rangkum_();
					}
				});
			}
		}
				

		$('.lo-checkbox[data-lo=A]').trigger('click');
	});
	
	function rangkum_()
	{
		defkatnil = <?= json_encode(default_kategori_nilai()) ?>;
		var conc = [];
		for(i in rangkum[selectedLo])
		{
			if(rangkum[selectedLo][i].length != 0){
				for(x in rangkum[selectedLo][i]){
					if(rangkum[selectedLo][i][x] != ''){
						conc.push(i +" - "+defkatnil[i.toUpperCase()]+" "+rangkum[selectedLo][i][x]);
					}
				}
			}
			
		}
		conc = conc.sort();
		console.log(conc);
		html ='';
		for(n in conc){
			html += '<span class="ms-1 badge badge-info">'+conc[n]+'</span>';
		}
		$('.rankuman-lo-'+selectedLo).html(html);
		conc = [];
	}
	

	// $('.komp-uts').on('change',function()
	// {
		// rangkum[selectedLo].uts =[];
		// $('.komp-uts').each(function(v,i){
			// if ($(this).is(":checked")) {
				// rangkum[selectedLo].uts.push($(this).val());
			// }
		// });
		// rangkum_();
	// });	
	// $('.komp-praktikum').on('change',function()
	// {
		// rangkum[selectedLo].praktikum =[];
		// $('.komp-praktikum').each(function(v,i){
			// if ($(this).is(":checked")) {
				// rangkum[selectedLo].praktikum.push($(this).val());
			// }
		// });
		// rangkum_();
	// });	
	// $('.komp-quiz').on('change',function()
	// {
		// rangkum[selectedLo].quiz =[];
		// $('.komp-quiz').each(function(v,i){
			// if ($(this).is(":checked")) {
				// rangkum[selectedLo].quiz.push($(this).val());
			// }
		// });
		// rangkum_();
	// });	
	// $('.komp-pr').on('change',function()
	// {
		// rangkum[selectedLo].pr =[];
		// $('.komp-pr').each(function(v,i){
			// if ($(this).is(":checked")) {
				// rangkum[selectedLo].pr.push($(this).val());
			// }
		// });
		// rangkum_();
	// });	
	function confirm(){
		Swal.fire({
		  title: 'Konfirmasi',
		  text: "Anda akan melakukan pengajuan perubahan nilai.",
		  icon: 'question',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Saya mengerti, dan Ajukan.',
		  cancelButtonText: 'Batal'
		}).then((result) => {
		  if (result.isConfirmed) {
			Swal.fire(
			  'Berhasil!',
			  'Pengajuan telah berhasil di kirim.',
			  'success'
			)
		  }
		})		
	}
	
	function inputKategoriNilaiBaru()
	{
		form = '';
		form += '<div class="form-group">';
		form += '	<label for="example-text-input" class="form-control-label">Nama Kategori Nilai</label>';
		form += '	<form method="POST" action="<?= base_role_url('kategori-nilai/save-kategori-nilai') ?>">';
		form += '<input name="label" required="" value="" class="form-control" type="text" onfocus="focused(this)" onfocusout="defocused(this)">';
		form += '<button class="btn btn-default" type="submit">Simpan</button>';
		form += '	</form>';
		form += '</div>';
		
		Swal.fire({
		  title: 'Input Kategori Nilai Baru',
		  html:form,
		  showCloseButton: true,
		  showCancelButton: false,
		  showConfirmButton: false,
		  focusConfirm: false,
		  confirmButtonText:
			'Simpan',
		  cancelButtonText:
			'Batal',
		  cancelButtonAriaLabel: 'Thumbs down'
		}, function() {
            confirmed = true;
            $this.submit();
        });		
	}	
	
	$(function()
	{
		$('#kd_kuliah, #tahun_akademik, #semester').on('change',function(e){
			
			var thnakademik = $('#tahun_akademik').val();
			var semester = $('#semester').val();
			
			if(thnakademik == '')
			{
				alert("Silahkan pilih tahun akademik");
				$(this).prop('selected',false);
				//e.preventDefault();
				return false;
			}
			if(semester == '')
			{
				alert("Silahkan pilih semester");
				$(this).prop('selected',false);
				//e.preventDefault();
				return false;
			}			
			
			var kd_kuliah = $('#kd_kuliah').val();
			var tahun_akademik = $('#tahun_akademik').val();
			var semester = $('#semester').val();
			var nim_mhs = $('#nim_mhs').val();
			var nama_mhs = $('#nama_mhs').val();	
			
			document.location.href='<?= base_role_url(); ?>/learning-outcome/<?= $catNilaiSlug ?>/'+kd_kuliah+'/'+tahun_akademik+'/'+semester;
		});

		$(document).on('keyup','.val-soal',function(){
			//$(this).parent().find(".val-total-nilai-mhs").text();
			var valSoal = [];
			$(this).parent().find('.val-soal').each(function(){

				if(!isNaN(parseFloat($(this).text()))){
					
					valSoal.push(parseFloat($(this).text()));
				}else{
					valSoal.push(0);
				}
				
			});
			var valBobot = [];
			$('.val-bobot').each(function(){

				if(!isNaN(parseFloat($(this).text()))){
					
					valBobot.push(parseFloat($(this).text()));
				}else{
					valBobot.push(0);
				}
				
			});
			x = valSoal;
			y = valBobot;
			var totalNilaiMhs = SUMPRODUCT((x, y) => x > 0, x, y).toFixed(2);
			$(this).parent().find(".val-total-nilai-mhs").text(totalNilaiMhs);
			var currElIndex = $(this).index();
			var avg = [];
			$('.rMhs').find('td:eq('+currElIndex+')').each(function()
			{
				if(!isNaN(parseFloat($(this).text())))
				{
					avg.push(parseFloat($(this).text()));
				}
			});

			var average = avg.reduce((a, b) => a + b, 0) / avg.length;
			var stdev = DEVIATION(avg);
			
			average = (isNaN(average)) ? 0 : average;
			stdev = (isNaN(stdev)) ? 0 : stdev;
			
			$('#rRataRata').find('td').eq(currElIndex - 1).text(average.toFixed(2));
			$('#rDeviasi').find('td').eq(currElIndex - 1).text(stdev.toFixed(2));
			
		});
		
		$(document).on('blur','.val-bobot',function(evt){
			var maxbobot = 1;
			var curEl = $(this);
			numberStr = curEl.text();
			currVal = limitNumberWithinRange(numberStr, 0, maxbobot);
			
			sVal = 0;
			curEl.siblings().each(function(i){
				if(i > 0){
					sVal = (isNaN(parseFloat($(this).text())) ? 0 : parseFloat($(this).text())) + sVal;
					
				}
			});
			sumBobot = (sVal + currVal);
			
			if(sumBobot <= maxbobot){
				curEl.html(currVal);
			}else{
				curEl.html(0);
			}
			
			// Validasi bobot harus sama dengan 1
			if(sumBobot != maxbobot){
				$('.val-bobot').each(function(){
					$(this).css('border','1px solid red');
				});
			}else{
				$('.val-bobot').each(function(){
					$(this).removeAttr('style');
				});				
			}
			
			$('.val-soal').trigger('keyup');
		});		
       function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
             return false;
		  }

          return true;
       }		
		function limitNumberWithinRange(num, min, max){
		  const MIN = min || 0;
		  const MAX = max || 1;
		  const parsed = parseFloat(num).toFixed(2)
		  return Math.min(Math.max(parsed, MIN), MAX)
		}		
			
	
		
		$('td').on('keyup',function(){
			if($(this).attr('id') == 'cel-jumlah-soal')
			{
				jmlSoal = parseInt($(this).text());
				$('#cel-jumlah-soal').attr('colspan',jmlSoal);
				
				$('#rata-rata').nextAll('td').remove();
				$('#deviasi').nextAll('td').remove();
				$('#bobot').nextAll('td').remove();
				$('#nilai-total-mhs').nextAll('td').remove();
				$('.val-total-nilai-mhs').nextAll('td').remove();
				
				for(n=0;n < jmlSoal;n++)
				{
					$('#rata-rata').after("<td></td>");
					
					$('#deviasi').after("<td></td>");
					
					$('#bobot').after("<td class='val-bobot' type='number' min='0' max='2.1' step='0.1' contenteditable='true'></td>");
					
					$('#nilai-total-mhs').after("<td class='font-weight-bolder text-primary align-middle text-center text-sm'>Soal "+(jmlSoal - n)+"</td>");
					$('.val-total-nilai-mhs').after("<td class='val-soal text-center' contenteditable='true'></td>");
				}
			}		

			
		});
	});
	$(document).ready(function()
	{
		for(lo in rangkum){
			$('.lo-checkbox[data-lo='+lo+']').trigger('click');
		}
		$('.lo-checkbox[data-lo=A]').trigger('click');
	});	
	</script>
<?= $this->endSection() ?>
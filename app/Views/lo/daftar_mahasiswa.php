<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>


	<div class="row">
		<div class="card mb-3 p-4">
			<div class="card-header p-0 pb-3">
				<div class="row">
					<div class="col-md-8 d-flex align-items-center">
					<h4 class="mb-0">FTMD - Teknik Dirgantara</h4>
					</div>
				</div>
			</div>	
			<form method="GET" action="">
				<div class="row mb-3">
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">Tahun Akademik </label>
						<select class="form-control form-control-md" id="tahun_akademik">
						<?php foreach($ws['tahunkuliah'] as $tahunkuliah){ ?>
							<option value="<?= $tahunkuliah ?>" <?= (isset($uriSegments[3]) && $uriSegments[3] == $tahunkuliah) ? 'selected':'' ?>><?= $tahunkuliah.'/'.($tahunkuliah +1) ?></option>
						<?php } ?>
						</select>
					
					</div>	
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">Semester</label>
						<?php if(session()->get('role') == 'dosen'){ ?>

							<select class="form-control form-control-md" id="semester">
							<?php foreach(['Ganjil','Genap'] as $sem){ ?>
								<option <?= (date('n')<=6 && $sem == 'Genap' ? 'selected':'') ?> value="<?= htmlentities(strtolower($sem)) ?>"><?= $sem ?></option>
							<?php } ?>
							<option>Semester Pendek</option>
							</select>


						<?php } else { ?>
							<select class="form-control form-control-md" id="semester">
								<option value="">Seluruh</option>
								<?php for($n=0;$n<=13;$n++){ ?>
								<option value=""><?= $n + 1 ?></option>
								<?php } ?>
							</select>
						<?php } ?>
						

					
					</div>				
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">Matakuliah</label>
						<select class="form-control form-control-md"  id="kd_kuliah">
						<?php foreach($ws['matakuliah'] as $kd_mk => $matakuliah){ ?>
							<option value="<?= $kd_mk ?>" <?= (isset($uriSegments[2]) && $uriSegments[2] == $kd_mk) ? 'selected':'' ?>><?= $matakuliah.' (<b>'.$kd_mk.'</b>)' ?></option>
						<?php } ?>
						</select>
					
					</div>					
						
				</div>
				<!--
				<div class="row mt-1 mb-2">
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">NIM Mahasiswa</label>
						<div class="form-group">
							<input type="text" id="nim_mhs" name="nim" class="form-control">
						</div>
					
					</div>	
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">Nama Mahasiswa</label>
						<div class="form-group">
							<input type="text" id="nama_mhs" name="nama" class="form-control">
						</div>
					
					</div>			
				</div>
				-->
				<!--
				<div class="row">
					<div class="col">
						<button type="submit" class="btn btn-primary btn-sm">Tampilkan Data</button>					
					</div>	
					<div class="col text-end">
						<button type="button" class="btn btn-primary btn-sm">Download Rekap Nilai</button>
						<button type="button" class="btn btn-success btn-sm">Upload Rekap Nilai</button>				
					</div>
				</div>
				-->
			</form>
		</div>
	</div>
	
	<div class="row">
		<div class="card mb-3 col" style="margin-right:20px;">	
			<div class="row mt-2" style="margin-right:10px;margin-left:10px;">
				<div class="col-md-8 d-flex align-items-center">
					<h4 class="mb-0">Keterangan Nilai</h4>
				</div>
				<div class="col-md-4 text-end">				
					<a href="javascript:;" title="Edit">
						<i class="fas fa-pencil-alt text-secondary text-sm" data-bs-toggle="tooltip" data-bs-placement="top" aria-hidden="true" aria-label="Edit Profile"></i><span class="sr-only">Edit Profile</span>
					</a>
				</div>
			</div>		
			<div class="row">
				<div class="table-responsive">
					<table class="table align-items-center mb-0">
					  <thead>
						<tr>
						  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">Nilai</th>
						  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">Batas</th>
						  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">Jumlah</th>
						  <th class="text-center text-uppercase text-secondary text-sm font-weight-bolder opacity-7">Bobot</th>
						</tr>
					  </thead>
					  <tbody>
					  <?php foreach($keterangan_nilai as $ketnil){ ?>
					  <tr>
						<td class="text-sm text-center"><?= $ketnil->nilai_huruf ?></td>
						<td class="text-sm text-center"><?= $ketnil->batas_min ?></td>
						<td class="text-sm text-center" id="ketnil-<?= $ketnil->nilai_huruf ?>">0</td>
						<td class="text-sm text-center"><?= $ketnil->bobot ?></td>
					  </tr>
					  <?php } ?>
					  </tbody>
					</table>
				  </div>		
			</div>
		</div>
		<div class="card mb-3 p-4 col">
			<div style="  margin: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);"><h2>IP Kelas : <b id="ip-kelas">0</b></h2></div>
		</div>
	</div>

	<div class="row mt-0">
		<div class="card">
			<div class="row mt-2" style="margin-right:10px;margin-left:10px;">
				<div class="card-header col-md-8 p-0 pb-3">
					<div class="row">
						<div class="col-md-10 d-flex align-items-center">
						<h4 class="mb-0">Rekap Nilai - <b>Matakuliah 
						<?= isset($uriSegments[2]) ? $uriSegments[2] : 'MT3103'?>
						(<?= isset($uriSegments[3]) ? $uriSegments[3].'/'.($uriSegments[3] + 1) : date('Y').'/'.(date('Y') + 1) ?>) </b></h4>
						</div>
					</div>
				</div>			
				<div class="col-md-4 text-end">
					
					<a href="javascript:;" id="save-rekap-nilai" title="Save Bobot Rekap Nilai">
						<i class="fa fa-save text-secondary text-sm " data-bs-toggle="tooltip" data-bs-placement="top" aria-hidden="true" aria-label="Save Rekap Nilai"></i><span class="sr-only">Save Rekap Nilai</span>
					</a>
&nbsp;
					<a href="javascript:;" onclick="inputKategoriNilaiBaru()" title="Tambah kategori nilai">
						<i class="fas fa-plus text-secondary text-sm" data-bs-toggle="tooltip" data-bs-placement="top" aria-hidden="true" aria-label="Edit Profile"></i><span class="sr-only">Edit Profile</span>
					</a>
					
					<!--
					<a href="javascript:;" onclick="confirm()" title="Edit">
						<i class="fas fa-pencil-alt text-secondary text-sm " data-bs-toggle="tooltip" data-bs-placement="top" aria-hidden="true" aria-label="Edit Profile"></i><span class="sr-only">Edit Profile</span>
					</a>
					-->
					
				</div>
			</div>				
		  <div class="table-responsive">
			<input type="hidden" id="id_bobot_rekap" value="<?= @$bobot_rekap_nilai->id; ?>">
			<table class="table align-items-center mb-0">
			  <thead>
				<tr id="rRataRata" class="text-primary">
					<th id="rata-rata" class="col-1 text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">RATA2</th>
					<?php foreach(default_kategori_nilai() as $katnil => $sat){ ?>
						<th></th>
					<?php } ?>
					<th></th>
				</tr>
				<tr id="rDeviasi" class="text-primary">
					<th id="deviasi" class="col-1 text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">DEVIASI</th>
					<?php foreach(default_kategori_nilai() as $katnil => $sat){ ?>
						<th></th>
					<?php } ?>
					<th></th>
				</tr>
				<tr id="rBobot">
					<th id="bobot" class="col-1 text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">BOBOT (0-1)</th>
					<?php $bobot = @json_decode($bobot_rekap_nilai->bobot); ?>
					<?php $i = 0;foreach(default_kategori_nilai() as $katnil => $sat){ ?>
						<th class="val-bobot" contenteditable><?= @$bobot[$i] ?></th>
					<?php $i++;} ?>
					<th></th>
				</tr>			  
				<tr class="rCol">
				  <th class="text-uppercase text-primary text-sm font-weight-bolder opacity-7">Mahasiswa</th>
				  <?php foreach(default_kategori_nilai() as $katnil => $sat){ ?>
				  <th class="text-center text-uppercase text-primary text-sm font-weight-bolder opacity-7"><?= $katnil ?></th>
				  <?php } ?>
				  <th class="text-center text-uppercase text-primary text-sm font-weight-bolder opacity-7">Total</th>
				  <th class="text-center text-uppercase text-primary text-sm font-weight-bolder opacity-7">Huruf</th>
				</tr>
			  </thead>
			  <tbody>
			  <?php foreach($ws['mahasiswa'] as $row){ ?>
				<tr class="rMhs">
				  <td>
					<div class="d-flex px-2 py-1">
					  <div>
						<img src="https://upload.wikimedia.org/wikipedia/id/9/95/Logo_Institut_Teknologi_Bandung.png" class="avatar avatar-sm me-3">
					  </div>
					  <div class="d-flex flex-column justify-content-center">
						<h6 class="mb-0 text-xs"><?= $row->nama; ?></h6>
						<p class="text-xs text-secondary mb-0">NIM <?= $row->nim; ?></p>
					  </div>
					</div>
				  </td>
				  <?php foreach(default_kategori_nilai() as $katnil => $sat){ ?>
				  <td class="align-middle text-center val-soal">
					<?= $nt = isset($row->nilaiTotal[$katnil]) ? $row->nilaiTotal[$katnil] : '0.00'; ?>
				  </td>
				  <?php } ?>
				  <td class="align-middle text-center val-total-nilai-mhs">-</td>
				  <td class="align-middle text-center val-nilai-huruf">
					-
				  </td>				  
				</tr>
			  <?php } ?>
			  </tbody>
			</table>
		  </div>
			
		</div>
	</div>
	<script>
	
	function confirm(){
		Swal.fire({
		  title: 'Konfirmasi',
		  text: "Anda akan melakukan pengajuan perubahan nilai.",
		  icon: 'question',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Saya mengerti, dan Ajukan.',
		  cancelButtonText: 'Batal'
		}).then((result) => {
		  if (result.isConfirmed) {
			Swal.fire(
			  'Berhasil!',
			  'Pengajuan telah berhasil di kirim.',
			  'success'
			)
		  }
		})		
	}
	var jmlH = {};
	var jmlX = {};
	var bbtY = [];
	var bbtX = [];
			
			
			
		$(document).on('blur','.val-bobot',function(evt){
			var maxbobot = 1;
			var curEl = $(this);
			numberStr = curEl.text();
			currVal = limitNumberWithinRange(numberStr, 0, maxbobot);
			
			sVal = 0;
			curEl.siblings().each(function(i){
				if(i > 0){
					sVal = (isNaN(parseFloat($(this).text())) ? 0 : parseFloat($(this).text())) + sVal;
					
				}
			});
			sumBobot = (sVal + currVal);
			
			if(sumBobot <= maxbobot){
				curEl.html(currVal);
			}else{
				curEl.html(0);
			}
			
			// Validasi bobot harus sama dengan 1
			if(sumBobot != maxbobot){
				$('.val-bobot').each(function(){
					//$(this).css('border','1px solid #ffdcdc');
				});
			}else{
				$('.val-bobot').each(function(){
					$(this).removeAttr('style');
				});				
			}
			var valBobot = [];
			$('.val-bobot').each(function(){

				if(!isNaN(parseFloat($(this).text()))){
					
					valBobot.push(parseFloat($(this).text()));
				}else{
					valBobot.push(0);
				}
				
			});			
			var valSoal = [];
			
			$('.rMhs').find('.val-soal').each(function(){
				
				if(!isNaN(parseFloat($(this).text()))){
					
					valSoal.push(parseFloat($(this).text()));
				}else{
					valSoal.push(0);
				}
				
				if(valBobot.length == valSoal.length){
					x = valSoal;
					y = valBobot;
					var totalNilaiMhs = SUMPRODUCT((x, y) => x > 0, x, y).toFixed(2);
					
					$(this).parent().find(".val-total-nilai-mhs").text(totalNilaiMhs);				
					
					nilaiHuruf = keterangan_nilai_huruf(totalNilaiMhs);
					
					$(this).parent().find(".val-nilai-huruf").text(nilaiHuruf);
					
									
					
					valSoal = [];
				}
			});
			var currElIndex = $(this).index();
			var avg = [];
			$('.rMhs').find('td:eq('+currElIndex+')').each(function()
			{
				if(!isNaN(parseFloat($(this).text())))
				{
					avg.push(parseFloat($(this).text()));
				}
			});

			var average = avg.reduce((a, b) => a + b, 0) / avg.length;
			var stdev = DEVIATION(avg);
			
			average = (isNaN(average)) ? 0 : average;
			stdev = (isNaN(stdev)) ? 0 : stdev;
			
			$('#rRataRata').find('th').eq(currElIndex).text(average.toFixed(2));
			$('#rDeviasi').find('th').eq(currElIndex).text(stdev.toFixed(2));
			
			//jmlX = [];
			// for(n in jmlH){
				// a = jmlH[n];
				// var sum = a.reduce((x, y) => {
				  // return parseFloat(x) + parseFloat(y)
				// },0);
				// $('#ketnil-'+n).text(sum);
				// //jmlX.push(sum);
			// }
			
			//
			//rekapJmlHuruf();
			rekapJmlHuruf();
			$('#ip-kelas').text(ipKelas());
			
			jmlX = {};
			jmlH = {};
		});	
		
		function rekapJmlHuruf()
		{
			$('.rMhs').find('.val-nilai-huruf').each(function()
			{
				hrf = $(this).text().trim();
				if(jmlX.hasOwnProperty(hrf)){
					jmlX[hrf] = jmlX[hrf] + 1;
				}else{
					jmlX[hrf] = 1;
				} 
				
			});
			$('td[id*=ketnil-]').each(function(){
				hrf = $(this).attr('id').split('-')[1];
				if(jmlX.hasOwnProperty(hrf)){
					$(this).text(jmlX[hrf]);
				}else{
					$(this).text(0);
				}
			});			
		}
		function ipKelas()
		{
			bbtY = [];
			bbtX = [];

			$('td[id*=ketnil-').each(function()
			{
				bbtX.push($(this).text());
			});
			xx = $.parseJSON('<?= json_encode($keterangan_nilai) ?>');
			for(i in xx){
				bbtY.push(xx[i].bobot);
			}
			ipKelasx = SUMPRODUCT((bbtX, bbtY) => bbtX > 0, bbtX, bbtY) / (bbtX.reduce((k, l) => {
			  return parseFloat(k) + parseFloat(l)
			},0));

			return ipKelasx.toFixed(2);
		}
		var n= 0;
		function keterangan_nilai_huruf(nilai)
		{
			var keterangan_nilai = $.parseJSON('<?= json_encode($keterangan_nilai) ?>');
			for(n in keterangan_nilai){
				if(nilai >= keterangan_nilai[n].batas_min){
					if(jmlH.hasOwnProperty(keterangan_nilai[n].nilai_huruf)){
						jmlH[keterangan_nilai[n].nilai_huruf].push(1);
					}else{
						jmlH[keterangan_nilai[n].nilai_huruf] = [1];
					}
					
					return keterangan_nilai[n].nilai_huruf;
				}
			}			
		}
		
		$(document).ready(function()
		{
			$('.val-bobot').trigger('blur');
		});
		
		$('#save-rekap-nilai').on('click',function()
		{
			bobot_rekap = [];
			$('.val-bobot').each(function()
			{
				bobot_rekap.push($(this).text().trim());
			});
			$.post('<?= base_role_url('rekap-nilai/save-bobot-rekap-nilai') ?>',{
				bobot_rekap_nilai:{
					'id':$('#id_bobot_rekap').val(),
					tahun_akademik:$('#tahun_akademik').val(),
					semester:$('#semester').val(),
					kode_kuliah:$('#kd_kuliah').val(),
					dosen_uuid:'<?= session()->get('id'); ?>',
					'bobot' : bobot_rekap				
				}
			},function(payload){
				if(payload.rowid != undefined)
				{
					$('#id_bobot_rekap').val(payload.rowid)
					Swal.fire(
					  'Data berhasil di simpan.',
					  '',
					  'success'
					)				
				}
			},'json');	
		});
		
		function limitNumberWithinRange(num, min, max){
		  const MIN = min || 0;
		  const MAX = max || 1;
		  const parsed = parseFloat(num).toFixed(2)
		  return Math.min(Math.max(parsed, MIN), MAX)
		}		
	function inputKategoriNilaiBaru()
	{
		form = '';
		form += '<div class="form-group">';
		form += '	<label for="example-text-input" class="form-control-label">Nama Kategori Nilai</label>';
		form += '	<input required="" value="" class="form-control" type="text" onfocus="focused(this)" onfocusout="defocused(this)">';
		form += '</div>';
		
		Swal.fire({
		  title: 'Input Kategori Nilai Baru',
		  html:form,
		  showCloseButton: true,
		  showCancelButton: false,
		  focusConfirm: false,
		  confirmButtonText:
			'Simpan',
		  cancelButtonText:
			'<i class="fa fa-thumbs-down"></i>',
		  cancelButtonAriaLabel: 'Thumbs down'
		})		
	}
	
	$(function()
	{
		$('#kd_kuliah, #tahun_akademik, #semester').on('change',function(){
			var kd_kuliah = $('#kd_kuliah').val();
			var tahun_akademik = $('#tahun_akademik').val();
			var semester = $('#semester').val();
			var nim_mhs = $('#nim_mhs').val();
			var nama_mhs = $('#nama_mhs').val();	
			
			document.location.href='<?= base_role_url(); ?>/rekap-nilai/'+kd_kuliah+'/'+tahun_akademik+'/'+semester;
		});
	});
	
	</script>
<?= $this->endSection() ?>
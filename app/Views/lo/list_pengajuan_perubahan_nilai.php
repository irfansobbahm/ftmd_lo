<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>


	<div class="row">
		<div class="card">	
			<div class="table-responsive">
			<table class="table align-items-center mb-0">
			  <thead>
				<tr>
				  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">Dosen</th>				
				  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">Status</th>
				  <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">Matakuliah</th>
				  <th></th>
				  <th></th>
				</tr>
			  </thead>			
			  <tbody>
				<tr>
					<td>
						<div class="d-flex px-2 py-1">
						  <div>
							<img src="https://demos.creative-tim.com/soft-ui-design-system-pro/assets/img/team-3.jpg" class="avatar avatar-sm me-3">
						  </div>
						  <div class="d-flex flex-column justify-content-center">
							<h6 class="mb-0 text-xs">Ony Arifianto, Ph.D., Hendarko, S.T., M.Sc.</h6>
							<p class="text-xs text-secondary mb-0">NIP 8888888</p>
						  </div>
						</div>					
					</td>				
					<td><span class="badge bg-warning badge-sm">Pengajuan</span></td>
					<td>Teknik Simulasi Terbang ( AE4025 )</td>
					<td>
						<button onclick="approvePengajuanPerubahanNilai()" class="btn btn-primary btn-xs">Setuju</button>
						<button onclick="rejectPengajuanPerubahanNilai()" class="btn btn-danger btn-xs">Tolak</button>
					</td>
				</tr>
				<tr>
					<td>
						<div class="d-flex px-2 py-1">
						  <div>
							<img src="https://demos.creative-tim.com/soft-ui-design-system-pro/assets/img/team-3.jpg" class="avatar avatar-sm me-3">
						  </div>
						  <div class="d-flex flex-column justify-content-center">
							<h6 class="mb-0 text-xs">Ony Arifianto, Ph.D., Hendarko, S.T., M.Sc.</h6>
							<p class="text-xs text-secondary mb-0">NIP 8888888</p>
						  </div>
						</div>					
					</td>				
					<td><span class="badge bg-success badge-sm">Disetujui</span></td>
					<td>Teknik Simulasi Terbang ( AE4025 )</td>
					<td>
						-
					</td>
				</tr>
				<tr>
					<td>
						<div class="d-flex px-2 py-1">
						  <div>
							<img src="https://demos.creative-tim.com/soft-ui-design-system-pro/assets/img/team-3.jpg" class="avatar avatar-sm me-3">
						  </div>
						  <div class="d-flex flex-column justify-content-center">
							<h6 class="mb-0 text-xs">Ony Arifianto, Ph.D., Hendarko, S.T., M.Sc.</h6>
							<p class="text-xs text-secondary mb-0">NIP 8888888</p>
						  </div>
						</div>					
					</td>				
					<td><span class="badge bg-success badge-sm">Disetujui</span></td>
					<td>Teknik Simulasi Terbang ( AE4025 )</td>
					<td>
						-
					</td>
				</tr>
				<tr>
					<td>
						<div class="d-flex px-2 py-1">
						  <div>
							<img src="https://demos.creative-tim.com/soft-ui-design-system-pro/assets/img/team-3.jpg" class="avatar avatar-sm me-3">
						  </div>
						  <div class="d-flex flex-column justify-content-center">
							<h6 class="mb-0 text-xs">Ony Arifianto, Ph.D., Hendarko, S.T., M.Sc.</h6>
							<p class="text-xs text-secondary mb-0">NIP 8888888</p>
						  </div>
						</div>					
					</td>				
					<td><span class="badge bg-danger badge-sm">Ditolak</span></td>
					<td>Teknik Simulasi Terbang ( AE4025 )</td>
					<td>
						-
					</td>
				</tr>				
							
			  </tbody>
			</table>
		  </div>		
		</div>
	</div>
	<script>
	
	function approvePengajuanPerubahanNilai(){
		Swal.fire({
		  title: 'Konfirmasi',
		  text: "Anda setuju perubahan nilai atas pengajuan perubahan nilai ini ?",
		  icon: 'question',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Ya, Saya Setuju.',
		  cancelButtonText: 'Batal.'
		}).then((result) => {
		  if (result.isConfirmed) {
			Swal.fire(
			  'Berhasil!',
			  'Pengajuan Perubahan Nilai Telah Disetujui.',
			  'success'
			)
		  }
		})		
	}
	
	function rejectPengajuanPerubahanNilai(){
		Swal.fire({
		  title: 'Konfirmasi',
		  text: "Anda menolak pengajuan perubahan nilai ini ?",
		  icon: 'question',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Ya',
		  cancelButtonText: 'Batal.'
		}).then((result) => {
		  if (result.isConfirmed) {
			Swal.fire(
			  'Ditolak',
			  'Pengajuan Perubahan Nilai Ditolak.',
			  'error'
			)
		  }
		})		
	}	
	
	</script>	
<?= $this->endSection() ?>
<?= $this->extend('layout/layout') ?>

<?= $this->section('content') ?>


	<div class="row">
		<div class="card mb-3 p-4">
			<div class="card-header p-0 pb-3">
				<div class="row">
					<div class="col-md-8 d-flex align-items-center">
					<h4 class="mb-0">FTMD - Teknik Dirgantara</h4>
					</div>
				</div>
			</div>	
			<form method="GET" action="">
				<div class="row mb-3">
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">Tahun Akademik </label>
						<select class="form-control form-control-md" id="tahun_akademik">
						<?php foreach($ws['tahunkuliah'] as $tahunkuliah){ ?>
							<option value="<?= $tahunkuliah ?>" <?= (isset($uriSegments[4]) && $uriSegments[4] == $tahunkuliah) ? 'selected':'' ?>><?= $tahunkuliah.'/'.($tahunkuliah +1) ?></option>
						<?php } ?>
						</select>
					
					</div>	
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">Semester</label>
						<?php if(session()->get('role') == 'dosen'){ ?>

							<select class="form-control form-control-md" id="semester">
							<?php foreach(['Ganjil','Genap'] as $sem){ ?>
								<option <?= (date('n')<=6 && strtolower($sem) == strtolower('Genap') ? 'selected':'') ?> value="<?= htmlentities(strtolower($sem)) ?>"><?= $sem ?></option>
							<?php } ?>
							<option>Semester Pendek</option>
							</select>


						<?php } else { ?>
							<select class="form-control form-control-md" id="semester">
								<option value="">Seluruh</option>
								<?php for($n=0;$n<=13;$n++){ ?>
								<option value=""><?= $n + 1 ?></option>
								<?php } ?>
							</select>
						<?php } ?>
						

					
					</div>				
					<div class="col-sm-4 col-6">
						<label class="form-label text-lg">Matakuliah</label>
						<select class="form-control form-control-md"  id="kd_kuliah">
						<?php foreach($ws['matakuliah'] as $kd_mk => $matakuliah){ ?>
							<option value="<?= $kd_mk ?>" <?= (isset($uriSegments[3]) && $uriSegments[3] == $kd_mk) ? 'selected':'' ?>><?= $matakuliah.' (<b>'.$kd_mk.'</b>)' ?></option>
						<?php } ?>
						</select>
					
					</div>					
						
				</div>
			</form>
		</div>
	</div>

	<div class="row mt-3">
		<div class="card pb-3">
			<div class="row mt-2" style="margin-right:10px;margin-left:10px;">
				<div class="card-header col-md-8 p-0 pb-3">
					<div class="row">
						<div class="col-md-10 d-flex align-items-center">
						<h4 class="mb-0"><?= $catNilai; ?> - <b>Matakuliah 
						<?= isset($uriSegments[3]) ? $uriSegments[3] : 'MT3103'?>
						(<?= isset($uriSegments[4]) ? $uriSegments[4].'/'.($uriSegments[4] + 1) : date('Y').'/'.(date('Y') + 1) ?>) </b></h4>
						</div>
					</div>
				</div>		
				<hr class="horizontal dark">
				<div class="text-end">
					<button type="button" id="atur-ulang-pembobotan-lo" class="btn bg-gradient-primary">Atur ulang</button>
					<button type="button" id="simpan-pembobotan-lo" disabled="disabled" class="btn bg-gradient-primary">Simpan</button>
					<!--<button type="button" class="btn bg-gradient-primary">Verifikasi</button>-->
				</div>				
			  <h5>Learning Outcome</h5>
			  <div class="row" style="margin-right:10px;margin-left:10px;">
				
				<?php foreach(range('A','G') as $v){ ?>
					<div class="form-check col-md">
					  <input class="form-check-input lo-checkbox" data-lo="<?= $v ?>" type="radio" value="" name="LO">
					  <label class="custom-control-label" for="customCheck1">LO <?= $v ?></label>
					</div>
				<?php } ?>
			  </div>
			</div>				
		  <div class="table-responsive">
			<input type="hidden" id="id_pembobotan_lo">
			<table id="tbl_pembobotan_lo" class="table table-sm table-bordered table-striped align-items-center mb-0">			
			  <thead>			
				<tr>
				  <td class="col-sm-0 text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">No</td>
				  <td class="col-sm-1 text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">NIM</td>
				  <td class=" text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">Nama</td>

				</tr>
			  </thead>
			  <tbody>
			  <?php foreach($ws['mahasiswa'] as $n => $row){ ?>
				<tr class="rMhs" data-nim="<?= $row->nim; ?>">
				  <td class="text-center"><?= $n + 1 ?></td>
				  <td><?= $row->nim; ?></td>
				  <td class="cNama" style="max-width:400px !important;">
					<div class="d-flex px-2 py-1">
					  <div class="d-flex flex-column justify-content-center">
						<h6 class="mb-0 text-xs"><?= $row->nama; ?></h6>
						
					  </div>
					</div>
				  </td>				  
				</tr>
			  <?php } ?>
			  </tbody>
			</table>
		  </div>
			
		</div>
	</div>
	<script>
	var selectedLo;
	var totalBobot = 0;
	function confirm(){
		Swal.fire({
		  title: 'Konfirmasi',
		  text: "Anda akan melakukan pengajuan perubahan nilai.",
		  icon: 'question',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Saya mengerti, dan Ajukan.',
		  cancelButtonText: 'Batal'
		}).then((result) => {
		  if (result.isConfirmed) {
			Swal.fire(
			  'Berhasil!',
			  'Pengajuan telah berhasil di kirim.',
			  'success'
			)
		  }
		})		
	}
	
	$(function()
	{
		$('#kd_kuliah, #tahun_akademik, #semester').on('change',function(){
			var kd_kuliah = $('#kd_kuliah').val();
			var tahun_akademik = $('#tahun_akademik').val();
			var semester = $('#semester').val();
			var nim_mhs = $('#nim_mhs').val();
			var nama_mhs = $('#nama_mhs').val();	
			
			document.location.href='<?= base_role_url(); ?>/learning-outcome/<?= $catNilaiSlug ?>/'+kd_kuliah+'/'+tahun_akademik+'/'+semester;
		});

		$(document).on('keyup','.val-soal',function(){
			//$(this).parent().find(".val-total-nilai-mhs").text();
			var valSoal = [];
			$(this).parent().find('.val-soal').each(function(){

				if(!isNaN(parseFloat($(this).text()))){
					
					valSoal.push(parseFloat($(this).text()));
				}else{
					valSoal.push(0);
				}
				
			});
			var valBobot = [];
			$('.val-bobot').each(function(){

				if(!isNaN(parseFloat($(this).text()))){
					
					valBobot.push(parseFloat($(this).text()));
				}else{
					valBobot.push(0);
				}
				
			});
			x = valSoal;
			y = valBobot;
			var totalNilaiMhs = SUMPRODUCT((x, y) => x > 0, x, y).toFixed(2);
			$(this).parent().find(".val-total-nilai-mhs").text(totalNilaiMhs);
			var currElIndex = $(this).index();
			var avg = [];
			$('.rMhs').find('td:eq('+currElIndex+')').each(function()
			{
				if(!isNaN(parseFloat($(this).text())))
				{
					avg.push(parseFloat($(this).text()));
				}
			});

			var average = avg.reduce((a, b) => a + b, 0) / avg.length;
			var stdev = DEVIATION(avg);
			
			average = (isNaN(average)) ? 0 : average;
			stdev = (isNaN(stdev)) ? 0 : stdev;
			
			$('#rRataRata').find('td').eq(currElIndex - 1).text(average.toFixed(2));
			$('#rDeviasi').find('td').eq(currElIndex - 1).text(stdev.toFixed(2));
			
		});
		
		$(document).on('blur','.val-bobot',function(evt){
			var maxbobot = 1;
			var curEl = $(this);
			numberStr = curEl.text();
			currVal = limitNumberWithinRange(numberStr, 0, maxbobot);
			
			sVal = 0;
			curEl.siblings().each(function(i){
				if(i > 0){
					sVal = (isNaN(parseFloat($(this).text())) ? 0 : parseFloat($(this).text())) + sVal;
					
				}
			});
			sumBobot = (sVal + currVal);
			$('.sum-bobot').html(sumBobot.toFixed(2));
			if(sumBobot <= maxbobot){
				curEl.html(currVal);
				if(sumBobot < maxbobot)
				{
					$('#simpan-pembobotan-lo').attr('disabled',true);
					$('.sum-bobot').css('background-color','lightpink');
				}else{
					$('#simpan-pembobotan-lo').removeAttr('disabled');
					$('.sum-bobot').removeAttr('style');
				}
			}else{
				
				curEl.html(0);
				val = 0;
				$('.val-bobot').each(function()
				{
					if(!isNaN(parseFloat($(this).text()))){
						val = val + parseFloat($(this).text());
					}
					
				});
				$('.sum-bobot').html(val.toFixed(2));
			}
			
			$('.row-lo-total').each(function(i)
			{
				var loTotal = $(this);
				var loTotalSkala4 = $(this).siblings('.row-lo-total-skala4');
				
				var yy = 0;
				$(this).parent().find('.val-val').each(function(i)
				{
					
					_val = (parseFloat($('.val-bobot').eq(i).text()) * parseFloat($(this).text()));
					
					if(!isNaN(_val)){
						yy = yy + _val;
						loTotal.text(yy.toFixed(2));
						sk4 = ((yy.toFixed(2) / 100).toFixed(2) * 4).toFixed(2);
						loTotalSkala4.text(sk4 < 1 ? (1).toFixed(2) : sk4);
					}
				});
				
			});			
			
			
		});		
	
       function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
             return false;
		  }

          return true;
       }		
		function limitNumberWithinRange(num, min, max){
		  const MIN = min || 0;
		  const MAX = max || 1;
		  const parsed = parseFloat(num).toFixed(2)
		  return Math.min(Math.max(parsed, MIN), MAX)
		}		
		
		$('td').on('keyup',function(){
			if($(this).attr('id') == 'cel-jumlah-soal')
			{
				jmlSoal = parseInt($(this).text());
				$('#cel-jumlah-soal').attr('colspan',jmlSoal);
				
				$('#rata-rata').nextAll('td').remove();
				$('#deviasi').nextAll('td').remove();
				$('#bobot').nextAll('td').remove();
				$('#nilai-total-mhs').nextAll('td').remove();
				$('.val-total-nilai-mhs').nextAll('td').remove();
				
				for(n=0;n < jmlSoal;n++)
				{
					$('#rata-rata').after("<td></td>");
					
					$('#deviasi').after("<td></td>");
					
					$('#bobot').after("<td class='val-bobot' type='number' min='0' max='2.1' step='0.1' contenteditable='true'></td>");
					
					$('#nilai-total-mhs').after("<td class='font-weight-bolder text-primary align-middle text-center text-sm'>Soal "+(jmlSoal - n)+"</td>");
					$('.val-total-nilai-mhs').after("<td class='val-soal text-center' contenteditable='true'></td>");
				}
			}		

			
		});
	});
	
	$('.lo-checkbox').on('change',function()
	{
		if ($(this).is(":checked")) {
			
			selectedLo = $(this).data('lo');
		} 
		
		$('.rem').remove();
	
		$.post('<?= base_role_url('learning-outcome/data-kelompok-nilai') ?>',{
			lo:selectedLo,
			tahun:$('#tahun_akademik').val(),
			semester:$('#semester').val(),
			kd_kuliah:$('#kd_kuliah').val(),
		},function(payload)
		{
			// $('#id_komponen_asesmen').val(payload.komponen_asesmen.id);
			console.log(payload);
			if(!$.isEmptyObject(payload.komponen_rangkuman)){
				//rangkum = merge(payload.komponen_rangkuman,rangkum);
				loakomp = '';
				defkatnil = <?= json_encode(default_kategori_nilai()) ?>;
				col = 0;
				objKatnil = {};
				
				if($.isEmptyObject(payload.komponen_rangkuman[selectedLo])){
					$('#simpan-pembobotan-lo').attr('disabled',true);
				}
				
				for(katnil in payload.komponen_rangkuman[selectedLo])
				{
					
					for(i in payload.komponen_rangkuman[selectedLo][katnil]){
						loakomp += '<td class="rem text-center text-uppercase text-primary text-sm font-weight-bolder opacity-7">'+katnil.toUpperCase()+' <br>('+defkatnil[katnil.toUpperCase()]+' '+payload.komponen_rangkuman[selectedLo][katnil][i]+')</td>';
						objKatnil[katnil.toUpperCase()] = payload.komponen_rangkuman[selectedLo][katnil];
						col++;
					}
				}
				loakomp += '<td class="rem text-center text-uppercase text-primary text-sm font-weight-bolder opacity-7" colspan="2">LO '+selectedLo+'</td>';
				
				lohead = '<tr class="rem text-uppercase text-primary text-sm font-weight-bolder opacity-7">';
				lohead += '<td colspan="3" rowspan="2" class="rem dt text-end col-2">Bobot(0-1)</td>';
				
				for(_katnil in objKatnil){
					for(s in objKatnil[_katnil])
					{
						lohead += '<td rowspan="2" contenteditable data-kategori_nilai="'+_katnil.toLowerCase()+'" data-idx_kategori_nilai="'+objKatnil[_katnil][s]+'" class="val-bobot"></td>';
					}
				}
				lohead += '<td class=" text-center text-uppercase text-primary text-sm font-weight-bolder opacity-7" colspan="2">Total Bobot</td>';
				lohead += '</tr>';
				lohead += '<tr class="rem text-uppercase text-primary text-sm font-weight-bolder opacity-7">';
				lohead += '<td class="sum-bobot" colspan="2">'+totalBobot+'</td>';
				lohead += '</tr>';				
				// if(selectedLo == 'A'){
					// loakomp = '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UAS (Soal 1)</td>';
					// loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UAS (Soal 3)</td>';
					// loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UTS (Soal 2)</td>';
					// loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">Kuis (Kuis 3)</td>';
					// loakomp += '<td class="rem text-center text-uppercase text-primary text-sm font-weight-bolder opacity-7">LO '+selectedLo+'</td>';
				// }
				
				// if(selectedLo == 'B'){
					// loakomp = '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UAS (Soal 1)</td>';
					// loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UAS (Soal 2)</td>';
					// loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UAS (Soal 4)</td>';
					// loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">Kuis (Kuis 3)</td>';
					// loakomp += '<td class="rem text-center text-uppercase text-primary text-sm font-weight-bolder opacity-7">LO '+selectedLo+'</td>';
				// }
				
				// if(selectedLo == 'C'){
					// loakomp = '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UTS (Soal 2)</td>';
					// loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UTS (Soal 3)</td>';
					// loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UTS (Soal 4)</td>';
					// loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">Kuis (Kuis 5)</td>';
					// loakomp += '<td class="rem text-center text-uppercase text-primary text-sm font-weight-bolder opacity-7">LO '+selectedLo+'</td>';
				// }
				
				// if(selectedLo == 'D'){
					// loakomp = '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UAS (Soal 2)</td>';
					// loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UAS (Soal 3)</td>';
					// loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">UTS (Soal 1)</td>';
					// loakomp += '<td class="rem text-left text-uppercase text-primary text-sm font-weight-bolder opacity-7">Kuis (Kuis 1)</td>';
					// loakomp += '<td class="rem text-center text-uppercase text-primary text-sm font-weight-bolder opacity-7">LO '+selectedLo+'</td>';
				// }		
				$('#tbl_pembobotan_lo thead').prepend(lohead);
				
				$('#tbl_pembobotan_lo  thead tr:eq(2)').append(loakomp);
				for(_katnil in objKatnil){
					for(s in objKatnil[_katnil])
					{					
						$('.rMhs').append('<td class="rem val-val font-weight-bolder" data-kategori_nilai="'+_katnil+'" data-idx_kategori_nilai="'+objKatnil[_katnil][s]+'"></td>');
					}
				}
				$('.rMhs').each(function()
				{
					$(this).find('td[class*=val-val]').each(function(){
					nim = $(this).parent().data('nim');
					
					kategori_nilai = $(this).data('kategori_nilai');
					idx = $(this).data('idx_kategori_nilai');
					nilai = $.parseJSON(payload.kelompok_nilai[kategori_nilai].kelompok_nilai_mhs[nim].nilai);
					$(this).text(nilai[(idx - 1)]);
					//$(this).append('<td class="rem val-val" data-kategori_nilai="'+_katnil+'" data-idx_kategori_nilai="'+s+'">'+payload.kelompok_nilai[_katnil].kelompok_nilai_mhs[nim].nilai[s]+'</td>');
					});
				});					
				$('.rMhs').append('<td data-id="" class="rem row-lo-total font-weight-bolder"></td><td class="rem row-lo-total-skala4 font-weight-bolder"></td>');
				// $('.val-val').each(function()
				// {
					// $(this).text((randomNumber(50,100)));
				// });
				
				$('.rMhs').each(function(){
					$(this).find('.cNama').nextUntil( ".row-lo-total" ).each(function(){
						cellnum = parseInt($(this).text());
						
					});
				});				
				
			}
			if(!$.isEmptyObject(payload.pembobotan_lo))
			{
				//$('#simpan-pembobotan-lo').removeAttr('disabled');
				$('#id_pembobotan_lo').val(payload.pembobotan_lo.id);
				
				$('.val-bobot').each(function(i)
				{	
					bobot = $.parseJSON(payload.pembobotan_lo.bobot);
					a = $(this).data('kategori_nilai')+';'+$(this).data('idx_kategori_nilai');
					$(this).text(bobot[a]);
					$(this).trigger('blur');
					

				});
				//$('.val-bobot')
				$('.rMhs').each(function(i)
				{
					rNim = $(this).data('nim');
					//console.log(payload.pembobotan_lo.nilaimhs[rNim].id);
					if(payload.pembobotan_lo !== null){
						$(this).find('.row-lo-total').attr('data-id', payload.pembobotan_lo.nilaimhs[rNim].id);
					}
				});				
			}else{
				$('#id_pembobotan_lo').val('');
				//$('#simpan-pembobotan-lo').attr('disabled',true);
			}

		},'json');	

	});	
	function randomNumber(min, max) {
		min = Math.ceil(min);
		max = Math.floor(max);
		
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}	
	
	$('#simpan-pembobotan-lo').on('click', function()
	{
		bobot ={};
		$('.val-bobot').each(function(){
			//bobot[$(this).data('kategori_nilai')+'-'+$(this).data('idx_kategori_nilai')]
			bobot[$(this).data('kategori_nilai')+';'+$(this).data('idx_kategori_nilai')] = ($(this).text()).trim();
		});
		
		nilaimhs = {};
		$('.rMhs').each(function(i)
		{
			nilaimhs[$(this).data('nim')] = {
				id:$(this).find('.row-lo-total').data('id'),
				value:$(this).find('.row-lo-total').text()
			};
		});

		$.post('<?= base_role_url('learning-outcome/save-pembobotan-lo') ?>',{
			pembobotan_lo:{
				'lo':selectedLo,
				'bobot': JSON.stringify(bobot),
				'id':$('#id_pembobotan_lo').val(),
				tahun_akademik:$('#tahun_akademik').val(),
				semester:$('#semester').val(),
				kode_kuliah:$('#kd_kuliah').val(),
				dosen_uuid:'<?= session()->get('id'); ?>',
				'nilaimhs' : nilaimhs				
			}
		},function(payload){
			$('.lo-checkbox[data-lo='+selectedLo+']').trigger('change');
			if(payload.rowid != undefined)
			{
				Swal.fire(
				  'Data berhasil di simpan.',
				  '',
				  'success'
				)				
			}			
			
		},'json');
	});
	
	$('#atur-ulang-pembobotan-lo').on('click',function()
	{
		$('.val-bobot').each(function(i)
		{
			$(this).text('');
			$(this).trigger('blur');
		});
	});
	</script>
<?= $this->endSection() ?>
<?php

namespace App\Models;

use CodeIgniter\Model;
use Ramsey\Uuid\Uuid;

class TicketModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'tickets';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = false;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['service_id','identifier','subject','description','status','progress','owner','agent'];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
	protected $updatedField  = 'updated_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = ['preInsert'];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];
	public $totalRows = 0;
	
	protected function preInsert(array $data)
	{
		$data['data'][$this->primaryKey] = Uuid::uuid4()->toString();
		$data['data']['identifier'] = 'T'.date('YmdHis');

		return $data;		
	}	
	
	public function countAllTicket($username, $role)
	{
        return [
			'open' => $this->count_agentTickets('', $role, 'open'),
			'in-progress' => $this->count_agentTickets($username,$role, 'inprogress'),
			'pending' => $this->count_agentTickets($username, $role, 'pending'),
			'closed' => $this->count_agentTickets($username, $role, 'closed'),
			'my-tickets' => $this->count_userTickets($username),
			'all-ticket' => $this->count_allTicket()
		];
	}	
	public function allTicket($filter = [])
	{
		$this->select($this->table.".*,u.name,u.email,u.username,ag.name agent_name,ag.email agent_email,ag.username agent_username,r.name role_name,s.title as service_title,s.service_model,s.slug service_slug");
		$this->join('users u','u.username = '.$this->table.'.owner', 'left');
		$this->join('users ag','ag.username = '.$this->table.'.agent', 'left');
		$this->join('roles r','r.identifier = ag.role', 'left');
		$this->join('services s','s.id = '.$this->table.'.service_id');
		
		if(!empty($filter)){
			$nfilter = [];
			foreach($filter as $k => $v)
			{
				if(!empty($v))
				{
					if($k == 'identifier'){
						$nfilter[$this->table.'.identifier'] = $v;
					}
					if($k == 'services'){
						$nfilter[$this->table.'.service_id'] = $v;
					}
					if($k == 'status'){
						$nfilter[$this->table.'.status'] = $v;
					}
					if($k == 'agent'){
						$nfilter['ag.name'] = $v;
					}
					if($k == 'owner'){
						$nfilter['u.name'] = $v;
					}	
				}				
			}

			$this->like($nfilter);
		}
		$this->orderBy($this->table.'.created_at','DESC');
		$result = $this->findAll();
				
		$this->totalRows = $this->countAllResults();
		
		return $result;
	}	
	
	public function count_allTicket()
	{
		$this->select("COUNT(*) total");
		$result = $this->first();
		
		return $result['total'];
	}	

	public function count_allTicketToday()
	{
		$this->select("COUNT(*) total");
		$this->where('DATE(created_at)','DATE(NOW())',false);
		$result = $this->first();

		return $result['total'];
	}

	public function count_allTicketSinceYesterday()
	{
		$this->select("COUNT(*) total");
		$this->where('DATE(created_at) >','DATE(NOW()) - 1',false);
		$this->where('status','open');
		$result = $this->first();

		return $result['total'];
	}	
	public function count_allTicketSinceLastWeek()
	{
		$this->select("COUNT(*) total");
		$this->where('DATE(created_at) >=','(DATE(created_at) - INTERVAL DAYOFWEEK(DATE(NOW()))+6 DAY)',false);
		$this->where('DATE(created_at) <','(DATE(created_at) - INTERVAL DAYOFWEEK(DATE(NOW()))-1 DAY)',false);
		$result = $this->first();

		return $result['total'];
	}	
	public function userTickets($filter=[], $username='', $status = '')
	{
		
		$this->select($this->table.".*,u.name,u.email,u.username,ag.name agent_name,ag.email agent_email,ag.username agent_username,r.name role_name,s.title as service_title,s.service_model,s.slug service_slug");
		$this->join('users u','u.username = '.$this->table.'.owner', 'left');
		$this->join('users ag','ag.username = '.$this->table.'.agent', 'left');
		$this->join('roles r','r.identifier = ag.role', 'left');
		$this->join('services s','s.id = '.$this->table.'.service_id');
		$this->where($this->table.'.owner', $username);	
		if(!empty($status))
		{
			$this->where($this->table.'.status', $status);	
		}
		if(!empty($filter)){
			$nfilter = [];
			foreach($filter as $k => $v)
			{
				if(!empty($v))
				{
					if($k == 'identifier'){
						$nfilter[$this->table.'.identifier'] = $v;
					}
					if($k == 'services'){
						$nfilter[$this->table.'.service_id'] = $v;
					}
					if($k == 'status'){
						$nfilter[$this->table.'.status'] = $v;
					}
					if($k == 'agent'){
						$nfilter['ag.name'] = $v;
					}
					if($k == 'owner'){
						$nfilter['u.name'] = $v;
					}	
				}				
			}

			$this->like($nfilter);
		}		
		$this->orderBy($this->table.'.created_at','DESC');
		$result = $this->findAll();

		$this->totalRows = $this->countAllResults();
		
		return $result;
	}
	public function count_userTickets($username='', $status = '')
	{
		$this->select("COUNT(*) total");
		$this->where($this->table.'.owner', $username);	
		if(!empty($status)){
			$this->where($this->table.'.status', $status);	
		}
		$result = $this->first();
		
		return $result['total'];
	}
	
	public function agentTickets($username='', $role = 'agent', $status = '', $filter=[])
	{
		$result = null;
		$this->select($this->table.".*,u.name,u.email,u.username,ag.name agent_name,ag.email agent_email,ag.username agent_username,r.name role_name,s.title as service_title,s.service_model,s.slug service_slug");
		$this->join('users u','u.username = '.$this->table.'.owner', 'left');
		$this->join('users ag','ag.username = '.$this->table.'.agent', 'left');
		$this->join('roles r','r.identifier = ag.role', 'left');
		$this->join('services s','s.id = '.$this->table.'.service_id');
		
		if(in_array($role,['agent','manager']))
		{
			if($role == 'agent'){
				if(!empty($username)){
					$this->where($this->table.'.agent', $username);
				}
			}
			if(!empty($status)){
				$this->where($this->table.'.status', $status);	
			}			
			if(!empty($filter)){
				$nfilter = [];
				foreach($filter as $k => $v)
				{
					if(!empty($v))
					{
						if($k == 'identifier'){
							$nfilter[$this->table.'.identifier'] = $v;
						}
						if($k == 'services'){
							$nfilter[$this->table.'.service_id'] = $v;
						}
						if($k == 'status'){
							//$nfilter[$this->table.'.status'] = $v;
						}
						if($k == 'agent'){
							$nfilter['ag.name'] = $v;
						}
						if($k == 'owner'){
							$nfilter['u.name'] = $v;
						}	
					}				
				}

				$this->like($nfilter);
			}		
			$this->orderBy($this->table.'.created_at','DESC');
			$result = $this->findAll();
			
			//exit($this->getLastQuery());
			
			$this->totalRows = $this->countAllResults();
		}
		return $result;
	}
	public function count_agentTickets($username='', $role = 'agent', $status = '')
	{
		$result = null;
		$this->select("COUNT(*) total");
		if(in_array($role,['agent','manager']))
		{
			if($role == 'agent'){
				if(!empty($username)){
					$this->where($this->table.'.agent', $username);
				}
			}
			if(!empty($status)){
				$this->where($this->table.'.status', $status);	
			}			
			
			$result = $this->first();
		}
		$result['total'] = (!isset($result['total'])) ? '0':$result['total'];
		return $result['total'];
	}
	
	public function ticketDetail($identifier, $username)
	{
		//exit(Uuid::uuid4()->toString());
		$this->select($this->table.'.*,u.name,u.email,u.username,u.name full_name,
			ag.name agent_name,ag.email agent_email,ag.username agent_username,
			r.name role_name,
			s.title service_name,s.service_model,s.slug service_slug, s.service_table, s.service_model, s.service_form
		');
		$this->join('users u','u.username = '.$this->table.'.owner');
		$this->join('users ag','ag.username = '.$this->table.'.agent', 'left');
		$this->join('roles r','r.identifier = ag.role', 'left');		
		$this->join('services s','s.id = '.$this->table.'.service_id');
		$this->where($this->table.'.identifier', $identifier);
		$ticket = $this->first();
		
		if(!empty($ticket['service_table']))
		{
			$ticket['service'] = model($ticket['service_model'])->getTicketData($ticket['identifier']);
		}
		
		//$ticket['actions'] = model('TicketActionsModel')->
		
		
		return $ticket;
	}
	
	public function ticketLock($identifier, $username)
	{
		$this->transStart();
		$this->set('agent', $username);
		$this->set('status', 'inprogress');
		$this->where('identifier', $identifier);
		$this->update();	
		$this->transComplete();
		
		return $this->transStatus();
	}	
	
	public function ticketUnlock($identifier, $username)
	{
		$this->transStart();
		$this->set('agent', '');
		$this->set('status', 'open');
		$this->where('identifier', $identifier);
		$this->update();	
		$this->transComplete();
		
		return $this->transStatus();
	}	
	
	public function saveTicketAction($identifier, array $data){
		
		foreach($data as $col => $val){
			$this->set($col, $val);
		}
		$this->where('identifier', $identifier);
		$this->update();			
	}
	
	public function hasTicketOwner($ticketIdentifier, $currUserName)
	{
		$this->where('identifier', $ticketIdentifier);
		$this->where('owner', $currUserName);
		
		return $this->countAllResults();
	}
	
	public function hasAgentInCharge($ticketIdentifier, $currUserName)
	{
		$this->where('identifier', $ticketIdentifier);
		$this->where('agent', $currUserName);
		
		return $this->countAllResults();
	}	
}

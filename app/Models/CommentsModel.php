<?php

namespace App\Models;

use CodeIgniter\Model;
use Ramsey\Uuid\Uuid;

class CommentsModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'ticket_comments';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['ticket_identifier','comment','user','hidden'];

    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

	public function getTicketComments($identifier = '', $hidden = 0){
		$this->where('ticket_identifier',$identifier);
		if($hidden == 0){
			$this->where('hidden', $hidden);
		}
		$this->join('users u','u.username = '.$this->table.'.user');
		$this->orderBy($this->table.'.created_at', 'ASC');
		
		return $this->findAll();
	}
}

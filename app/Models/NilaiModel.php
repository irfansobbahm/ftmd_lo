<?php

namespace App\Models;

use CodeIgniter\Database\RawSql;
use CodeIgniter\Model;
use Ramsey\Uuid\Uuid;

class NilaiModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'kelompok_nilai';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['kategori_nilai','tahun_akademik','semester','kode_kuliah','dosen_uuid','jumlah_soal','rata_rata','deviasi','bobot'];

    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

	public function save_nilai($data)
	{
		$this->save($data);
		
		return $this->insertID();
	}
	
	public function getRow($filter)
	{
		foreach($filter as $col => $val){
			$this->where($col, $val);
		}
		return $this->first();
	}
	
	public function getAll($filter)
	{
		foreach($filter as $col => $val){
			$this->where($col, $val);
		}
		return $this->findAll();
	}
	
	public function getMhsNilai($filter)
	{
		$nilai = [];
		$NilaiMhsModel = model('NilaiMhsModel');
		$this->select('id,kategori_nilai');
		foreach($filter as $col => $val){
			if($col != 'nim'){
				$this->where($col, $val);
			}
		}
		$allKategoriNilai = $this->findAll();

		foreach($allKategoriNilai as $kategori)
		{
			$NilaiMhsModel->getRow([
				'id_kelompok_nilai' => $kategori->id,
				'nim' => $filter['nim']
			]);
			$nilai[$kategori->kategori_nilai] = @$NilaiMhsModel->getRow([
				'id_kelompok_nilai' => $kategori->id,
				'nim' => $filter['nim']
			])->nilai;
		}
		
		return $nilai;
	}
	
	public function getMhsNilaiLO($filter)
	{
		$nilai = [];
		$nilaiTotal = [];
		$nilaiLo = [];
		$NilaiMhsModel = model('NilaiMhsModel');
		$PembobotanLoModel = model('PembobotanLoModel');
		$this->select('id,kategori_nilai');
		foreach($filter as $col => $val){
			if($col != 'nim'){
				$this->where($col, $val);
			}
		}
		$allKategoriNilai = $this->findAll();

		foreach($allKategoriNilai as $kategori)
		{
			$nilai[$kategori->kategori_nilai] = json_decode(@$NilaiMhsModel->getRow([
				'id_kelompok_nilai' => $kategori->id,
				'nim' => $filter['nim']
			])->nilai);

			$nilaiTotal[$kategori->kategori_nilai] = @$NilaiMhsModel->getRow([
				'id_kelompok_nilai' => $kategori->id,
				'nim' => $filter['nim']
			])->total;			

			$pembobotanLo = $PembobotanLoModel->getAll([
				'tahun_akademik' => $filter['tahun_akademik'],
				'kode_kuliah' => $filter['kode_kuliah'],
				'semester' => $filter['semester'],
			]);
			
			foreach($nilai[$kategori->kategori_nilai] as $i => $val){
				foreach($pembobotanLo as $nlo)
				{
					$_ = strtolower($kategori->kategori_nilai).';'.($i+1);
					$bobot = json_decode($nlo->bobot);
					if (isset($bobot->{$_})) {
						$loNilai = (float) $bobot->{$_} * (float) $val;
						$nilaiLo[$nlo->lo][] = $loNilai;
					}
				}
				
				
			}
			

		}		
		foreach($nilaiLo as $i => $n)
		{
			$nilaiLo[$i] = number_format((float) array_sum($nilaiLo[$i]), 2, '.', '');
		}
		return ['nilai' => $nilai, 'nilaiTotal' => $nilaiTotal, 'nilaiLo' => $nilaiLo];
	}
}

<?php

namespace App\Models\App;

use CodeIgniter\Model;
use Ramsey\Uuid\Uuid;

class TicketLogsModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'ticket_logs';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = false;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['ticket_identifier','action','actor','actor_role'];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = ['preInsert'];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

	protected function preInsert(array $data)
	{
		$data['data'][$this->primaryKey] = Uuid::uuid4()->toString();

		return $data;		
	}

	public function ticketLogs($ticket_identifier)
	{
		$this->select($this->table.'.*,u.name actor_name, la.title, la.message_format, la.icon_class');
		$this->where('ticket_identifier', $ticket_identifier);
		$this->join('users u','u.username = '.$this->table.'.actor');
		$this->join('ticket_actions la','la.identifier = '.$this->table.'.action');
		$this->orderBy($this->table.'.log_date','ASC');
		
		return $this->findAll();
	}
}

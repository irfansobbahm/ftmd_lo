<?php

namespace App\Models;

use CodeIgniter\Database\RawSql;
use CodeIgniter\Model;
use Ramsey\Uuid\Uuid;

class KomponenAsesmenRangkumanModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'komponen_asesmen_rangkuman';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['LO','kategori_nilai','urutan_satuan','id_komponen_asesmen'];

    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];
	
	public function getRow($filter)
	{
		foreach($filter as $col => $val){
			$this->where($col, $val);
		}
		return $this->first();
	}
	
	public function getAll($filter)
	{
		foreach($filter as $col => $val){
			$this->where($col, $val);
		}
		return $this->findAll();
	}
	
	public function getAllKomponenAsesmenRangkuman($filter, $pembobotan_lo = false)
	{
		foreach($filter as $col => $val){
			$this->where($col, $val);
		}
		$data = [];
		$this->orderBy('urutan_satuan','ASC');
		$findAll = $this->findAll();
		if($this->countAllResults() > 0)
		{
			foreach(get_lo() as $lo)
			{
				$data[$lo] = new \stdClass();
			}
			foreach($findAll as $rangkm)
			{
				$data[$rangkm->LO]->{strtolower($rangkm->kategori_nilai)}['id-'.$rangkm->id] = $rangkm->urutan_satuan;
				if($pembobotan_lo)
				{
					$KomponenAsesmenModel = model('KomponenAsesmenModel');
					$PembobotanLoModel = model('PembobotanLoModel');
					
					$komponen_asesmen = $KomponenAsesmenModel->getRow([
						'id' => $filter['id_komponen_asesmen']
					]);
					$data[$rangkm->LO]->bobot = $PembobotanLoModel->getRow([
						'lo' => $rangkm->LO,
						'kode_kuliah' => $komponen_asesmen->kode_kuliah,
						'dosen_uuid' => $komponen_asesmen->dosen_uuid,
						'tahun_akademik' => $komponen_asesmen->tahun_akademik,
						'semester' => $komponen_asesmen->semester
					]);
				}
			}
		}
		return $data;
	}
	
	public function hasilAsesmen($filter)
	{
		$NilaiModel = model('NilaiModel');
		$PembobotanLoModel = model('PembobotanLoModel');
		$KomponenAsesmenModel = model('KomponenAsesmenModel');
		
		$dataKomponenAsesmen = $KomponenAsesmenModel->getRow($filter);	
		$komponenAsesmenRows = $this->getAll(['id_komponen_asesmen'=> @$dataKomponenAsesmen->id]);
	
		foreach($komponenAsesmenRows as $komponen)
		{
			$data[$komponen->LO][$komponen->kategori_nilai][] = $komponen->urutan_satuan; 
		}

		// echo "<pre>";
		// print_r($data);
				
		// exit;
	}
}

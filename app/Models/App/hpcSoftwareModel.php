<?php

namespace App\Models\App;

use CodeIgniter\Model;

class hpcSoftwareModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'app_software_hpc';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['os_id','software','license'];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];
	
	public function getSoftware($os_id = 0)
	{
		$this->select($this->table.'.*, os.os');
		$this->join('app_os_hpc os','os.id = '.$this->table.'.os_id');
		if(isset($os_id) && !empty($os_id)){
			$this->where('os_id', $os_id);
		}
		
		return $this->findAll();
	}
	
	public function getRow($id)
	{
		return $this->find($id);
	}
	public function hasNewData(array $data)
	{
		if(!isset($data['id']) && empty($data['id'])){
			$this->set($data);
			$this->insert();
			$data['id'] = $this->insertID();
		}
	}
	
	public function saveData(array $data)
	{
		$this->save($data);
	}
	
	public function deleteData($id)
	{
		$this->where('id', $id)->delete();
	}	
}

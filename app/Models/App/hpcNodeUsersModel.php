<?php

namespace App\Models\App;

use CodeIgniter\Model;

class hpcNodeUsersModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'app_hpc_node_users';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['node_id','user','password','status'];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
	protected $updatedField  = 'updated_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];
	
	public function getUsersByNodeID($nodeId, $userid ='')
	{
		$this->where('node_id', $nodeId);
		if(!empty($userid))
		{
			$this->where('user', $userid);
		}
		return $this->findAll();
	}
}

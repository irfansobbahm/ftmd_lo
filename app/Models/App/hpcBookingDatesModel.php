<?php

namespace App\Models\App;

use CodeIgniter\Model;

class hpcBookingDatesModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'app_hpc_booking_dates';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['booking_hpc_id','ticket_identifier','date'];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];
	
	public function getAllDatesBooked($package_id)
	{
		$this->select('date,b.package_id');
		$this->where('date >=','NOW()');
		$this->join('app_booking_hpc b','b.ticket_identifier==ticket_identifier');
		
		return $this->findAll();
	}
	
	public function getBookedDateDetail($date = ''){
		$this->select($this->table.'.date,os.os,nd.ip,pkg.title');
		$this->where('date', $date);
		$this->join('app_booking_hpc b','b.ticket_identifier = '.$this->table.'.ticket_identifier AND b.booking_status = "approved"');
		$this->join('app_os_hpc os','os.id = b.os_id');
		$this->join('app_hpc_nodes nd','nd.id = b.node_id AND os.id = nd.cat_os_id');
		$this->join('app_package_hpc pkg','pkg.id = b.package_id');
		$this->orderBy('date,os.os,nd.ip');
		
		return $this->findAll();
	}
	
	// public function getBookedCalendar(){
		// $this->select($this->table.'.date start,'.$this->table.'.date end,b.ticket_identifier,os.os,nd.ip,pkg.title,pkg.id title_id');
		// $this->join('app_booking_hpc b','b.ticket_identifier = '.$this->table.'.ticket_identifier AND b.booking_status = "approved"');
		// $this->join('app_os_hpc os','os.id = b.os_id');
		// $this->join('app_hpc_nodes nd','nd.id = b.node_id AND os.id = nd.cat_os_id');
		// $this->join('app_package_hpc pkg','pkg.id = b.package_id');
		// $this->orderBy('b.ticket_identifier,date,os.os,nd.ip');
		// $result = $this->findAll();
		
		
		// return $result;
		// $n = [];
		// foreach($result as $row){
			// $n[$row['ticket_identifier']][] = $row['start'];
		// }
		// $x = [];
		// $c = [];
		// $idflist = array_keys($n);
		// foreach($idflist as $key)
		// {
			// if(check_continuous_dates($n[$key], $c))
			// {
				// $x[] = [ 
					// 'start' => $n[$key][0], 
					// 'end'=>$n[$key][count($n[$key]) - 1]
				// ];
			// }else{
				// $x[] = [ 
					// 'start' => $n[$key][0],
					// 'end'=> $n[$key][0]
				// ];
			// }
		// }
		// echo "<pre>";
		// print_r($c);
		// print_r($n);
		// print_r($x);
		// return "test";
	// }	
	
	public function getBookedCalendar(){
		$this->select($this->table.'.date start,'.$this->table.'.date end,b.ticket_identifier,os.os,nd.ip,pkg.title,pkg.id title_id');
		$this->join('app_booking_hpc b','b.ticket_identifier = '.$this->table.'.ticket_identifier AND b.booking_status = "approved"');
		$this->join('app_os_hpc os','os.id = b.os_id');
		$this->join('app_hpc_nodes nd','nd.id = b.node_id AND os.id = nd.cat_os_id');
		$this->join('app_package_hpc pkg','pkg.id = b.package_id');
		$this->orderBy('b.ticket_identifier,date,os.os,nd.ip');
		$result = $this->findAll();
			
		return $result;
	}		
}

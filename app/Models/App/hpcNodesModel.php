<?php

namespace App\Models\App;

use CodeIgniter\Model;

class hpcNodesModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'app_hpc_nodes';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = false;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['cat_os_id','ip','cores','ram','storage'];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];
	
	public function getByOSCat($cat_os_id)
	{
		
		$this->where('cat_os_id', $cat_os_id);
		$results = $this->findAll();
		$countResults = $this->countAllResults();
		if($countResults > 0){
			foreach($results as $i => $row)
			{
				$results[$i]['users'] = model('App\Models\App\hpcNodeUsersModel')->getUsersByNodeID($row['id']);
			}
		}	

		return $results;
	}
	
	public function getAllNodes()
	{
		$this->select($this->table.'.*,os.os');
		$this->join('app_os_hpc os','os.id = cat_os_id');
		$allNodes = $this->findAll();
		return [
			'numrows' => $this->countAllResults(), 
			'data' => $allNodes
		];
	}
	
	public function getNodesOS($cat_os_id = '')
	{
		$this->select($this->table.'.*,os.os');
		$this->join('app_os_hpc os','os.id = cat_os_id');
		if(!empty($cat_os_id)){
			$this->where($this->table.'.cat_os_id',$cat_os_id);
		}
		$allNodes = $this->findAll();
		return [
			'numrows' => $this->countAllResults(), 
			'data' => $allNodes
		];
	}	
	
	public function updateNode($where, $data)
	{
		foreach($data as $key => $val){
			$this->set($key, $val);
		}
		$this->where($where[0], $where[1]);
		return $this->update();		
	}
}

<?php

namespace App\Models\App;

use CodeIgniter\Model;

class hpcOSModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'app_os_hpc';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = false;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['os','versi'];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];
	
	public function getOSandSoftware()
	{
		$hpcSoftwareModel = model('App\Models\App\hpcSoftwareModel');
		$osList = $this->findAll();
		foreach($osList as $i => $os){
			$osList[$i]['softwares'] = $hpcSoftwareModel->where('os_id', $os['id'])->findAll();
		}
		
		return $osList;
	}
}

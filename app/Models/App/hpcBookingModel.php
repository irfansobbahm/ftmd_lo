<?php

namespace App\Models\App;

use CodeIgniter\Model;
use Ramsey\Uuid\Uuid;

class hpcBookingModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'app_booking_hpc';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = false;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['ticket_identifier','username','node_pass','name','nim_nik','tlp','email','purpose_id','doc_id','package_id','os_id','node_id','node_user_id','booking_dates','booking_start_date','booking_end_date','booking_status'];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = ['preInsert'];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];
	
	protected function preInsert(array $data)
	{
		$data['data'][$this->primaryKey] = Uuid::uuid4()->toString();
		$data['data']['doc_id'] = Uuid::uuid4()->toString();
		$data['data']['username'] = session()->get('username');

		return $data;		
	}	
	
	public function getTicketData($identifier)
	{
		$this->select($this->table.'.*,t.created_at, pg.id package_id,pg.title package_title, pg.cores package_cores, pg.ram package_ram, pg.storage package_storage,os.os,ps.title purpose,nu.id os_user_id, nu.user os_user,nu.password  os_password,nd.id node_id,nd.ip node_ip');
		$this->where($this->table.'.ticket_identifier', $identifier);
		//$this->where($this->table.'.booking_status', 'approved');
		$this->join('tickets t','t.identifier = "'.$identifier.'"');
		$this->join('app_package_hpc pg','pg.id = '.$this->table.'.package_id');
		$this->join('app_os_hpc os','os.id = '.$this->table.'.os_id');
		$this->join('app_hpc_booking_purpose ps','ps.id = '.$this->table.'.purpose_id');
		$this->join('app_hpc_node_users nu','nu.id = '.$this->table.'.node_user_id','left');
		$this->join('app_hpc_nodes nd','nd.id = nu.node_id','left');
		
		$result = $this->first();
		// echo $this->getLastQuery();
// print_r($result);
// exit;
		$result['booking_dates'] = model('App\Models\App\hpcBookingDatesModel')->where('booking_hpc_id', $result['id'])->orderBy('date','asc')->findAll();

		return $result;
	}
	
	public function getAllUserBookingHPC($username = '')
	{
		$this->select($this->table.'.*,t.created_at, pg.id package_id,pg.title package_title, pg.cores package_cores, pg.ram package_ram, pg.storage package_storage,os.os,ps.title purpose,nu.id os_user_id, nu.user os_user,nu.password  os_password,nd.id node_id,nd.ip node_ip');
		$this->where($this->table.'.username', $username);
		$this->where($this->table.'.booking_status', 'approved');
		$this->join('tickets t','t.identifier = '.$this->table.'.ticket_identifier');
		$this->join('app_package_hpc pg','pg.id = '.$this->table.'.package_id');
		$this->join('app_os_hpc os','os.id = '.$this->table.'.os_id');
		$this->join('app_hpc_booking_purpose ps','ps.id = '.$this->table.'.purpose_id');
		$this->join('app_hpc_node_users nu','nu.id = '.$this->table.'.node_user_id','left');
		$this->join('app_hpc_nodes nd','nd.id = nu.node_id','left');
		//$this->groupBy('nd.ip');
		$this->orderBy('t.created_at','desc');
		
		$result = $this->findAll();
		
		//$result['booking_dates'] = model('App\Models\App\hpcBookingDatesModel')->where('booking_hpc_id', $result['id'])->orderBy('date','asc')->findAll();
		
		return $result;		
	}
	
	public function getBookingDates($id)
	{
		return model('App\Models\App\hpcBookingDatesModel')->where('booking_hpc_id', $id)->orderBy('date','asc')->findAll();
	}
	
	public function getBookedCalendar()
	{
		$this->select($this->table.'.booking_start_date start,DATE('.$this->table.'.booking_end_date) + 1 as end,'.$this->table.'.ticket_identifier,os.os,nd.ip,pkg.title,pkg.id title_id');
		$this->join('app_os_hpc os','os.id = '.$this->table.'.os_id');
		$this->join('app_hpc_nodes nd','nd.id = '.$this->table.'.node_id AND os.id = nd.cat_os_id');
		$this->join('app_package_hpc pkg','pkg.id = '.$this->table.'.package_id');
		
		return $this->where('booking_status', 'approved')->findAll();
	}
	
	public function saveNewNodeUser(array $data)
	{
		$hpcNodeUsersModel = model('App\Models\App\hpcNodeUsersModel');
		
		$hpcNodeUsersModel->save($data);
		
		return $hpcNodeUsersModel->insertID;		
	}
	public function updateNodeUser($pk = 0, array $data)
	{
		$hpcNodeUsersModel = model('App\Models\App\hpcNodeUsersModel');
		
		return $hpcNodeUsersModel->update($pk, $data);		
	}	
	public function checkNodeUserExists($nodeId, $username)
	{
		$hpcNodeUsersModel = model('App\Models\App\hpcNodeUsersModel');
		$hpcNodeUsersModel->where('node_id', $nodeId);
		$hpcNodeUsersModel->where('user', $username);
		
		return $hpcNodeUsersModel->countAllResults();
	}
	
	public function getTicketUserNode($identifier)
	{
		$this->select('nu.status,nu.user,nu.password');
		$this->where($this->table.'.ticket_identifier', $identifier);
		$this->join('app_hpc_node_users nu','nu.id = '.$this->table.'.node_user_id','left');
		
		return $this->first();
	}	
	
	public function changeStatus($ticketId, $status)
	{
		$this->where('ticket_identifier', $ticketId);
		$this->set('booking_status', $status);
		
		return $this->update();
	}
	
	public function getBookedPackage()
	{
		extract($_GET);
		
		$this->select($this->table.'.os_id,SUM(ph.cores) sum_cores,SUM(ph.ram) sum_ram,SUM(ph.storage) sum_storage,n.ip,bk.date');
		$this->where($this->table.'.booking_status','approved');
		$this->where($this->table.'.os_id', $os_id);
		$this->join('app_package_hpc ph','ph.id = '.$this->table.'.package_id');
		$this->join('app_hpc_booking_dates bk','bk.booking_hpc_id = '.$this->table.'.id');
		$this->join('app_hpc_nodes n','n.id = '.$this->table.'.node_id');
		
		$this->groupBy($this->table.'.os_id, bk.date,n.ip');
		
		$results = $this->findAll();
		
		$newPackages = [];
		$hpcPackageModel = model('App\Models\App\hpcPackageModel');
		$packages = $hpcPackageModel->findAll();
		
		foreach($packages as $package){
			$newPackages[$package['id']] = $package;
		}
		
		$hpcNodesModel = model('App\Models\App\hpcNodesModel');
		$limitNodes = $hpcNodesModel->findAll();
		
		$newLimit = [];
		foreach($limitNodes as $limit){
			$newLimit[$limit['cat_os_id'].'_'.$limit['ip']] = $limit;
		}
		$output = [];
		$newResults = [];
		foreach($results as $n => $result){
			$newResults[$n] = $result;
			$newResults[$n]['limit'] = $newLimit[$result['os_id'].'_'.$result['ip']];
			$coreOver = (($result['sum_cores'] + $newPackages[$package_id]['cores']) > $newResults[$n]['limit']['cores']);
			$ramOver = (($result['sum_ram'] + $newPackages[$package_id]['ram']) > $newResults[$n]['limit']['ram']);
			$storageOver = (($result['sum_storage'] + $newPackages[$package_id]['ram']) > $newResults[$n]['limit']['storage']);
			$newResults[$n]['availability'] = !($package_id == 3) && (!$coreOver && !$ramOver && !$storageOver);
			
			$output[] = ($newResults[$n]['availability'] === false) ? $result['date']:'';
		}

		$availability = [];
		foreach($newResults as $re){
			$availability[$re['date']][$re['ip']] = $re;
		}
		
		foreach($availability as $date => $val)
		{
			$check = [];
			foreach($availability[$re['date']] as $node){
				
				$check[] = $node['availability'];
			}
			if(count(array_unique($check)) === 1){
				if(current($check) === false){
					$output[] = $date;
				}
			}  
			  			
		}
	
		//return $availability;
		//return ['booked' => $newResults, 'package'=>$newPackages];
		return $output;
	}	
}

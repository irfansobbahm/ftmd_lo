<?php

namespace App\Models\App;

use CodeIgniter\Model;

class loMatrixModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'lo_matrix';
    protected $primaryKey       = 'id_lomatrix';
    protected $useAutoIncrement = false;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['kode_kuliah','lo_a','lo_b','lo_c','lo_d','lo_e','lo_f','lo_g'];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];
	
	public function getLOAktual($kode_kuliah)
	{
		$hpcSoftwareModel = model('App\Models\App\loMatrixModel');
		$this->where('kode_kuliah', $kode_kuliah);
		$data = $this->get();
		//echo $this->getLastQuery();
		return $data;
	}	
	
	public function getOSandSoftware()
	{
		$hpcSoftwareModel = model('App\Models\App\hpcSoftwareModel');
		$osList = $this->findAll();
		foreach($osList as $i => $os){
			$osList[$i]['softwares'] = $hpcSoftwareModel->where('os_id', $os['id'])->findAll();
		}
		
		return $osList;
	}
}

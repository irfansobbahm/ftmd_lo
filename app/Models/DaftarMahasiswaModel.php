<?php

namespace App\Models;

use CodeIgniter\Model;
use Ramsey\Uuid\Uuid;

class DaftarMahasiswaModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'daftar_mahasiswa';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['id_fakultas','id_prodi','kode_kuliah','kelas','semester','tahun_akademik','dosen_ina','nim_mhs'];

    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

	public function getMahasiswaDosen($dosen_ina = ''){
		$this->where($this->table.'.dosen_ina',$dosen_ina);
		$this->join('mahasiswa m','m.nim = '.$this->table.'.nim_mhs');
		$this->join('prodi p','p.id = '.$this->table.'.id_prodi');
		$this->orderBy('m.nim', 'ASC');
		
		return $this->findAll();
	}
}

<?php

namespace App\Models;

use CodeIgniter\Database\RawSql;
use CodeIgniter\Model;
use Ramsey\Uuid\Uuid;

class KategoriNilaiModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'kategori_nilai';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['nama','uuid_dosen','uuid_pembuat'];

    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];
	
	public function getRow($filter)
	{
		foreach($filter as $col => $val){
			$this->where($col, $val);
		}
		return $this->first();
	}
	
	public function getAll($filter)
	{
		foreach($filter as $col => $val){
			$this->where($col, $val);
		}
		return $this->findAll();
	}	
}

<?php

namespace App\Models;

use CodeIgniter\Database\RawSql;
use CodeIgniter\Model;
use Ramsey\Uuid\Uuid;

class PembobotanLoNilaiMhsModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'pembobotan_lo_nilai_mhs';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['nim','lo_a','lo_b','lo_c','lo_d','lo_e','lo_f','lo_g','nilai_lo','id_pembobotan_lo'];

    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];
	
	public function getRow($filter)
	{
		foreach($filter as $col => $val){
			$this->where($col, $val);
		}
		return $this->first();
	}
	
	public function getAll($filter)
	{
		foreach($filter as $col => $val){
			$this->where($col, $val);
		}
		return $this->findAll();
	}	
	public function save_plomhs($data)
	{
		$this->save($data);
		
		return $this->insertID();
	}	
}

<?php

namespace App\Models;


use CodeIgniter\Model;
use Ramsey\Uuid\Uuid;

class MenuModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'menu';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['parent_id','label','slug','display','has_badge','badge_helpers','icon_class','has_guide','map_route_id','index'];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules    = [];

    protected $validationMessages = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];	
	
	public $menus;
	
	public function getMenuByBadgeHelperName($badgeHelperName = '')
	{
		$this->select('*');
		$this->join('routes r','r.id = '.$this->table.'.map_route_id');
		$this->where('badge_helpers', $badgeHelperName);
		$this->where('has_badge', 1);
		$data = $this->findAll();
		$data = array_filter($data, function($obj){
			if (isset($obj->slug)) {
				$obj->slug = ($obj->slug == 'default' || empty($obj->slug)) ? slugify($obj->label) :$obj->slug;
			}
			return true;
		});		
		return $data;
	}	
	public function getMenus(string $role='',string $display = '')
	{
		// Load common helper 
		helper('Common_helpers');
		
		$arrData = $this->select('menu.id, menu.parent_id, menu.label, menu.slug, menu.has_badge, menu.badge_helpers, menu.icon_class, r.method, r.controller, r.function, r.roles, r.namespace, r.named_as');
		if(!empty($display)){
			$arrData = $this->where('menu.display', $display);
		}
		$arrData = $this->join('routes r','r.id = menu.map_route_id', 'left');
		$arrData = (!empty($role)) ? $arrData->like('r.roles', $role):$arrData;
		$arrData = $arrData->orderBy('menu.index','ASC');
		$arrData = $arrData->findAll();
		//echo $this->getLastQuery();
		$arrTreeById = [];
		$objItem = array_filter($arrData, function($obj){
			if (isset($obj->slug)) {
				$obj->slug = ($obj->slug == 'default' || empty($obj->slug)) ? slugify($obj->label) :$obj->slug;
			}
			return true;
		});
		foreach($arrData AS $objItem)
		{

			$arrTreeById[$objItem->id] = $objItem;
			$objItem->submenu = [];
		}

		$arrChildIds = [];

		foreach($arrTreeById AS $objItem)
		{
			if (isset($arrTreeById[$objItem->parent_id]))   
			{
				$arrTreeById[$objItem->parent_id]->submenu[] = $objItem;
				$arrChildIds[] = $objItem->id;
			}
		}

		array_walk($arrChildIds, function($val) use (&$arrTreeById) {
			unset($arrTreeById[$val]);
		});
		
		$this->menus = $arrTreeById;
		
		return $arrTreeById;
	}	

	function getPath(array $arrObj = [], $path='') {
		
		$result = array();
		$arrObj = !func_num_args() ? $this->menus: $arrObj;
		
		foreach($arrObj as $obj){
			if(count($obj->submenu) > 0){
				$result[$obj->slug] = $obj->slug;
				$result = array_merge($result,$this->getPath($obj->submenu, $obj->slug));
			}else{
				$result[$obj->slug] = (!empty($path) ? $path.'/':'').$obj->slug;
			}
		}
		
		return $result;
		
	}	
	
	function pageInfo()
	{
		$router = service('router'); 
		$this->controller  = class_basename($router->controllerName()); 
		$this->method  = class_basename($router->methodName());
		
		$uri = current_url(true);
		//print_r($uri->getSegment($uri->getTotalSegments()));
		
		$page = $this->select('*')
		->join('routes r','r.id = menu.map_route_id')
		->where('r.method', 'get')
		->where('r.controller', $this->controller)
		->where('r.function', $this->method)
		->first();

		return $page;
	}
	
}

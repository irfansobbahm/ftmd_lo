<?php

namespace App\Models;

use CodeIgniter\Model;
use Ramsey\Uuid\Uuid;

class UserForms extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'user_forms';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['title','description','user_id','status'];

    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

	public function getAll($user_id = '')
	{
		if(!empty($user_id))
		{
			$this->where('user_id',$user_id);
		}
		$this->select($this->table.".*, (SELECT COUNT(*) FROM user_form_fields WHERE user_id = '".$user_id."' AND form_id = ".$this->table.".id ) as fieldscount");
		return $this->findAll();
	}
	
	public function getForm($form_id = '')
	{

		$result = $this->find($form_id);
		
		return $result;
	}
}

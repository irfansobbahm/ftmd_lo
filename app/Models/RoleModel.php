<?php

namespace App\Models;

use CodeIgniter\Model;
use Ramsey\Uuid\Uuid;

class RoleModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'roles';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = false;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['identifier','name'];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules    = [
        'identifier'     => 'required|min_length[3]|is_unique[roles.identifier]',
    ];

    protected $validationMessages = [
        'identifier'        => [
            'is_unique' => 'Sorry. Identifier already exists.',
        ],
    ];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = ['setPKVal'];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];
	
	protected function setPKVal(array $data)
	{
		$data['data'][$this->primaryKey] = Uuid::uuid4()->toString();

		return $data;		
	}	
	
	public function setRoleMenu(string $role)
	{
		$menuModel = model('MenuModel');
		$menus = $menuModel->getMenus($role);
		$role_resources = $menuModel->getPath();

		return array_map(function($val) use($role){
			return $role.'/'.$val;
		}, $role_resources);
	}
}

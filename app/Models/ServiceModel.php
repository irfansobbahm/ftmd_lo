<?php

namespace App\Models;

use CodeIgniter\Model;
use Ramsey\Uuid\Uuid;

class ServiceModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'services';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = false;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['title','description','service_table','service_form','service_model','catid','parent_catid'];

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = ['setPKVal'];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];
	
	protected function setPKVal(array $data)
	{
		$data['data'][$this->primaryKey] = Uuid::uuid4()->toString();

		return $data;		
	}	
	
	public function getServiceByCurrentURLSlug($uri)
	{
		$lastSegment = $uri->getSegment($uri->getTotalSegments());
		
		return $this->where('SLUGIFY(title)', $lastSegment)->first();		
	}
}

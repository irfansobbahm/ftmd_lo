<?php

namespace App\Models;

use CodeIgniter\Model;
use Ramsey\Uuid\Uuid;

class UserFormFields extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'user_form_fields';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['form_id','descr','type','sub'];

    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

	public function getTypes($user_id = '')
	{
		if(!empty($user_id))
		{
			$this->where('user_id',$user_id);
		}
		return $this->findAll();
	}
	
	public function getFields($form_id = '')
	{
		$this->where('form_id',$form_id);
		return $this->findAll();
	}
}

<?php

namespace App\Models;

use CodeIgniter\Database\RawSql;
use CodeIgniter\Model;
use Ramsey\Uuid\Uuid;

class NilaiMhsModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'kelompok_nilai_mhs';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['id_kelompok_nilai','nim','total','nilai'];

    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];
	
	public function getRow($filter)
	{
		foreach($filter as $col => $val){
			$this->where($col, $val);
		}
		return $this->first();
	}
	
	public function getAll($filter)
	{
		foreach($filter as $col => $val){
			$this->where($col, $val);
		}
		return $this->findAll();
	}

	public function getAllKelompokNilaiMhs($filter)
	{
		foreach($filter as $col => $val){
			$this->where($col, $val);
		}
		$data = [];
		$findAll = $this->findAll();
		if($this->countAllResults() > 0){
			foreach($findAll as $row)
			{
				$data[$row->nim] = $row;
			}
		}
		return $data;
	}	
}

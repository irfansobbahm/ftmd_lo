<?php

namespace App\Models;

use CodeIgniter\Model;
use Ramsey\Uuid\Uuid;

class UserModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'users';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = false;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['username','nip_nim','role','name','email'];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    // protected $validationRules    = [
        // 'username'     => 'required|alpha_numeric_space|min_length[3]|is_unique[users.username]',
        // 'email'        => 'required|valid_email|is_unique[users.email]',
        // // 'password'     => 'required|min_length[8]',
        // // 'pass_confirm' => 'required_with[password]|matches[password]',
    // ];

    // protected $validationMessages = [
        // 'email'        => [
            // 'is_unique' => 'Sorry. That email has already been taken. Please choose another.',
        // ],
        // 'username'        => [
            // 'is_unique' => 'Sorry. That username has already been taken. Please choose another.',
        // ],		
    // ];
    protected $skipValidation       = true;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = ['setPKVal'];//,'hashPassword'];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];
	
	protected function setPKVal(array $data)
	{
		$data['data'][$this->primaryKey] = Uuid::uuid4()->toString();

		return $data;		
	}
	
	protected function hashPassword(array $data)
	{
		if (! isset($data['data']['password'])) {
			return $data;
		}
		
		//password_verify(string $password, string $hash)
		$data['data']['password_hash'] = password_hash($data['data']['password'], PASSWORD_DEFAULT);
		unset($data['data']['password']);

		return $data;
	}	
	
	public function getAgents(){
		$this->where('role','agent');
		
		return $this->findAll();
	}
	
	public function changeUserRole($username=0, $data=[])
	{
		if(!empty($data)){
			foreach($data as $col => $val)
			{
				$this->set($col, $val);
			}
		}
		$this->where('username', $username);
		
		return $this->update();
	}
	
	public function userExists($username)
	{
		return $this->getWhere(['username'=>$username]);
	}
	
	public function addNewUser($data)
	{
		
		$this->insert($data);
		
		return $this->insertID();
	}
	
	public function count_allNewUserToday()
	{
		$this->select("COUNT(*) total");
		$this->where('DATE(created_at)','DATE(NOW())',false);
		$result = $this->first();

		return $result['total'];
	}
	public function count_allUsers()
	{
		$this->select("COUNT(*) total");
		$result = $this->first();

		return $result['total'];
	}	
	public function count_allUserSinceLastWeek()
	{
		$this->select("COUNT(*) total");
		$this->where('DATE(created_at) >=','(DATE(created_at) - INTERVAL DAYOFWEEK(DATE(NOW()))+6 DAY)',false);
		$this->where('DATE(created_at) <','(DATE(created_at) - INTERVAL DAYOFWEEK(DATE(NOW()))-1 DAY)',false);
		$result = $this->first();

		return $result['total'];
	}	
}

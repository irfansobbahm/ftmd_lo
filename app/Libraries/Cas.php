<?php

class Cas
{
	function __construct()
	{
		// Enable debugging
		phpCAS::setLogger();
		//phpCAS::setDebug('/tmp/phpcas.log');
		// Enable verbose error messages. Disable in production!
		phpCAS::setVerbose(true);

		// Initialize phpCAS
		// 'http://login-dev4.itb.ac.id/cas';
		phpCAS::client(CAS_VERSION_2_0, 'login-dev4.itb.ac.id', 443, '/cas');
		
		//
		phpCAS::setNoCasServerValidation();	
		
		// TAMBAH INI JIKA REDIRECT LOOP TERJADI.
		//phpCAS::setNoClearTicketsFromUrl();		
	}
 
}

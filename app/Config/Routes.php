<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
/*function ()
{
    //echo view('errors/html/error_403');
}*/
$routes->setAutoRoute(true);
$this->session = session();

$routes->group($this->session->has('role') ? $this->session->get('role') : '(:any)', ['filter' => 'authFilter'],function($routes)
{
	$roleModel = model('RoleModel');
	$menuModel = model('MenuModel');
	$menus = $menuModel->getMenus();
	$menu_path = $menuModel->getPath();

	$_menus = function(array $obj, &$routes, $menu_path) use (&$_menus)
	{
		foreach($obj as $menu)
		{
			//echo "<pre>";
			//print_r(explode('/(:any)',$menu->slug));
			if(!empty($menu->method))
			{
				$args = [
					$menu_path[$menu->slug],
					$menu->controller.'::'.(empty($menu->function) ? 'index':$menu->function)
				];
				
				$optionalParams = [];
				if(!empty($menu->named_as)){
					$optionalParams['as'] = $menu->named_as;
					$args[] = $optionalParams;
				}	
				if(!empty($menu->namespace)){
					$optionalParams['namespace'] = $menu->namespace;
					$args[] = $optionalParams;
				}			
				call_user_func_array([$routes,$menu->method], $args);
				//echo $menu_path[$menu->slug].$menu->controller.'::'.(empty($menu->function) ? 'index':$menu->function)."<br>";
				//$routes->{$menu->method}($menu_path[$menu->slug], $menu->controller.'::'.(empty($menu->function) ? 'index':$menu->function));
				if(count($menu->submenu) > 0){
					
					$_menus($menu->submenu, $routes, $menu_path);
				}
			}
		}
	};
	$_menus($menus, $routes, $menu_path);
//exit;
	$role_path = $roleModel->setRoleMenu($this->session->has('role') ? $this->session->get('role') : '');
// echo "<pre>";
// print_r($role_path);
// exit;
	$this->session->set('role_path', $role_path);

});
// $routes->group('agent', function($routes)
// {
	// $routes->get('api/get-users-nodes/(:any)', 'Hpc::usersNode/$1',['namespace'=>'App\\Controllers\\Api']);
// });
$routes->get('home','Home::index');
//$routes->get('(:any)','BaseController::http404');
$routes->get('403','BaseController::http403',['as'=>'403']);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}

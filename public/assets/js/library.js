var STDEV = function(arr) {
    var n = arr.length;
    var sum = 0;

    arr.map(function(data) {
        sum+=data;
    });

    var mean = sum / n;

    var variance = 0.0;
    var v1 = 0.0;
    var v2 = 0.0;

    if (n != 1) {
        for (var i = 0; i<n; i++) {
            v1 = v1 + (arr[i] - mean) * (arr[i] - mean);
            v2 = v2 + (arr[i] - mean);
        }

        v2 = v2 * v2 / n;
        variance = (v1 - v2) / (n-1);
        if (variance < 0) { variance = 0; }
        stddev = Math.sqrt(variance);
    }

    return {
        mean: Math.round(mean*100)/100,
        variance: variance,
        deviation: Math.round(stddev*100)/100
    };
};

const SUMPRODUCT = (callback, ar1, ar2) => {
  if(ar1.length !== ar2.length)
	throw new RangeError()

  let sum = 0
	  
  for(i=0; i<ar1.length; i++){
	if(callback(ar1[i], ar2[i]))
	  sum += ar1[i] * ar2[i]
  }

  return sum
}


// Javascript program to calculate the standard deviation of an array
const DEVIATION = function(arr){
	// Creating the mean with Array.reduce
	let mean = arr.reduce((acc, curr)=>{
		return acc + curr
	}, 0) / arr.length;
	
	// Assigning (value - mean) ^ 2 to every array item
	arr = arr.map((k)=>{
		return (k - mean) ** 2
	})
	
	// Calculating the sum of updated array
	let sum = arr.reduce((acc, curr)=> acc + curr, 0);
	
	// Calculating the variance
	let variance = sum / arr.length
	
	// Returning the standard deviation
	return Math.sqrt(sum / arr.length)
}

